// HistoryDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "AutoWeatherStation.h"
#include "AutoWeatherStationDlg.h"
#include "HistoryDlg.h"
#include "afxdialogex.h"
#include "ElementSelectDlg.h"
#include "StationSelectDlg.h"
#include "CApplication.h"
#include "CWorkbooks.h"
#include "CWorkbook.h"
#include "CWorksheets.h"
#include "CWorksheet.h"
#include "CRange.h"
#include "CFont0.h"
#include "ProgressCtrlThread.h"

//自定义消息
#define WM_THREAD_MESSAGES		WM_USER + 99		////用于与UI线程（而不是其中的对话框）直接通信的自定义消息
#define WM_PROGRESS_CHANGED		WM_USER + 100		//用于改变ProgressCtrlThread线程中进度条信息的自定义消息

//Excel文件格式枚举信息 From：其他项目正确Import后的Excel.tlh
enum XlFileFormat
{
	xlAddIn = 18,
	xlCSV = 6,
	xlCSVMac = 22,
	xlCSVMSDOS = 24,
	xlCSVWindows = 23,
	xlDBF2 = 7,
	xlDBF3 = 8,
	xlDBF4 = 11,
	xlDIF = 9,
	xlExcel2 = 16,
	xlExcel2FarEast = 27,
	xlExcel3 = 29,
	xlExcel4 = 33,
	xlExcel5 = 39,
	xlExcel7 = 39,
	xlExcel9795 = 43,
	xlExcel4Workbook = 35,
	xlIntlAddIn = 26,
	xlIntlMacro = 25,
	xlWorkbookNormal = -4143,
	xlSYLK = 2,
	xlTemplate = 17,
	xlCurrentPlatformText = -4158,
	xlTextMac = 19,
	xlTextMSDOS = 21,
	xlTextPrinter = 36,
	xlTextWindows = 20,
	xlWJ2WD1 = 14,
	xlWK1 = 5,
	xlWK1ALL = 31,
	xlWK1FMT = 30,
	xlWK3 = 15,
	xlWK4 = 38,
	xlWK3FM3 = 32,
	xlWKS = 4,
	xlWorks2FarEast = 28,
	xlWQ1 = 34,
	xlWJ3 = 40,
	xlWJ3FJ3 = 41,
	xlUnicodeText = 42,
	xlHtml = 44,
	xlWebArchive = 45,
	xlXMLSpreadsheet = 46,
	xlExcel12 = 50,
	xlOpenXMLWorkbook = 51,
	xlOpenXMLWorkbookMacroEnabled = 52,
	xlOpenXMLTemplateMacroEnabled = 53,
	xlTemplate8 = 17,
	xlOpenXMLTemplate = 54,
	xlAddIn8 = 18,
	xlOpenXMLAddIn = 55,
	xlExcel8 = 56,
	xlOpenDocumentSpreadsheet = 60,
	xlWorkbookDefault = 51
};

// CHistoryDlg 对话框

IMPLEMENT_DYNAMIC(CHistoryDlg, CDialogEx)

CHistoryDlg::CHistoryDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CHistoryDlg::IDD, pParent)
{
	m_pElementSelect = new TCHAR[20];	//分配内存用于存放从ini文件中读取的信息
}

CHistoryDlg::~CHistoryDlg()
{
	delete[] m_pElementSelect;	//释放内存
	m_pElementSelect = NULL;	//指针赋为NULL
}

void CHistoryDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_DATE_START, m_DateStart);
	DDX_Control(pDX, IDC_DATE_END, m_DateEnd);
	DDX_Control(pDX, IDC_TIME_END, m_TimeEnd);
	DDX_Control(pDX, IDC_TIME_START, m_TimeStart);
	DDX_Control(pDX, IDC_LIST_TABLE, m_HistoryTable);
}


BEGIN_MESSAGE_MAP(CHistoryDlg, CDialogEx)
	ON_BN_CLICKED(IDC_BTN_STATIONSET, &CHistoryDlg::OnBnClickedBtnStationset)
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_BTN_ELEMENTSET, &CHistoryDlg::OnBnClickedBtnElementset)
	ON_BN_CLICKED(IDC_BTN_Q, &CHistoryDlg::OnBnClickedBtnQ)
	ON_WM_SIZE()
	ON_WM_SIZING()
	ON_BN_CLICKED(IDC_BTN_EXPORTDATA, &CHistoryDlg::OnBnClickedBtnExportdata)
END_MESSAGE_MAP()


/**************************************************************************************************************************************
//	EASYSIZE Macro 注解
//来源	http://www.codeproject.com/KB/dialog/easysize.aspx?msg=1198087	
//	
//格式	EASYSIZE(control,left,top,right,bottom,options)
//
//参数说明
	control is the ID of the dialog item you want re-positioned (which will be referred to as the 'current control' further on).
	left, top, right and bottom can be either the ID of another control in the dialog (not the current control), or one of the 
special values, ES_BORDER and ES_KEEPSIZE.
	Basically, if you specify an ID, the distance from the current control and the item designated by the ID will remain the same 
when the dialog is resized: The current control will 'stick' to the other item. ES_BORDER works the same way as if you had 
specified a control ID, except that it's the distance between the current control and the dialog border that will be kept 
constant. Specifying ES_KEEPSIZE in, let's say left, will keep the width of the current control the same, and will make the 
current control right-aligned to whatever you specified in right. The width (or height, if you specified ES_KEEPSIZE in top or 
bottom) of the current control will always remain what it is in the dialog resource. (I know this explanation sucks, but look 
at the demo application if you are confused or post you question in the board below). Obviously ES_KEEPSIZE cannot be specified 
in both "left and right" or "top and bottom".
	options can be a combination of ES_HCENTER, ES_VCENTER and 0 (use 0 if you don't want any of the other). ES_HCENTER horizontally 
centers the control between the two items specified in left and right (both of those can not be ES_KEEPSIZE!). The width of the 
current control will always remain the same as in the dialog resource. ES_VCENTER works the same way, but for vertical centering 
(using top and bottom, and where the height will remain constant).
**************************************************************************************************************************************/
BEGIN_EASYSIZE_MAP(CHistoryDlg)
	EASYSIZE(IDC_LIST_TABLE, ES_BORDER, ES_BORDER, ES_BORDER, ES_BORDER, 0)
	EASYSIZE(IDC_STATIC4, ES_KEEPSIZE, ES_BORDER, ES_BORDER, ES_KEEPSIZE, 0)
	EASYSIZE(IDC_STATIC5, ES_KEEPSIZE, ES_BORDER, ES_BORDER, ES_KEEPSIZE, 0)
	EASYSIZE(IDC_STATIC_DATA_NUM, ES_KEEPSIZE, ES_BORDER, ES_BORDER, ES_KEEPSIZE, 0)
	EASYSIZE(IDC_BTN_EXPORTDATA, ES_KEEPSIZE, ES_BORDER, ES_BORDER, ES_KEEPSIZE, 0)	//注意至少要有一个ES_BORDER，否则控件将无法显示
	EASYSIZE(IDC_STATIC_LOGO, IDC_STATIC_LEFT, ES_BORDER, IDC_STATIC4, ES_KEEPSIZE, ES_HCENTER)
END_EASYSIZE_MAP

// CHistoryDlg 消息处理程序


BOOL CHistoryDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  在此添加额外的初始化
////初始化EasySize
	INIT_EASYSIZE;
////初始化DateTimePicker
	CTime
		dtpMin(2014, 1, 1, 0, 0, 0),
		dtpMax(2020, 12, 31, 0, 0, 0);
	//设置控件可显示的日期范围
	m_DateStart.SetRange(&dtpMin, &dtpMax);
	m_DateEnd.SetRange(&dtpMin, &dtpMax);
	CTime tmpTime(2014, 1, 1, 0, 0, 0);
	m_DateStart.SetTime(&tmpTime);
	m_TimeStart.SetTime(&tmpTime);
//	CString str;
//	str = tmpTime.Format(_T("%x\r\n%X"));
////从Config.ini文件中读取上次CheckBox选中情况的数据存入buf中
	GetPrivateProfileString(_T("HistoryData"), _T("ElementSelect"), _T(""), m_pElementSelect, 20, _T(".\\Config.ini"));	//注意nSize = 19 + 1，要为字符串结束符\0留位置
////初始化表格
	//设置列表控件风格	
	m_HistoryTable.SetExtendedStyle(
		//		LVS_EX_AUTOSIZECOLUMNS |		//Automatically size LISTVIEW columns.
		//		LVS_EX_FLATSB |					//扁平风格显示滚动条
		//		LVS_EX_FULLROWSELECT |			//整行选中
		//		LVS_EX_HEADERDRAGDROP |			//表头可拖动
		//		LVS_EX_TRACKSELECT |			//选中跟踪
		LVS_EX_GRIDLINES |				//画出网格线
		//		LVS_EX_BORDERSELECT |			//Changes border color when an item is selected, instead of highlighting the item.
		//		LVS_EX_CHECKBOXES |				//行头添加CheckBox
		//		LVS_EX_HEADERINALLVIEWS |		//显示列头，Show column headers in all view modes.
		//		LVS_EX_COLUMNOVERFLOW |			//Indicates that an overflow button should be displayed in icon/tile view if there is not enough client width to display the complete set of header items.
		//		LVS_EX_TRANSPARENTBKGND |		//表格背景透明
		//		LVS_EX_ONECLICKACTIVATE |		//单击激活（鼠标划过时变为手型，单击时响应LVN_ITEMACTIVATE事件）
		//		LVS_EX_TWOCLICKACTIVATE |		//双击激活（鼠标单击时变为手型，再单击则响应LVN_ITEMACTIVATE事件）
		//		LVS_EX_UNDERLINECOLD |			//选中时带有文字下划线
		//		LVS_EX_UNDERLINEHOT |			//选中时带有文字下划线，离开时颜色变浅
		NULL							//占位，便于测试各个参数功能
		);
	//插入列
	int i = 2;	//用于选择要素
	TCHAR* pTmpChar = m_pElementSelect;	//【注意】使用数组前将指向数组的指针赋给临时变量
	m_HistoryTable.InsertColumn(0, _T("台站名"), LVCFMT_LEFT, 80);
	m_HistoryTable.InsertColumn(1, _T("时间"), LVCFMT_LEFT, 150);
	if (*pTmpChar++ == _T('1'))
		m_HistoryTable.InsertColumn(i++, _T("当前温度(℃)"), LVCFMT_LEFT, 80);
	if (*pTmpChar++ == _T('1'))
		m_HistoryTable.InsertColumn(i++, _T("平均温度(℃)"), LVCFMT_LEFT, 80);
	if (*pTmpChar++ == _T('1'))
		m_HistoryTable.InsertColumn(i++, _T("最大温度(℃)"), LVCFMT_LEFT, 80);
	if (*pTmpChar++ == _T('1'))
		m_HistoryTable.InsertColumn(i++, _T("最小温度(℃)"), LVCFMT_LEFT, 80);
	if (*pTmpChar++ == _T('1'))
		m_HistoryTable.InsertColumn(i++, _T("当前湿度(%)"), LVCFMT_LEFT, 80);
	if (*pTmpChar++ == _T('1'))
		m_HistoryTable.InsertColumn(i++, _T("平均湿度(%)"), LVCFMT_LEFT, 80);
	if (*pTmpChar++ == _T('1'))
		m_HistoryTable.InsertColumn(i++, _T("最大湿度(%)"), LVCFMT_LEFT, 80);
	if (*pTmpChar++ == _T('1'))
		m_HistoryTable.InsertColumn(i++, _T("最小湿度(%)"), LVCFMT_LEFT, 80);
	if (*pTmpChar++ == _T('1'))
		m_HistoryTable.InsertColumn(i++, _T("当前气压(kPa)"), LVCFMT_LEFT, 90);
	if (*pTmpChar++ == _T('1'))
		m_HistoryTable.InsertColumn(i++, _T("平均气压(kPa)"), LVCFMT_LEFT, 90);
	if (*pTmpChar++ == _T('1'))
		m_HistoryTable.InsertColumn(i++, _T("最大气压(kPa)"), LVCFMT_LEFT, 90);
	if (*pTmpChar++ == _T('1'))
		m_HistoryTable.InsertColumn(i++, _T("最小气压(kPa)"), LVCFMT_LEFT, 90);
	if (*pTmpChar++ == _T('1'))
		m_HistoryTable.InsertColumn(i++, _T("当前风速(m/s)"), LVCFMT_LEFT, 90);
	if (*pTmpChar++ == _T('1'))
		m_HistoryTable.InsertColumn(i++, _T("平均风速(m/s)"), LVCFMT_LEFT, 90);
	if (*pTmpChar++ == _T('1'))
		m_HistoryTable.InsertColumn(i++, _T("最大风速(m/s)"), LVCFMT_LEFT, 90);
	if (*pTmpChar++ == _T('1'))
		m_HistoryTable.InsertColumn(i++, _T("最小风速(m/s)"), LVCFMT_LEFT, 90);
	if (*pTmpChar++ == _T('1'))
		m_HistoryTable.InsertColumn(i++, _T("矢量风速(m/s)"), LVCFMT_LEFT, 90);
	if (*pTmpChar++ == _T('1'))
		m_HistoryTable.InsertColumn(i++, _T("当前风向(°)"), LVCFMT_LEFT, 80);
	if (*pTmpChar++ == _T('1'))
		m_HistoryTable.InsertColumn(i++, _T("矢量风向(°)"), LVCFMT_LEFT, 80);
////设置对话框图标
	HICON m_hIcon;
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	SetIcon(m_hIcon, TRUE); // Set big icon
	SetIcon(m_hIcon, FALSE); // Set small icon

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常:  OCX 属性页应返回 FALSE
}


void CHistoryDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	// TODO:  在此处添加消息处理程序代码
	UPDATE_EASYSIZE;	//EasySize用于更新控件位置的宏
}


void CHistoryDlg::OnSizing(UINT fwSide, LPRECT pRect)
{
	CDialogEx::OnSizing(fwSide, pRect);

	// TODO:  在此处添加消息处理程序代码
	EASYSIZE_MINSIZE(910, 500, fwSide, pRect);	//参数用法：mx为宽，my为高
}


void CHistoryDlg::OnClose()
{
	// TODO:  在此添加消息处理程序代码和/或调用默认值
//	DestroyWindow();	//关闭并销毁由Create()创建的窗口
//	CDialogEx::OnClose();
	ShowWindow(FALSE);	//使用默认方式，即隐藏（而不销毁）窗口
	CAutoWeatherStationDlg* pDlg = (CAutoWeatherStationDlg*)theApp.GetMainWnd();
	pDlg->m_toolbar.GetToolBarCtrl().CheckButton(ID_MENU_HISTORY, FALSE);	//同步Toolbox中的按钮
}


void CHistoryDlg::OnOK()	//屏蔽Esc键关闭窗口
{
	// TODO:  在此添加专用代码和/或调用基类

//	CDialogEx::OnOK();
}


void CHistoryDlg::OnCancel()	//屏蔽回车键关闭窗口
{
	// TODO:  在此添加专用代码和/或调用基类

//	CDialogEx::OnCancel();
}


void CHistoryDlg::OnBnClickedBtnElementset()
{
	// TODO:  在此添加控件通知处理程序代码
	CElementSelectDlg esdlg;
	esdlg.SetOwner(NULL);
	esdlg.DoModal();
}


void CHistoryDlg::OnBnClickedBtnStationset()
{
	// TODO:  在此添加控件通知处理程序代码
	CStationSelectDlg ssdlg;
	ssdlg.DoModal();
}


void CHistoryDlg::OnBnClickedBtnQ()
{
	// TODO:  在此添加控件通知处理程序代码
////从Config.ini文件中读取已选择要素
	GetPrivateProfileString(_T("HistoryData"), _T("ElementSelect"), _T(""), m_pElementSelect, 20, _T(".\\Config.ini"));
////从Config.ini文件中读取上次保存的已选择台站（需要查询的台站）列表
	TCHAR buf[2048] = { '\0' };
	GetPrivateProfileString(_T("HistoryData"), _T("SelectedStation"), _T(""), buf, 2048, _T(".\\Config.ini"));	//注意nSize = 2047 + 1，要为字符串结束符\0留位置
	CString strSelSta(buf);
	//将所有逗号替换为“' OR AwsType = '”，再增加头部“AwsType = '”和尾部“'”以组成完整的WHERE语句
	strSelSta.Replace(_T(","), _T("' OR AwsType = '"));
	strSelSta = _T("AwsType = '") + strSelSta + _T("'");
////从时间日期控件中读取需要查询的时间日期区间
	CString strLowerLimit, strUpperLimit, strDateTimeInterval;
	CTime tLowerDate, tLowerTime, tUpperDate, tUpperTime;
	m_DateStart.GetTime(tLowerDate);
	m_TimeStart.GetTime(tLowerTime);
	strLowerLimit = _T("TTime >= #") + tLowerDate.Format(_T("%Y-%m-%d")) + _T(" ") + tLowerTime.Format(_T("%H:%M:%S")) + _T("#");
	m_DateEnd.GetTime(tUpperDate);
	m_TimeEnd.GetTime(tUpperTime);
	strUpperLimit = _T("TTime <= #") + tUpperDate.Format(_T("%Y-%m-%d")) + _T(" ") + tUpperTime.Format(_T("%H:%M:%S")) + _T("#");
	strDateTimeInterval = strLowerLimit + _T(" AND ") + strUpperLimit;
	//预判断，查询的开始时间不应当晚于结束时间
	if (tLowerDate > tUpperDate || (tLowerDate <= tUpperDate && tLowerTime.GetHour() * 3600 + tLowerTime.GetMinute() * 60 + tLowerTime.GetSecond() > tUpperTime.GetHour() * 3600 + tUpperTime.GetMinute() * 60 + tUpperTime.GetSecond()))
	{
		AfxMessageBox(_T("查询的开始时间不应当晚于结束时间，请重新选择！"));
		return;
	}
////清空ListCtrl
	m_HistoryTable.DeleteAllItems();
////打开数据库
	//查询条件
	m_AwsDatabase.m_strFilter = _T("(") + strSelSta + _T(")") + _T(" AND ") + strDateTimeInterval;	//由于AND优先级大于OR，因此要在台站选择字串首尾加上括号
//	m_AwsDatabase.m_strFilter = "AwsType = 'MS50U000' OR AwsType = 'MS50U001'";		//条件查询测试
//	m_AwsDatabase.m_strFilter = "AwsType = 'MS50U001' AND CurTemp = 1";
//	m_AwsDatabase.m_strFilter = "AwsType = 'MS50U004' OR AwsType = 'MS50U111'";
//	m_AwsDatabase.m_strFilter = "TTime >= #2015-1-29 00:00:01# AND TTime <= #2015-1-29 23:59:59#";
	//在前述条件下打开数据库
	if (!m_AwsDatabase.IsOpen())
	{
		m_AwsDatabase.Open();
	}
	else
	{
		m_AwsDatabase.Requery();
	//	m_AwsDatabase.Close();
	//	m_AwsDatabase.Open();
	}
	int iDataCount = 0;
//	iDataCount = m_AwsDatabase.GetODBCFieldCount();	//获取数据库列数
//	m_AwsDatabase.MoveFirst();	//前面有Requery()，因此无需MoveFirst()
	while (!m_AwsDatabase.IsEOF())
	{
		m_AwsDatabase.MoveNext();
	}
	iDataCount = m_AwsDatabase.GetRecordCount();
	m_AwsDatabase.MoveFirst();	//重新指回第一个数据
////取出数据并显示
	//现将上面获取到的数据数量在对话框中显示出来
	CString str;
	str.Format(_T("%d"), iDataCount);
	CStatic* pSta = (CStatic*)GetDlgItem(IDC_STATIC_DATA_NUM);
	pSta->SetWindowText(str);
	//创建ProgressCtrl的UI线程，用于显示处理进度条
	CWinThread* pThread = AfxBeginThread(RUNTIME_CLASS(CProgressCtrlThread));	//此UI线程创建后会自动结束，无需手动关闭，原理详见CProgressCtrlThread::InitInstance()
//	CWinThread* pThread1 = AfxBeginThread(RUNTIME_CLASS(CProgressCtrlThread));	//测试同时创建多个UI线程，成功
//【注意】向UI线程中的对话框发送消息，暂时有2种思路：
//	①	设法获取到对话框的句柄，然后直接::PostMessage()，在UI线程的对话框类的MESSAGE_MAP消息映射中实现ON_MESSAGE()宏，如：
//	HWND hdlg = pThread->GetMainWnd()->GetSafeHwnd();	//获取UI线程主对话框句柄	
//	BOOL success = ::PostMessage(hdlg, WM_PROGRESS_CHANGED, (WPARAM)1, (LPARAM)iDataCount);	//向UI线程的主对话框发送自定义消息,设置ProgressCtrl进度条的Range
//	if (!success)	//如果消息未发送成功
//		DWORD err = ::GetLastError();
//	经多次测试，只成功了一次，原因未知，待研究
//	②	直接使用::PostThreadMessage()或CWinThread::PostThreadMessage()向UI线程的发送消息，然后在UI线程里面再对对话框控件进行操作（发送消息或直接获取控件指针），
//		注意在UI线程的在UI线程本身的类中的MESSAGE_MAP消息映射里实现ON_THREAD_MESSAGE宏（不是ON_MESSAGE宏），如：
	pThread->PostThreadMessage(WM_THREAD_MESSAGES, (WPARAM)1, (LPARAM)iDataCount);	//向UI线程发送自定义消息,设置ProgressCtrl进度条的Range
//	pThread1->PostThreadMessage(WM_THREAD_MESSAGES, (WPARAM)1, (LPARAM)iDataCount);	
//	或
//	::PostThreadMessage(pThread->m_nThreadID, WM_THREAD_MESSAGES, (WPARAM)1, (LPARAM)iDataCount);
//	均可

	//将数据库内容全部复制到ListCtrl中
	int iRow = 0;	//注意上限2^32 = 2147483648
	CString strTemp;
	while (!m_AwsDatabase.IsEOF())
	{
		int i = 2;	//用于选择要素
		TCHAR* pTmpChar = m_pElementSelect;	//首地址暂存起来以供还原
		//插入行
		m_HistoryTable.InsertItem(iRow, _T(""));
		//添加内容
		//台站名
		m_HistoryTable.SetItemText(iRow, 0, m_AwsDatabase.m_AwsType);
		//时间
		strTemp = m_AwsDatabase.m_TTime.Format(_T("%Y年%m月%d日 %X"));	//CTime格式化为CString
		m_HistoryTable.SetItemText(iRow, 1, strTemp);
		//温度
		if (*pTmpChar++ == _T('1'))
		{
			strTemp.Format(_T("%.2f"), (m_AwsDatabase.m_CurTemp - 5000) / 100.0);
			m_HistoryTable.SetItemText(iRow, i++, strTemp);
		}
		if (*pTmpChar++ == _T('1'))
		{
			strTemp.Format(_T("%.2f"), (m_AwsDatabase.m_AvgTemp - 5000) / 100.0);
			m_HistoryTable.SetItemText(iRow, i++, strTemp);
		}
		if (*pTmpChar++ == _T('1'))
		{
			strTemp.Format(_T("%.2f"), (m_AwsDatabase.m_MaxTemp - 5000) / 100.0);
			m_HistoryTable.SetItemText(iRow, i++, strTemp);
		}
		if (*pTmpChar++ == _T('1'))
		{
			strTemp.Format(_T("%.2f"), (m_AwsDatabase.m_MaxTemp - 5000) / 100.0);
			m_HistoryTable.SetItemText(iRow, i++, strTemp);
		}
		//湿度
		if (*pTmpChar++ == _T('1'))
		{
			strTemp.Format(_T("%ld"), m_AwsDatabase.m_CurHumi);
			m_HistoryTable.SetItemText(iRow, i++, strTemp);
		}
		if (*pTmpChar++ == _T('1'))
		{
			strTemp.Format(_T("%ld"), m_AwsDatabase.m_AvgHumi);
			m_HistoryTable.SetItemText(iRow, i++, strTemp);
		}
		if (*pTmpChar++ == _T('1'))
		{
			strTemp.Format(_T("%ld"), m_AwsDatabase.m_MaxHumi);
			m_HistoryTable.SetItemText(iRow, i++, strTemp);
		}
		if (*pTmpChar++ == _T('1'))
		{
			strTemp.Format(_T("%ld"), m_AwsDatabase.m_MinHumi);
			m_HistoryTable.SetItemText(iRow, i++, strTemp);
		}
		//气压
		if (*pTmpChar++ == _T('1'))
		{
			strTemp.Format(_T("%.2f"), m_AwsDatabase.m_CurBaro / 1000.0);
			m_HistoryTable.SetItemText(iRow, i++, strTemp);
		}
		if (*pTmpChar++ == _T('1'))
		{
			strTemp.Format(_T("%.2f"), m_AwsDatabase.m_AvgBaro / 1000.0);
			m_HistoryTable.SetItemText(iRow, i++, strTemp);
		}
		if (*pTmpChar++ == _T('1'))
		{
			strTemp.Format(_T("%.2f"), m_AwsDatabase.m_MaxBaro / 1000.0);
			m_HistoryTable.SetItemText(iRow, i++, strTemp);
		}
		if (*pTmpChar++ == _T('1'))
		{
			strTemp.Format(_T("%.2f"), m_AwsDatabase.m_MinBaro / 1000.0);
			m_HistoryTable.SetItemText(iRow, i++, strTemp);
		}
		//风速
		if (*pTmpChar++ == _T('1'))
		{
			strTemp.Format(_T("%.2f"), m_AwsDatabase.m_CurWindSpe / 100.0);
			m_HistoryTable.SetItemText(iRow, i++, strTemp);
		}
		if (*pTmpChar++ == _T('1'))
		{
			strTemp.Format(_T("%.2f"), m_AwsDatabase.m_AvgWindSpe / 100.0);
			m_HistoryTable.SetItemText(iRow, i++, strTemp);
		}
		if (*pTmpChar++ == _T('1'))
		{
			strTemp.Format(_T("%.2f"), m_AwsDatabase.m_MaxWindSpe / 100.0);
			m_HistoryTable.SetItemText(iRow, i++, strTemp);
		}
		if (*pTmpChar++ == _T('1'))
		{
			strTemp.Format(_T("%.2f"), m_AwsDatabase.m_MinWindSpe / 100.0);
			m_HistoryTable.SetItemText(iRow, i++, strTemp);
		}
		if (*pTmpChar++ == _T('1'))
		{
			strTemp.Format(_T("%.2f"), m_AwsDatabase.m_VectWindSpe / 100.0);
			m_HistoryTable.SetItemText(iRow, i++, strTemp);
		}
		//风向
		if (*pTmpChar++ == _T('1'))
		{
			strTemp.Format(_T("%.2f"), m_AwsDatabase.m_CurWindDir / 10.0);
			m_HistoryTable.SetItemText(iRow, i++, strTemp);
		}
		if (*pTmpChar++ == _T('1'))
		{
			strTemp.Format(_T("%.2f"), m_AwsDatabase.m_VectWindDir / 10.0);
			m_HistoryTable.SetItemText(iRow, i++, strTemp);
		}
		//指向下一行
		m_AwsDatabase.MoveNext();
		iRow++;
		//设置UI线程中的进度条
		if (iRow % (iDataCount / 20 + 1) == 0)	//将ProgressCtrl分成20份，即以5%的数量增加，+1防止被0除
		{
//			::PostMessage(hdlg, WM_PROGRESS_CHANGED, (WPARAM)2, (LPARAM)iRow);	//向UI线程的主对话框发送自定义消息,设置ProgressCtrl进度条当前位置（暂无法使用）
			pThread->PostThreadMessage(WM_THREAD_MESSAGES, (WPARAM)2, (LPARAM)iRow);	//向UI线程发送自定义消息, 设置ProgressCtrl进度条当前位置
//			pThread1->PostThreadMessage(WM_THREAD_MESSAGES, (WPARAM)2, (LPARAM)iRow);
		}

/*		//调试用，为了验证模态对话框会阻塞主窗口线程的运行！
		//（但不会阻塞当前线程的消息循环，亦即模态对话框与主对话框是并列关系而不是从属关系，它们均直接受控于当前线程）
		if (iRow == iDataCount / 10)
		{
			AfxMessageBox(L"阻塞！");	
		}
*/
	}
//	::PostMessage(hdlg, WM_PROGRESS_CHANGED, (WPARAM)3, (LPARAM)iDataCount);	//向UI线程的主对话框发送自定义消息,直接设置ProgressCtrl进度条为满状态（暂无法使用）
	pThread->PostThreadMessage(WM_THREAD_MESSAGES, (WPARAM)3, (LPARAM)iDataCount);	//向UI线程的主对话框发送自定义消息,直接设置ProgressCtrl进度条为满状态
//	pThread1->PostThreadMessage(WM_THREAD_MESSAGES, (WPARAM)3, (LPARAM)iDataCount);
	//数据库使用完后将其关闭
//	m_AwsDatabase.Close();	//关闭记录集
//	m_AwsDatabase.m_pDatabase->Close();	//关闭数据库

	m_HistoryTable.SetFocus();	//为了激活鼠标滚轮查看

	// Ensure that the last item is visible. 
	int nCount = m_HistoryTable.GetItemCount();
	if (nCount > 0)
		m_HistoryTable.EnsureVisible(nCount - 1, FALSE);
	
//	m_HistoryTable.SendMessage(WM_VSCROLL, SB_BOTTOM, NULL);	//备用，功能同上，滚动到最后一行

}


void CHistoryDlg::OnBnClickedBtnExportdata()
{
	// TODO:  在此添加控件通知处理程序代码
////确保列表不为空，否则后面方法二获取COleSafeArray二维数组元素个数会出现-1
	if (!m_HistoryTable.GetItemCount())	//如果列表为空，则提示并直接返回
	{
		AfxMessageBox(_T("列表为空，请先查询后再导出！"));
		return;
	}
////创建ProgressCtrl的UI线程，用于显示处理进度条
	CWinThread* pThread = AfxBeginThread(RUNTIME_CLASS(CProgressCtrlThread));
////创建Excel文件并打开至某一sheet
	//Workbooks—>Workbook —>Worksheets—>Worksheet —>Range   
	CApplication app;       //Excel应用程序接口   
	CWorkbooks books;       //工作薄集合   
	CWorkbook book;			//工作薄   
	CWorksheets sheets;     //工作表集合   
	CWorksheet sheet;       //工作表   
	CRange range;           //Excel中针对单元格的操作都应先获取其对应的Range对象   
	CFont0 font;
	CRange cols;
	LPDISPATCH lpDisp = NULL;
	
	/*
	COleVariant类为VARIANT数据类型的包装，在自动化程序中，通常都使用
	VARIANT数据类型进行参数传递。故下列程序中，函数参数都是通过COleVariant
	类来转换了的。
	*/
	//covOptional 可选参数的VARIANT类型   
//	COleVariant covOptional((long)DISP_E_PARAMNOTFOUND, VT_ERROR);	//【注】此处注释掉，直接用系统提供的_variant_t类型的vtMissing
	
	/*创建Excel 服务器(启动Excel)*/
	::PostThreadMessage(pThread->m_nThreadID, WM_THREAD_MESSAGES, (WPARAM)1, (LPARAM)100);	//Range设置成默认值
	::PostThreadMessage(pThread->m_nThreadID, WM_THREAD_MESSAGES, (WPARAM)4, (LPARAM)1);	//文字提示1：“Excel服务器启动中...”
	::PostThreadMessage(pThread->m_nThreadID, WM_THREAD_MESSAGES, (WPARAM)2, (LPARAM)10);	//进度：10%
	if (!app.CreateDispatch(_T("Excel.Application")))
	{
		this->MessageBox(_T("启动Excel服务器失败！"));
		return;
	}
	::PostThreadMessage(pThread->m_nThreadID, WM_THREAD_MESSAGES, (WPARAM)2, (LPARAM)30);	//进度：30%
	/*判断当前Excel的版本，为后面保存文件格式作准备*/
	XlFileFormat NewFileFormat;	//文件另存为的格式
	CString strExcelVersion = app.get_Version();
	int iStart = 0;
	strExcelVersion = strExcelVersion.Tokenize(_T("."), iStart);
	if (_T("11") == strExcelVersion)
	{
		NewFileFormat = xlExcel8;
	//	AfxMessageBox(_T("当前Excel的版本是2003。"));
		::PostThreadMessage(pThread->m_nThreadID, WM_THREAD_MESSAGES, (WPARAM)4, (LPARAM)2);	//文字提示2：“创建工作簿...\r\nExcel Version: 2003”
	}
	else if (_T("12") == strExcelVersion)
	{
		NewFileFormat = xlOpenXMLWorkbook;
	//	AfxMessageBox(_T("当前Excel的版本是2007。"));
		::PostThreadMessage(pThread->m_nThreadID, WM_THREAD_MESSAGES, (WPARAM)4, (LPARAM)3);	//文字提示3：“创建工作簿...\r\nExcel Version: 2007”
	}
	else if (_T("14") == strExcelVersion)
	{
		NewFileFormat = xlOpenXMLWorkbook;
	//	AfxMessageBox(_T("当前Excel的版本是2010。"));
		::PostThreadMessage(pThread->m_nThreadID, WM_THREAD_MESSAGES, (WPARAM)4, (LPARAM)4);	//文字提示4：“创建工作簿...\r\nExcel Version: 2010”
	}
	else if (_T("15") == strExcelVersion)
	{
		NewFileFormat = xlOpenXMLWorkbook;
	//	AfxMessageBox(_T("当前Excel的版本是2013。"));
		::PostThreadMessage(pThread->m_nThreadID, WM_THREAD_MESSAGES, (WPARAM)4, (LPARAM)5);	//文字提示5：“创建工作簿...\r\nExcel Version: 2013”
	}
	else
	{
		NewFileFormat = xlExcel8;
	//	AfxMessageBox(_T("当前Excel的版本是其他版本。"));
		::PostThreadMessage(pThread->m_nThreadID, WM_THREAD_MESSAGES, (WPARAM)4, (LPARAM)6);	//文字提示6：“创建工作簿...\r\nExcel Version: Other”
	}

	/*得到工作簿容器*/
	books.AttachDispatch(app.get_Workbooks());

	/*打开一个工作簿，如不存在，则新增一个工作簿*/
	CString strBookPath = _T("D:\\data.xls");
	try
	{
		/*打开一个工作簿*/
		lpDisp = books.Open(strBookPath,
			vtMissing, vtMissing, vtMissing, vtMissing, vtMissing,
			vtMissing, vtMissing, vtMissing, vtMissing, vtMissing,
			vtMissing, vtMissing, vtMissing, vtMissing);
		book.AttachDispatch(lpDisp);
	}
	catch (...)
	{
		/*增加一个新的工作簿*/
		lpDisp = books.Add(vtMissing);
		book.AttachDispatch(lpDisp);
	}


	/*得到工作簿中的Sheet的容器*/
	sheets.AttachDispatch(book.get_Sheets());

	/*打开一个Sheet，如不存在，就新增一个Sheet*/
	CString strSheetName = _T("NewSheet");
	try
	{
		/*打开一个已有的Sheet*/
		lpDisp = sheets.get_Item(_variant_t(strSheetName));
		sheet.AttachDispatch(lpDisp);
	}
	catch (...)
	{
		/*创建一个新的Sheet*/
		lpDisp = sheets.Add(vtMissing, vtMissing, _variant_t((long)1), vtMissing);
		sheet.AttachDispatch(lpDisp);
		sheet.put_Name(strSheetName);
	}
	::PostThreadMessage(pThread->m_nThreadID, WM_THREAD_MESSAGES, (WPARAM)2, (LPARAM)50);	//进度：50%
	::PostThreadMessage(pThread->m_nThreadID, WM_THREAD_MESSAGES, (WPARAM)4, (LPARAM)7);	//文字提示7：“写入数据...”
////以range的方式操作sheet
	CAutoWeatherStationDlg* pDlg = (CAutoWeatherStationDlg*)theApp.GetMainWnd();
	//ListCtrl中的表头导出至Excel
	CHeaderCtrl* pHeaderCtrl = pDlg->m_HistoryDlg.m_HistoryTable.GetHeaderCtrl();	//获取表头指针
	enum   { sizeOfBuffer = 256 };
	TCHAR  lpBuffer[sizeOfBuffer];
	HDITEM hdi;
	hdi.mask = HDI_TEXT;	//表头每列信息将被认为是字符串类型
	hdi.pszText = lpBuffer;	//存放字符串的指针
	hdi.cchTextMax = sizeOfBuffer;
	CString strRangFirst, strRangeLast;
	for (int i = 0; i < pHeaderCtrl->GetItemCount(); i++)
	{
		strRangFirst.Format(_T("%c1"), 'A' + i);
		strRangeLast = strRangFirst;
		range = sheet.get_Range(_variant_t(strRangFirst), _variant_t(strRangeLast));	//一个单元格一个单元格地选择
		pHeaderCtrl->GetItem(i, &hdi);
		range.put_Value2(_variant_t(hdi.pszText));	//赋值，注意put_Value2()与put_Value()参数数量不同
		//设置表头格式：粗体，居中
		//表头为粗体
		font = range.get_Font();
		font.put_Bold(_variant_t(TRUE));
		//对齐方式为水平居中
		range.put_HorizontalAlignment(_variant_t((long)-4108));   //水平对齐：默认＝1,居中＝-4108,左＝-4131,右＝-4152（默认即为左对齐）
	//	range.put_VerticalAlignment(_variant_t((long)-4107));    //垂直对齐：默认＝2,居中＝-4108,左＝-4160,右＝-4107 （默认即为居中）
	}
	::PostThreadMessage(pThread->m_nThreadID, WM_THREAD_MESSAGES, (WPARAM)2, (LPARAM)50);	//进度：60%

#define METHOD 2	//方法一与方法二选择开关，实测（2600行x10列）方法2（耗时7s(第一次),1.3s）比方法1（耗时24s(第一次),18s）快3~15倍
#if METHOD == 1
	//ListCtrl中的数据写入Excel【方法一】
	CString strText;
	for (int j = 0; j < pDlg->m_HistoryDlg.m_HistoryTable.GetItemCount(); j++)	//选中某一行
	{
		for (int i = 0; i < pHeaderCtrl->GetItemCount(); i++)	//选中一行中的某一列
		{
			strRangFirst.Format(_T("%c%d"), 'A' + i, 2 + j);	//注意不要覆盖掉表头，因此应当从第2行开始存储
			strRangeLast = strRangFirst;
			range = sheet.get_Range(_variant_t(strRangFirst), _variant_t(strRangeLast));	//一个单元格一个单元格地选择
			strText = pDlg->m_HistoryDlg.m_HistoryTable.GetItemText(j, i);	//获取列表控件中的第j行第i列数据
		#if 1
			range.put_Value2(_variant_t(strText));	//赋值，所有列均视为字符串类型
		#endif
		#if 0
			if (i < 2)
				range.put_Value2(_variant_t(strText));	//赋值，前两列视为字符串类型
			else
				range.put_Value2(_variant_t(_ttof(strText)));	//赋值，后面所有列全视为浮点数值类型
		#endif
		}
	}
#endif
#if METHOD == 2
	//ListCtrl中的数据转存至COleSafeArray类型的二维数组中，然后写入Excel【方法二，速度更快】
	/*数组元素为String字符串类型*/
	VARTYPE vt = VT_BSTR; 
	/*定义数组的维数和下标的起始值*/
	SAFEARRAYBOUND sabData[2];
	sabData[0].cElements = pDlg->m_HistoryDlg.m_HistoryTable.GetItemCount();
	sabData[0].lLbound = 0;
	sabData[1].cElements = pHeaderCtrl->GetItemCount();
	sabData[1].lLbound = 0;

	COleSafeArray olesaData;
	olesaData.Create(vt, sizeof(sabData) / sizeof(SAFEARRAYBOUND), sabData);

	/*通过指向数组的指针来对二维数组的元素进行间接赋值*/
//	long(*pArray)[2] = NULL;
//	olesaData.AccessData((void **)&pArray);
//	memset(pArray, 0, sabData[0].cElements * sabData[1].cElements * sizeof(long));

	/*释放指向数组的指针*/
//	olesaData.UnaccessData();
//	pArray = NULL;

	/*对二维数组的元素进行逐个赋值*/
	CString strElement;
	long index[2] = { 0, 0 };
	long lFirstLBound = 0;
	long lFirstUBound = 0;
	long lSecondLBound = 0;
	long lSecondUBound = 0;
	olesaData.GetLBound(1, &lFirstLBound);
	olesaData.GetUBound(1, &lFirstUBound);
	olesaData.GetLBound(2, &lSecondLBound);
	olesaData.GetUBound(2, &lSecondUBound);
	for (index[0] = lFirstLBound; index[0] <= lFirstUBound; index[0]++)
	{
		for (index[1] = lSecondLBound; index[1] <= lSecondUBound; index[1]++)
		{
			strElement = m_HistoryTable.GetItemText(index[0], index[1]);
			olesaData.PutElement(index, (BSTR)strElement.AllocSysString());	//将CString转换为BSTR类型，否则会提示内存不足
		}
	}
	::PostThreadMessage(pThread->m_nThreadID, WM_THREAD_MESSAGES, (WPARAM)2, (LPARAM)75);	//进度：75%

	/*把ColesaWritefeArray变量转换为VARIANT,并写入到Excel表格中*/
	strRangFirst.Format(_T("A2"));	//开始单元格固定
	strRangeLast.Format(_T("%c%d"), 'A' + lSecondUBound - lSecondLBound, 2 + lFirstUBound - lFirstLBound);	//结束单元格不固定（注意下标计算）
	range = sheet.get_Range(_variant_t(strRangFirst), _variant_t(strRangeLast));	//区域选中
	VARIANT varWrite = (VARIANT)olesaData;
	range.put_Value2(varWrite);
	//选中当前Range所在的所有整列，设置宽度为自动适应
	cols = range.get_EntireColumn();
	cols.AutoFit();
#endif
	::PostThreadMessage(pThread->m_nThreadID, WM_THREAD_MESSAGES, (WPARAM)2, (LPARAM)80);	//进度：85%
	::PostThreadMessage(pThread->m_nThreadID, WM_THREAD_MESSAGES, (WPARAM)4, (LPARAM)8);	//文字提示8：“打开表格...”
	::PostThreadMessage(pThread->m_nThreadID, WM_THREAD_MESSAGES, (WPARAM)2, (LPARAM)100);	//进度：100%
////保存文件并退出
	/*显示Excel表格，并设置状态为用户可控制*/
	app.put_Visible(TRUE);
	app.put_UserControl(TRUE);
	::PostThreadMessage(pThread->m_nThreadID, WM_THREAD_MESSAGES, (WPARAM)3, (LPARAM)100);	//进度：100%，并关闭进度条对话框

/*暂不自动保存
	//根据Excel版本，来选择保存文件的格式
	CString strSaveAsName = _T("D:\\new");	//扩展名会自动加上
	book.SaveAs(_variant_t(strSaveAsName), _variant_t((long)NewFileFormat), vtMissing, vtMissing, vtMissing,
		vtMissing, 0, vtMissing, vtMissing, vtMissing,
		vtMissing, vtMissing);
*/

	//释放对象（重要！）
	font.ReleaseDispatch();
	cols.ReleaseDispatch();
	range.ReleaseDispatch();
	sheet.ReleaseDispatch();
	sheets.ReleaseDispatch();
	book.ReleaseDispatch();
	books.ReleaseDispatch();
	//退出程序    
	app.Quit();
	//app一定要释放，否则程序结束后还会有一个Excel进程驻留在内存中，而且程序重复运行的时候会出错    
	app.ReleaseDispatch();
}



// Awsdata.h : CAwsdata 类的实现



// CAwsdata 实现

// 代码生成在 2014年9月22日, 10:44

#include "stdafx.h"
#include "Awsdata.h"
IMPLEMENT_DYNAMIC(CAwsdata, CRecordset)

CAwsdata::CAwsdata(CDatabase* pdb)
	: CRecordset(pdb)
{
	m_ID = 0;
	m_CmdRet = L"";
	m_Command = L"";
	m_AwsType = L"";
	m_DDate;
	m_TTime;
	m_CurTemp = 0;
	m_AvgTemp = 0;
	m_MaxTemp = 0;
	m_MinTemp = 0;
	m_CurHumi = 0;
	m_AvgHumi = 0;
	m_MaxHumi = 0;
	m_MinHumi = 0;
	m_CurBaro = 0;
	m_AvgBaro = 0;
	m_MaxBaro = 0;
	m_MinBaro = 0;
	m_CurWindSpe = 0;
	m_AvgWindSpe = 0;
	m_MaxWindSpe = 0;
	m_MinWindSpe = 0;
	m_VectWindSpe = 0;
	m_CurWindDir = 0;
	m_VectWindDir = 0;
	m_nFields = 25;
	m_nDefaultType = dynaset;
}
//#error 安全问题：连接字符串可能包含密码。
// 此连接字符串中可能包含明文密码和/或其他重要
// 信息。请在查看完此连接字符串并找到所有与安全
// 有关的问题后移除 #error。可能需要将此密码存
// 储为其他格式或使用其他的用户身份验证。
CString CAwsdata::GetDefaultConnect()
{
	CString strConnect, strPath;
//	strConnect =  _T("						\
		DSN=AwsData;						\
		DBQ=AwsData.mdb;					\
		DefaultDir=C:\\AutoWeatherStation;	\
		DriverId=25;						\
		FIL=MS Access;						\
		MaxBufferSize=2048;					\
		PageTimeout=5;						\
		UID=admin;							\
	");
	// User must select data source and supply user ID and password:
//	strConnect = _T("ODBC;");
	// User ID and password required:
//	strConnect = _T("ODBC;DSN=AwsData;");
	// Password required (myuserid must be replaced with a valid user ID):
//	strConnect = _T("ODBC;DSN=AwsData;UID=admin;");
	// Hard-coded user ID and password (SECURITY WEAKNESS--AVOID):
//	strConnect = _T("\
					DSN=AwsData;\
					UID=admin;PWD=admin;\
					DBQ=C:\\AutoWeatherStation\\AwsData.mdb;\
					");
	//获取当前程序路径
	TCHAR curPath[128];
	DWORD ret = ::GetCurrentDirectory(128, curPath);
	strPath.Format(_T("DBQ=%s\\AwsData.mdb;"), curPath);
	//直接从程序目录中查找数据库
	strConnect = _T("\
					DSN=AwsData;\
					UID=admin;PWD=admin;\
					")
					+ strPath;

	return strConnect;
}

CString CAwsdata::GetDefaultSQL()
{
	return _T("[Awsdata]");
}

void CAwsdata::DoFieldExchange(CFieldExchange* pFX)
{
	pFX->SetFieldType(CFieldExchange::outputColumn);
// RFX_Text() 和 RFX_Int() 这类宏依赖的是
// 成员变量的类型，而不是数据库字段的类型。
// ODBC 尝试自动将列值转换为所请求的类型
	RFX_Long(pFX, _T("[ID]"), m_ID);
	RFX_Text(pFX, _T("[CmdRet]"), m_CmdRet);
	RFX_Text(pFX, _T("[Command]"), m_Command);
	RFX_Text(pFX, _T("[AwsType]"), m_AwsType);
	RFX_Date(pFX, _T("[DDate]"), m_DDate);
	RFX_Date(pFX, _T("[TTime]"), m_TTime);
	RFX_Long(pFX, _T("[CurTemp]"), m_CurTemp);
	RFX_Long(pFX, _T("[AvgTemp]"), m_AvgTemp);
	RFX_Long(pFX, _T("[MaxTemp]"), m_MaxTemp);
	RFX_Long(pFX, _T("[MinTemp]"), m_MinTemp);
	RFX_Long(pFX, _T("[CurHumi]"), m_CurHumi);
	RFX_Long(pFX, _T("[AvgHumi]"), m_AvgHumi);
	RFX_Long(pFX, _T("[MaxHumi]"), m_MaxHumi);
	RFX_Long(pFX, _T("[MinHumi]"), m_MinHumi);
	RFX_Long(pFX, _T("[CurBaro]"), m_CurBaro);
	RFX_Long(pFX, _T("[AvgBaro]"), m_AvgBaro);
	RFX_Long(pFX, _T("[MaxBaro]"), m_MaxBaro);
	RFX_Long(pFX, _T("[MinBaro]"), m_MinBaro);
	RFX_Long(pFX, _T("[CurWindSpe]"), m_CurWindSpe);
	RFX_Long(pFX, _T("[AvgWindSpe]"), m_AvgWindSpe);
	RFX_Long(pFX, _T("[MaxWindSpe]"), m_MaxWindSpe);
	RFX_Long(pFX, _T("[MinWindSpe]"), m_MinWindSpe);
	RFX_Long(pFX, _T("[VectWindSpe]"), m_VectWindSpe);
	RFX_Long(pFX, _T("[CurWindDir]"), m_CurWindDir);
	RFX_Long(pFX, _T("[VectWindDir]"), m_VectWindDir);

}
/////////////////////////////////////////////////////////////////////////////
// CAwsdata 诊断

#ifdef _DEBUG
void CAwsdata::AssertValid() const
{
	CRecordset::AssertValid();
}

void CAwsdata::Dump(CDumpContext& dc) const
{
	CRecordset::Dump(dc);
}
#endif //_DEBUG




// VirtualMeterDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "AutoWeatherStation.h"
#include "AutoWeatherStationDlg.h"
#include "VirtualMeterDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define WM_REFRESH_VIRTUAL_METER	WM_USER + 3		//主对话框向Meter对话框发送新数据的自定义消息

// CVirtualMeterDlg 对话框

IMPLEMENT_DYNAMIC(CVirtualMeterDlg, CDialogEx)

CVirtualMeterDlg::CVirtualMeterDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CVirtualMeterDlg::IDD, pParent)
{

}

void CVirtualMeterDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_THERMOMETER, m_Thermometer);
//	DDX_Control(pDX, IDC_COMPASS, m_Compass);
	DDX_Control(pDX, IDC_METER_WIND_SPEED, m_MeterWindSpeed);
	DDX_Control(pDX, IDC_HUMIDIOMETER, m_Humidiometer);
	DDX_Control(pDX, IDC_BAROMETER, m_Barometer);
	DDX_Control(pDX, IDC_METER_WIND_DIR, m_MeterWindDir);
}

BEGIN_MESSAGE_MAP(CVirtualMeterDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_CTLCOLOR()
	ON_MESSAGE(WM_REFRESH_VIRTUAL_METER, &CVirtualMeterDlg::OnRefreshVirtualMeter)
	ON_WM_CLOSE()
END_MESSAGE_MAP()


// CVirtualMeterDlg 消息处理程序

BOOL CVirtualMeterDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO:  在此添加额外的初始化代码

	//初始化风速仪表
//	COLORREF m_colorNeedle = RGB(255, 128, 64);
	COLORREF m_colorNeedle = RGB(100, 28, 164);
	m_MeterWindSpeed.SetNeedleColor(m_colorNeedle);
	m_MeterWindSpeed.SetColorTick(TRUE);
	m_MeterWindSpeed.SetRange(0, 70);
	m_MeterWindSpeed.SetScaleDecimals(0);
	m_MeterWindSpeed.SetValueDecimals(2);
	m_MeterWindSpeed.SetTicks(10);
	m_MeterWindSpeed.SetSubTicks(7);
	CString str=_T("风速m/s");
	m_MeterWindSpeed.SetUnits(str);
	m_MeterWindSpeed.SetValueDecimals(1);	//风速1位小数
	m_MeterWindSpeed.UpdateNeedle(0.00f);

	//初始化温度仪表
	static int nCurVal = 0;
	m_Thermometer.SetScale(10, 10);
	m_Thermometer.SetAxis(50, -50, _T(""));
	m_Thermometer.SetData(10, 1);

	//初始化湿度仪表
	m_Humidiometer.SetScale(10, 10);
	m_Humidiometer.SetAxis(100, 0, _T(""));
	m_Humidiometer.SetData(10, 2);

	//初始化气压仪表
	m_Barometer.SetScale(10, 10);
	m_Barometer.SetAxis(150, 50, _T(""));
	m_Barometer.SetData(60, 3);

	//初始化罗盘（风向仪表）
//	m_Compass.EnableWindow(TRUE);
//	m_Compass.SetShow(TRUE);
//	m_Compass.SetAngle(36);
	COLORREF m_colorNeedle_d = RGB(255, 128, 64);
	m_MeterWindDir.SetNeedleColor(m_colorNeedle_d);
	m_MeterWindDir.SetColorTick(TRUE);
	m_MeterWindDir.SetRange(0, 360);
	m_MeterWindDir.SetScaleDecimals(0);
	m_MeterWindDir.SetValueDecimals(2);
	m_MeterWindDir.SetTicks(10);
	m_MeterWindDir.SetSubTicks(7);
	CString str_d = _T("风向°");
	m_MeterWindDir.SetUnits(str_d);
	m_MeterWindDir.SetValueDecimals(1);	//风向1位小数
	m_MeterWindDir.UpdateNeedle(0.00f);

	//初始化字体
	CFont* p_font = new CFont;
	p_font->CreateFont(36,		// nHeight
		0,			// nWidth
		0,			// nEscapement
		0,			// nOrientation
		FW_BOLD,	// nWeight
		FALSE,		// bItalic
		FALSE,		// bUnderline
		0,			// cStrikeOut
		ANSI_CHARSET,				// nCharSet
		OUT_DEFAULT_PRECIS,			// nOutPrecision
		CLIP_DEFAULT_PRECIS,		// nClipPrecision
		DEFAULT_QUALITY,			// nQuality
		DEFAULT_PITCH | FF_SWISS,	// nPitchAndFamily
		_T("Arial"));		// lpszFac
	((CStatic*)GetDlgItem(IDC_STATIC_STATION))->SetFont(p_font, FALSE);
	((CStatic*)GetDlgItem(IDC_STATIC_STATION))->SetWindowText(_T("MS50U000"));

////设置对话框图标
	HICON m_hIcon;
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	SetIcon(m_hIcon, TRUE); // Set big icon
	SetIcon(m_hIcon, FALSE); // Set small icon

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CVirtualMeterDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CRect rect;
		CPaintDC dc(this);
		GetClientRect(rect);
//		dc.FillSolidRect(rect, RGB(200, 250, 255));   //修改对话框背景颜色
	//	dc.FillSolidRect(rect, RGB(240, 240, 220));   //修改对话框背景颜色

		CDialogEx::OnPaint();
	}
}


HBRUSH CVirtualMeterDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	// TODO:  在此更改 DC 的任何特性
	if (nCtlColor == CTLCOLOR_STATIC)
	{
		//总体颜色设置
//		pDC->SetTextColor(RGB(50, 50, 200));  //字体颜色
		pDC->SetTextColor(RGB(50, 50, 100));
	//	pDC->SetBkColor(RGB(240, 240, 220));   //字体背景色
		//STATIC控件单独颜色设置
		switch (pWnd->GetDlgCtrlID())
		{
		case IDC_STATIC_TEMP:
		case IDC_STATIC_HUMI:
		case IDC_STATIC_BARO:
			pDC->SetTextColor(RGB(50, 50, 100));  //字体颜色
		//	pDC->SetBkColor(RGB(240, 240, 220));   //字体背景色
			break;
		case IDC_STATIC_STATION:
			pDC->SetTextColor(RGB(50, 100, 200));  //字体颜色
		default:
			break;
		}
	}
	if (nCtlColor == CTLCOLOR_EDIT)
	{
//		pDC->SetTextColor(RGB(150, 105, 200));  //字体颜色
		pDC->SetTextColor(RGB(0, 0, 255));
	//	pDC->SetBkColor(RGB(200, 250, 155));	//字体背景色  
	}
/*	
	if (nCtlColor == CTLCOLOR_BTN)
	{
		pDC->SetTextColor(RGB(100, 100, 255));  //字体颜色
		pDC->SetBkColor(RGB(200, 250, 155));   //字体背景色  
	}
*/
	// TODO:  如果默认的不是所需画笔，则返回另一个画笔
	return hbr;
}


afx_msg LRESULT CVirtualMeterDlg::OnRefreshVirtualMeter(WPARAM wParam, LPARAM lParam)
{
	int flag = (int)lParam;
	CString str, strSta;
	CArray<CString, CString>* arr;
	switch (flag)
	{
	case 1:
		str = *(CString*)wParam;
		((CStatic*)GetDlgItem(IDC_STATIC_STATION))->GetWindowText(strSta);	//获取台站号
		if (str != strSta)
		{
			((CStatic*)GetDlgItem(IDC_STATIC_STATION))->SetWindowText(str);	//显示新台站号
			//清空原数据
			//温度
			m_Thermometer.SetData(0, 1);
			((CStatic*)GetDlgItem(IDC_EDIT_TEMP_AVG))->SetWindowText(_T(""));
			((CStatic*)GetDlgItem(IDC_EDIT_TEMP_MAX))->SetWindowText(_T(""));
			((CStatic*)GetDlgItem(IDC_EDIT_TEMP_MIN))->SetWindowText(_T(""));
			//湿度
			m_Humidiometer.SetData(0, 2);
			((CStatic*)GetDlgItem(IDC_EDIT_HUMI_AVG))->SetWindowText(_T(""));
			((CStatic*)GetDlgItem(IDC_EDIT_HUMI_MAX))->SetWindowText(_T(""));
			((CStatic*)GetDlgItem(IDC_EDIT_HUMI_MIN))->SetWindowText(_T(""));
			//气压
			m_Barometer.SetData(100, 3);
			((CStatic*)GetDlgItem(IDC_EDIT_BARO_AVG))->SetWindowText(_T(""));
			((CStatic*)GetDlgItem(IDC_EDIT_BARO_MAX))->SetWindowText(_T(""));
			((CStatic*)GetDlgItem(IDC_EDIT_BARO_MIN))->SetWindowText(_T(""));
			//风速
			m_MeterWindSpeed.UpdateNeedle(0);
			((CStatic*)GetDlgItem(IDC_EDIT_WIND_AVG))->SetWindowText(_T(""));
			((CStatic*)GetDlgItem(IDC_EDIT_WIND_MAX))->SetWindowText(_T(""));
			((CStatic*)GetDlgItem(IDC_EDIT_WIND_MIN))->SetWindowText(_T(""));
			//风向
		//	m_Compass.SetAngle(0);
			m_MeterWindDir.UpdateNeedle(0);
			((CStatic*)GetDlgItem(IDC_EDIT_WIND_VECT))->SetWindowText(_T(""));			
		}
		break;
	case 2:
		arr = (CArray<CString, CString>*)wParam;
		((CStatic*)GetDlgItem(IDC_STATIC_STATION))->GetWindowText(strSta);	//获取台站号
		if (arr->GetAt(0) == strSta)	//判断数据中的台站名与从ListBox中双击选择的台站名是否吻合
		{
			//温度
			m_Thermometer.SetData((_ttoi(arr->GetAt(7)) - 5000) / 100.0, 1);
			str.Format(_T("%.1f"), (_ttoi(arr->GetAt(8)) - 5000) / 100.0);
			((CStatic*)GetDlgItem(IDC_EDIT_TEMP_AVG))->SetWindowText(str);
			str.Format(_T("%.1f"), (_ttoi(arr->GetAt(9)) - 5000) / 100.0);
			((CStatic*)GetDlgItem(IDC_EDIT_TEMP_MAX))->SetWindowText(str);
			str.Format(_T("%.1f"), (_ttoi(arr->GetAt(10)) - 5000) / 100.0);
			((CStatic*)GetDlgItem(IDC_EDIT_TEMP_MIN))->SetWindowText(str);
			//湿度
			m_Humidiometer.SetData(_ttoi(arr->GetAt(11)), 2);
			str.Format(_T("%d"), _ttoi(arr->GetAt(12)));
			((CStatic*)GetDlgItem(IDC_EDIT_HUMI_AVG))->SetWindowText(str);
			str.Format(_T("%d"), _ttoi(arr->GetAt(13)));
			((CStatic*)GetDlgItem(IDC_EDIT_HUMI_MAX))->SetWindowText(str);
			str.Format(_T("%d"), _ttoi(arr->GetAt(14)));
			((CStatic*)GetDlgItem(IDC_EDIT_HUMI_MIN))->SetWindowText(str);
			//气压
			m_Barometer.SetData(_ttoi(arr->GetAt(15)) / 1000.0, 3);
			str.Format(_T("%.1f"), _ttoi(arr->GetAt(16)) / 1000.0);
			((CStatic*)GetDlgItem(IDC_EDIT_BARO_AVG))->SetWindowText(str);
			str.Format(_T("%.1f"), _ttoi(arr->GetAt(17)) / 1000.0);
			((CStatic*)GetDlgItem(IDC_EDIT_BARO_MAX))->SetWindowText(str);
			str.Format(_T("%.1f"), _ttoi(arr->GetAt(18)) / 1000.0);
			((CStatic*)GetDlgItem(IDC_EDIT_BARO_MIN))->SetWindowText(str);
			//风速
			m_MeterWindSpeed.UpdateNeedle(_ttoi(arr->GetAt(19)) / 100.0);
			str.Format(_T("%.1f"), _ttoi(arr->GetAt(20)) / 100.0);
			((CStatic*)GetDlgItem(IDC_EDIT_WIND_AVG))->SetWindowText(str);
			str.Format(_T("%.1f"), _ttoi(arr->GetAt(21)) / 100.0);
			((CStatic*)GetDlgItem(IDC_EDIT_WIND_MAX))->SetWindowText(str);
			str.Format(_T("%.1f"), _ttoi(arr->GetAt(22)) / 100.0);
			((CStatic*)GetDlgItem(IDC_EDIT_WIND_MIN))->SetWindowText(str);
			//风向
			//m_Compass.SetAngle(_ttoi(arr->GetAt(23)) / 10.0);
			m_MeterWindDir.UpdateNeedle(_ttoi(arr->GetAt(23)) / 10.0);
			str.Format(_T("%.1f"), _ttoi(arr->GetAt(24)) / 10.0);
			((CStatic*)GetDlgItem(IDC_EDIT_WIND_VECT))->SetWindowText(str);
		}
		break;
	default:
		break;
	}

	return 0;
}


void CVirtualMeterDlg::OnClose()
{
	// TODO:  在此添加消息处理程序代码和/或调用默认值
//	CDialogEx::OnClose();
	ShowWindow(FALSE);	//使用默认方式，即隐藏（而不销毁）窗口
	CAutoWeatherStationDlg* pDlg = (CAutoWeatherStationDlg*)theApp.GetMainWnd();
	pDlg->m_toolbar.GetToolBarCtrl().CheckButton(ID_MENU_METER, FALSE);	//同步Toolbox中的按钮
}

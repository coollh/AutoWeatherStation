
// AutoWeatherStationDlg.h : 头文件
//

#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include "CnComm.h"
#include "ServerSocket.h"
#include "HistoryDlg.h"
#include "AwsdataStorage.h"
#include "VirtualMeterDlg.h"

// CAutoWeatherStationDlg 对话框
class CAutoWeatherStationDlg : public CDialogEx
{
// 构造
public:
	CAutoWeatherStationDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
	enum { IDD = IDD_AUTOWEATHERSTATION_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

private:
	LONG m_lLastSel;	// 存储上次更改字体颜色后的光标位置，方便下次查找
	//串口配置参数
	int m_nBaudrate;
	int m_nCheckbit;
	int m_nCom;
	int m_nDatabit;
	int m_nStopbit;
public:
	//定义连接方式标识；0：未连接，1：串口方式连接，2：网络方式连接
	int ConnectionModeFlag;
	CServerSocket* m_iSocket;	//服务器Socket
	CnComm m_com;	//CnComm串口类
	CButton m_BtnListen;
	CButton m_BtnStop;
	CButton m_BtnComopen;
	CButton m_BtnComclose;
	UINT m_uEditPort;
	CIPAddressCtrl m_IpAddressTcp;
	CHistoryDlg m_HistoryDlg;	//历史数据对话框
	afx_msg void OnBnClickedBtnListen();
	CServerSocket* GetServerSocket() const;
	CAwsdataStorage* m_pAwsdataStorage;		//数据存储类指针，建立一个初始化主对话框后一直存在的对象，直到程序退出
	afx_msg void OnBnClickedBtnStop();
	CRichEditCtrl m_RichEdit2Info;
	CListBox m_List;
	afx_msg void OnEnChangeRichedit2Info();
	afx_msg void OnBnClickedBtnOpenhistory();
	virtual void OnCancel();
	virtual void OnOK();
	afx_msg void OnClose();
	afx_msg void OnBnClickedBtnComopen();
	afx_msg void OnBnClickedBtnComclose();
protected:
	afx_msg LRESULT OnComReceive(WPARAM wParam, LPARAM lParam);
public:
	afx_msg void OnBnClickedButtonSend();
	UINT m_uEditRpt;
	CButton m_CheckRpt;
	afx_msg void OnBnClickedCheckRpt();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnSelchangeComboCmd();
	CComboBox m_ComboCmd;
	afx_msg void OnMenuModifyId();
	afx_msg void OnMenuInitializeClock();
	afx_msg void OnMenuNormalMode();
	afx_msg void OnMenuTestMode();
	afx_msg void OnMenuAutoServOpen();
	afx_msg void OnMenuAutoServClose();
	//事件标识，用于同步
	CEvent m_AutoServEvent;	//用于自动监测线程中，与主窗口中的OnTimer()配合实现定时阻塞
	CEvent m_AquireInfoEvent;	//用于自动监测线程中，获取主窗口ListBox控件信息成功后的同步
	afx_msg void OnBnClickedBtnCe();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnAbout();
	CStdioFile m_LogFile;	//网络接收日志log文件
protected:
	afx_msg LRESULT OnListboxInfo(WPARAM wParam, LPARAM lParam);
public:
	afx_msg void OnBnClickedBtnOpenMeter();
	CVirtualMeterDlg* m_pVMdlg;	//存放Meter对话框指针
	CString* m_pstrStation;	//存放台站名
	afx_msg void OnDblclkList();
	afx_msg void OnMove(int x, int y);
	CToolBar m_toolbar;
	CStatusBar m_statusbar;
	afx_msg BOOL OnToolTipNotify(UINT id, NMHDR * pNMHDR, LRESULT * pResult);
//	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnMenuCe();
	afx_msg void OnMenuHistory();
	afx_msg void OnMenuMeter();
};

// 台站时钟初始化线程
UINT ClockInitThread(LPVOID pParam);	//【注意】创建的线程函数为全局函数！

// 自动监测线程，注意此线程在OnInitDialog()中创建，且创建后不会退出
UINT AutoServThread(LPVOID pParam);
#pragma once

#include "Awsdata.h"
// CClientSocket 命令目标

class CClientSocket : public CSocket
{
public:
	CClientSocket(CPtrList* pList);
	virtual ~CClientSocket();
////成员变量
	CPtrList* m_pList;	//保存服务器ServerSocket中m_listSockets的内容
	CString m_strName;	//连接名称
////成员函数
	virtual void OnClose(int nErrorCode);
	virtual void OnReceive(int nErrorCode);
};



/*------------------------------------------------------------------
*	版权所有 (C) 2007～2009 西安众恒科技有限公司
*
*	文件名称：StThermometer.cpp
*
*	文件描述：模拟温度计
*
*	创 建 者：Z.G.Feng
*
*	版 本 号：Ver 1.0.0.0
*
*	创建日期：2009-03-11
 *------------------------------------------------------------------*/
#include "stdafx.h"
#include "MemDC.h"
#include "StThermometer.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CStThermometer
/*------------------------------------------------------------------
 *	函数名称：CStThermometer()
 *
 *	函数描述：构造函数
 *
 *	输入参数：void
 *
 *	输出参数：void
 *
 *	返 回 值：void
 *------------------------------------------------------------------*/
CStThermometer::CStThermometer()
{
	m_clrText = RGB(0,0,0);
	m_clrBackGround = RGB(240 ,255 ,255 );
	m_clrBorder = RGB( 0, 191, 255);
	m_clrWithin = RGB(175,238,212);

	m_dlMaxVal = 125.0;
	m_dlMinVal = 0.0;
	m_strUints = "℃";
	m_nTicks = 10;
	m_nSubTicks = 10;

	m_nAlrmVal = 85;
	m_nWarnVal = 50;

	m_flData = 0.0;
	text_data = 0.0;
}
/*------------------------------------------------------------------
 *	函数名称：~CStThermometer()
 *
 *	函数描述：析构函数
 *
 *	输入参数：void
 *
 *	输出参数：void
 *
 *	返 回 值：void
 *------------------------------------------------------------------*/
CStThermometer::~CStThermometer()
{
}

BEGIN_MESSAGE_MAP(CStThermometer, CStatic)
	//{{AFX_MSG_MAP(CStThermometer)
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CStThermometer message handlers
/*------------------------------------------------------------------
 *	函数名称：OnPaint()
 *
 *	函数描述：绘制函数
 *
 *	输入参数：void
 *
 *	输出参数：void
 *
 *	返 回 值：void
 *------------------------------------------------------------------*/
void CStThermometer::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	
	//获取控件区域
	GetClientRect(&m_rectCtrl);
	CMemDC memDC(&dc,&m_rectCtrl);

	//计算绘图区域
	m_rectDraw.top = m_rectCtrl.top + (m_rectCtrl.Height() - (m_rectCtrl.Height() % m_nTicks)) / 20;
	m_rectDraw.bottom = m_rectCtrl.bottom - (m_rectCtrl.Height() - (m_rectCtrl.Height() % m_nTicks)) / 20;
	m_rectDraw.left = m_rectCtrl.left + m_rectCtrl.Width() / 3;
	m_rectDraw.right = m_rectDraw.left + m_rectCtrl.Width() / 8;
	//绘制仪表盘
	if(m_dcBkGround.GetSafeHdc()== NULL|| (m_bmBkGround.m_hObject == NULL))
	{
		m_dcBkGround.CreateCompatibleDC(&dc);
		m_bmBkGround.CreateCompatibleBitmap(&dc, m_rectCtrl.Width(),m_rectCtrl.Height()) ;
		m_bmOldBkGround = m_dcBkGround.SelectObject(&m_bmBkGround) ;
		OnDrawBackGround(&m_dcBkGround);
	}
	memDC.BitBlt(0,0,m_rectCtrl.Width(),m_rectCtrl.Height(),&m_dcBkGround,0,0,SRCCOPY) ;
	
	//绘制刻度
	OnDrawScale(&memDC);
	//绘制限值线
//	OnDrawLimit(&memDC);
	//绘制数据
	OnDrawData(&memDC);
}
/*------------------------------------------------------------------
 *	函数名称：SetAxis()
 *
 *	函数描述：设置轴函数
 *
 *	输入参数：double				$ dlMax
 *			  double				$ dlMin
 *			  LPCTSTR				$ szUints
 *
 *	输出参数：void
 *
 *	返 回 值：void
 *------------------------------------------------------------------*/
void CStThermometer::SetAxis(double dlMax,double dlMin,LPCTSTR szUints)
{
	m_dlMaxVal = dlMax;
	m_dlMinVal = dlMin;
	m_strUints = szUints;
}
/*------------------------------------------------------------------
 *	函数名称：SetScale()
 *
 *	函数描述：设置刻度函数
 *
 *	输入参数：int					$ nTicks
 *			  int					$ nSubTicks
 *
 *	输出参数：void
 *
 *	返 回 值：void
 *------------------------------------------------------------------*/
void CStThermometer::SetScale(int nTicks,int nSubTicks)
{
	m_nTicks = nTicks;
	m_nSubTicks = nSubTicks;
}
/*------------------------------------------------------------------
 *	函数名称：SetLimit()
 *
 *	函数描述：设置限值函数
 *
 *	输入参数：int					$ nAlrm
 *			  int					$ nWarn
 *
 *	输出参数：void
 *
 *	返 回 值：void
 *------------------------------------------------------------------*/
void CStThermometer::SetLimit(int nAlrm,int nWarn)
{
	m_nAlrmVal = nAlrm;
	m_nWarnVal = nWarn;
}
/*------------------------------------------------------------------
 *	函数名称：SetData()
 *
 *	函数描述：设置数值函数
 *
 *	输入参数：float					$ flData flag 1代表温度，2代表湿度，3代表气压	Modified by: Lee，2015-05-06
 *
 *	输出参数：void
 *
 *	返 回 值：void
 *------------------------------------------------------------------*/
void CStThermometer::SetData(float flData,int flag)
{
	if (flag == 1)
	{
		text_data = flData;
		m_flData = flData + 50;
	}
	else if (flag == 2)
	{
		text_data = flData;
		m_flData = flData;
	}
	else if (flag == 3)
	{
		text_data = flData;
		m_flData = flData - 50;
	}
	OnRefurbish();
}
/*------------------------------------------------------------------
 *	函数名称：OnRefurbish()
 *
 *	函数描述：刷新函数
 *
 *	输入参数：void
 *
 *	输出参数：void
 *
 *	返 回 值：void
 *------------------------------------------------------------------*/
void CStThermometer::OnRefurbish()
{
	Invalidate(FALSE);
}
/*------------------------------------------------------------------
 *	函数名称：OnDrawBackGround()
 *
 *	函数描述：绘制背景函数
 *
 *	输入参数：CDC*					$ pDC
 *
 *	输出参数：void
 *
 *	返 回 值：void
 *------------------------------------------------------------------*/
void CStThermometer::OnDrawBackGround(CDC *pDC)
{
	CPen				BorderPen,
						*pOldPen;

	//填充背景颜色
	pDC->FillSolidRect(&m_rectCtrl,m_clrBackGround);

	//绘制内框
	BorderPen.CreatePen(PS_SOLID,6 + 6,m_clrWithin);
	pOldPen = pDC->SelectObject(&BorderPen);
	pDC->MoveTo(m_rectCtrl.left,m_rectCtrl.top);
	pDC->LineTo(m_rectCtrl.left,m_rectCtrl.bottom);
	pDC->LineTo(m_rectCtrl.right,m_rectCtrl.bottom);
	pDC->LineTo(m_rectCtrl.right,m_rectCtrl.top);
	pDC->LineTo(m_rectCtrl.left,m_rectCtrl.top);
	pDC->SelectObject(pOldPen);

	//绘制外框
	BorderPen.DeleteObject();
	BorderPen.CreatePen(PS_SOLID,6,m_clrBorder);
	pOldPen = pDC->SelectObject(&BorderPen);
	pDC->MoveTo(m_rectCtrl.left,m_rectCtrl.top);
	pDC->LineTo(m_rectCtrl.left,m_rectCtrl.bottom);
	pDC->LineTo(m_rectCtrl.right,m_rectCtrl.bottom);
	pDC->LineTo(m_rectCtrl.right,m_rectCtrl.top);
	pDC->LineTo(m_rectCtrl.left,m_rectCtrl.top);
	pDC->SelectObject(pOldPen);
}
/*------------------------------------------------------------------
 *	函数名称：OnDrawScale()
 *
 *	函数描述：绘制刻度函数
 *
 *	输入参数：CDC*					$ pDC
 *
 *	输出参数：void
 *
 *	返 回 值：void
 *------------------------------------------------------------------*/
void CStThermometer::OnDrawScale(CDC* pDC)
{
	CPen				BorderPen,
		                *pOldPen;
	CBrush				BackBrush,
						*pOldBrush;

	//创建画笔
	BorderPen.CreatePen(PS_SOLID,2,RGB(0,191,255));
	pOldPen = pDC->SelectObject(&BorderPen);
	BackBrush.CreateSolidBrush(m_clrBackGround);
	pOldBrush = pDC->SelectObject(&BackBrush);
	pDC->Rectangle(&m_rectDraw);
	pDC->SelectObject(pOldBrush);

	//绘制刻度
	double				dlSpace = 0.0,
						dlSubSpace = 0.0;
	CFont				ScaleFont,
						*pOldFont;
	CString				strLabel;

	ScaleFont.CreateFont(m_rectCtrl.Height() / 22,0,0,0,0,FALSE,FALSE,0,ANSI_CHARSET,
		OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,DEFAULT_PITCH | FF_ROMAN,_T("幼圆"));
	pOldFont = (CFont*)pDC->SelectObject(&ScaleFont);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextAlign(TA_LEFT);
	pDC->SetTextColor(RGB(0,0,0));//m_clrText
	dlSpace = m_rectDraw.Height() / m_nTicks;
	dlSubSpace = dlSpace / m_nSubTicks;
	for(int i = 0; i <= m_nTicks; i ++)
	{
		pDC->MoveTo(m_rectDraw.right,m_rectDraw.bottom - i * (int)dlSpace - 2);
		pDC->LineTo(m_rectDraw.right + 10,m_rectDraw.bottom - i * (int)dlSpace - 2);
		//绘制子刻度
		if(i < m_nTicks)
		{
			for(int j = 1; j < m_nSubTicks; j ++)
			{
				pDC->MoveTo(m_rectDraw.right,m_rectDraw.bottom - i * (int)dlSpace - 2 - j * (int)dlSubSpace);
				pDC->LineTo(m_rectDraw.right + 6,m_rectDraw.bottom - i * (int)dlSpace - 2 - j * (int)dlSubSpace);
			}
		}
		strLabel.Format(_T("%.0f"),m_dlMinVal + ((m_dlMaxVal - m_dlMinVal) / m_nTicks) * i);
		pDC->TextOut(m_rectDraw.right + 14,m_rectDraw.bottom - i * (int)dlSpace - 13,strLabel);
	}
	//输出单位符号
	pDC->TextOut(m_rectDraw.right + 20,m_rectDraw.bottom - 10,m_strUints);
	pDC->SelectObject(pOldFont);
	pDC->SelectObject(pOldPen);
}
/*------------------------------------------------------------------
 *	函数名称：OnDrawLimit()
 *
 *	函数描述：绘制限值函数
 *
 *	输入参数：CDC*					$ pDC
 *
 *	输出参数：void
 *
 *	返 回 值：void
 *------------------------------------------------------------------*/
void CStThermometer::OnDrawLimit(CDC* pDC)
{
	CPen				LimitPen,
						*pOldPen;
	CFont				LimitFont,
						*pOldFont;
	CString				strLabel;
	
	LimitFont.CreateFont(m_rectCtrl.Height() / 18,0,0,0,0,FALSE,FALSE,0,ANSI_CHARSET,
		OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,DEFAULT_PITCH | FF_ROMAN,_T("幼圆"));
	pOldFont = (CFont*)pDC->SelectObject(&LimitFont);
	pDC->SetBkMode(TRANSPARENT);
	//绘制预警线
	if(m_nWarnVal > 0)
	{
		LimitPen.CreatePen(PS_DOT,1,RGB(205 ,149 ,12 ));
		pOldPen = pDC->SelectObject(&LimitPen);
		pDC->MoveTo(m_rectDraw.right - 2 ,m_rectDraw.bottom - 2 - (int)((double)m_rectDraw.Height() / (m_dlMaxVal - m_dlMinVal) * (double)m_nWarnVal));
		pDC->LineTo(m_rectCtrl.left + 6,m_rectDraw.bottom - 2 - (int)((double)m_rectDraw.Height() / (m_dlMaxVal - m_dlMinVal) * (double)m_nWarnVal));
		pDC->SelectObject(pOldPen);
		strLabel.Format(_T("%d"),m_nWarnVal);
		pDC->SetTextAlign(TA_TOP);
		pDC->SetTextColor(RGB(205, 149, 12 ));
		pDC->TextOut(m_rectCtrl.left + 10,m_rectDraw.bottom - 2 - (int)((double)m_rectDraw.Height() / (m_dlMaxVal - m_dlMinVal) * (double)m_nWarnVal),strLabel);
	}
	//绘制报警线
	if(m_nAlrmVal > 0)
	{
		LimitPen.DeleteObject();
		LimitPen.CreatePen(PS_DOT,1,RGB(255,000,000));
		pOldPen = pDC->SelectObject(&LimitPen);
		pDC->MoveTo(m_rectDraw.right - 2 ,m_rectDraw.bottom - 2 - (int)((double)m_rectDraw.Height() / (m_dlMaxVal - m_dlMinVal) * (double)m_nAlrmVal));
		pDC->LineTo(m_rectCtrl.left + 6,m_rectDraw.bottom - 2 - (int)((double)m_rectDraw.Height() / (m_dlMaxVal - m_dlMinVal) * (double)m_nAlrmVal));
		pDC->SelectObject(pOldPen);
		strLabel.Format(_T("%d"),m_nAlrmVal);
		pDC->SetTextAlign(TA_BOTTOM);
		pDC->SetTextColor(RGB(255,000,000));
		pDC->TextOut(m_rectCtrl.left + 10,m_rectDraw.bottom - 2 - (int)((double)m_rectDraw.Height() / (m_dlMaxVal - m_dlMinVal) * (double)m_nAlrmVal),strLabel);
	}
	pDC->SelectObject(pOldFont);
}
/*------------------------------------------------------------------
 *	函数名称：OnDrawData()
 *
 *	函数描述：绘制数据函数
 *
 *	输入参数：CDC*					$ pDC
 *
 *	输出参数：void
 *
 *	返 回 值：void
 *------------------------------------------------------------------*/
void CStThermometer::OnDrawData(CDC* pDC)
{
	CRect				rcOrder;
	//					rcWarn,
	//					rcAlrm;
	/*
	if(m_flData >= m_nAlrmVal)
	{
		rcOrder.left = m_rectDraw.left + 1;
		rcOrder.top = m_rectDraw.bottom - 2 - (int)((double)m_rectDraw.Height() / (m_dlMaxVal - m_dlMinVal) * (double)m_nWarnVal);
		rcOrder.right = m_rectDraw.right - 2;
		rcOrder.bottom = m_rectDraw.bottom - 2;

		rcWarn.left = m_rectDraw.left + 1;
		rcWarn.top = m_rectDraw.bottom - 2 - (int)((double)m_rectDraw.Height() / (m_dlMaxVal - m_dlMinVal) * (double)m_nAlrmVal);
		rcWarn.right = m_rectDraw.right - 2;
		rcWarn.bottom = rcOrder.top;

		rcAlrm.left = m_rectDraw.left + 1;
		rcAlrm.top = m_rectDraw.bottom - 2 - (int)((double)m_rectDraw.Height() / (m_dlMaxVal - m_dlMinVal) * (double)m_flData);
		rcAlrm.right = m_rectDraw.right - 2;
		rcAlrm.bottom = rcWarn.top;

		pDC->FillSolidRect(&rcOrder,RGB(000,255,000));
		pDC->FillSolidRect(&rcWarn,RGB(255,255,000));
		pDC->FillSolidRect(&rcAlrm,RGB(255,000,000));
	}
	else if(m_flData >= m_nWarnVal && m_flData < m_nAlrmVal)
	{
		rcOrder.left = m_rectDraw.left + 1;
		rcOrder.top = m_rectDraw.bottom - 2 - (int)((double)m_rectDraw.Height() / (m_dlMaxVal - m_dlMinVal) * (double)m_nWarnVal);
		rcOrder.right = m_rectDraw.right - 2;
		rcOrder.bottom = m_rectDraw.bottom - 2;
		
		rcWarn.left = m_rectDraw.left + 1;
		rcWarn.top = m_rectDraw.bottom - 2 - (int)((double)m_rectDraw.Height() / (m_dlMaxVal - m_dlMinVal) * (double)m_flData);
		rcWarn.right = m_rectDraw.right - 2;
		rcWarn.bottom = rcOrder.top;

		pDC->FillSolidRect(&rcOrder,RGB(000,255,000));
		pDC->FillSolidRect(&rcWarn,RGB(255,255,000));
	}
	else if(m_flData > 0 && m_flData < m_nWarnVal)
	{
		rcOrder.left = m_rectDraw.left + 1;
		rcOrder.top = m_rectDraw.bottom - 2 - (int)((double)m_rectDraw.Height() / (m_dlMaxVal - m_dlMinVal) * (double)m_flData);
		rcOrder.right = m_rectDraw.right - 2;
		rcOrder.bottom = m_rectDraw.bottom - 2;
		pDC->FillSolidRect(&rcOrder,RGB(000,255,000));
	}*/         
	rcOrder.left = m_rectDraw.left + 1;
	rcOrder.top = m_rectDraw.bottom - 2 - (int)((double)m_rectDraw.Height() / (m_dlMaxVal - m_dlMinVal) * ((double)m_flData));
	rcOrder.right = m_rectDraw.right - 2;
	rcOrder.bottom = m_rectDraw.bottom - 2;
	pDC->FillSolidRect(&rcOrder,RGB(227,13,23));


	CString strtemp;
	CFont m_font,*pFontOld;

	//  数值显示
	strtemp.Format(_T("%.1f"), text_data); 
	m_font.CreateFont (m_rectDraw.Height()/18, 0, 0, 0, 400,
						FALSE, FALSE, 0, ANSI_CHARSET,
						OUT_DEFAULT_PRECIS, 
						CLIP_DEFAULT_PRECIS,
						DEFAULT_QUALITY, 
						DEFAULT_PITCH|FF_SWISS, _T("Arial"));
	pFontOld = pDC->SelectObject(&m_font);
	pDC->SetTextColor(RGB(25,25,112));
	pDC->SetTextAlign(TA_TOP|TA_CENTER);
	pDC->TextOut(m_rectDraw.right, m_rectDraw.bottom, strtemp);
	// 恢复字体和背景色
	pDC->SelectObject(pFontOld);

}
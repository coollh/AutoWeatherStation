// ModifyIdDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "AutoWeatherStation.h"
#include "AutoWeatherStationDlg.h"
#include "ModifyIdDlg.h"
#include "afxdialogex.h"
#include "ServerSocket.h"
#include "ClientSocket.h"


//自定义消息
#define WM_ID_INFO_RECEIVED		WM_USER + 1	//台站扫描线程每扫描到一个台站便会向本对话框发送一次消息

//Timer定时器响应宏定义
#define TIMERID_CHANGEID		1

// CModifyIdDlg 对话框

IMPLEMENT_DYNAMIC(CModifyIdDlg, CDialogEx)

CModifyIdDlg::CModifyIdDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CModifyIdDlg::IDD, pParent)
{

}

CModifyIdDlg::~CModifyIdDlg()
{
}

void CModifyIdDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_STATION_REPORT, m_ListStnRepo);
	DDX_Control(pDX, IDC_COMBO_STATION_LIST, m_ComboStnList);
	DDX_Control(pDX, IDC_COMBO_CURRENT_ID, m_ComboCurrentId);
	DDX_Control(pDX, IDC_EDIT_CHANGE_ID, m_EditChangeId);
}


BEGIN_MESSAGE_MAP(CModifyIdDlg, CDialogEx)
	ON_BN_CLICKED(IDOK, &CModifyIdDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_BTN_START_SCAN, &CModifyIdDlg::OnBnClickedBtnStartScan)
//	ON_WM_TIMER()
	ON_MESSAGE(WM_ID_INFO_RECEIVED, &CModifyIdDlg::OnIdInfoReceived)
//	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_STATION_REPORT, &CModifyIdDlg::OnLvnItemchangedListStationReport)
	ON_NOTIFY(LVN_ITEMACTIVATE, IDC_LIST_STATION_REPORT, &CModifyIdDlg::OnLvnItemActivateListStationReport)
	ON_BN_CLICKED(IDC_BTN_CHANGE_ID, &CModifyIdDlg::OnBnClickedBtnChangeId)
	ON_BN_CLICKED(IDC_BTN_REGISTER_NEW, &CModifyIdDlg::OnBnClickedBtnRegisterNew)
	ON_BN_CLICKED(IDC_BTN_ASSIGN_NEW, &CModifyIdDlg::OnBnClickedBtnAssignNew)
END_MESSAGE_MAP()


// CModifyIdDlg 消息处理程序


BOOL CModifyIdDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  在此添加额外的初始化
////初始化表格
	//设置列表控件风格	
	m_ListStnRepo.SetExtendedStyle(
		//		LVS_EX_AUTOSIZECOLUMNS |		//Automatically size LISTVIEW columns.
		//		LVS_EX_FLATSB |					//扁平风格显示滚动条
		LVS_EX_FULLROWSELECT |			//整行选中
		//		LVS_EX_HEADERDRAGDROP |			//表头可拖动
		//		LVS_EX_TRACKSELECT |			//选中跟踪
		LVS_EX_GRIDLINES |				//画出网格线
		//		LVS_EX_BORDERSELECT |			//Changes border color when an item is selected, instead of highlighting the item.
		//		LVS_EX_CHECKBOXES |				//行头添加CheckBox
		//		LVS_EX_HEADERINALLVIEWS |		//显示列头，Show column headers in all view modes.
		//		LVS_EX_COLUMNOVERFLOW |			//Indicates that an overflow button should be displayed in icon/tile view if there is not enough client width to display the complete set of header items.
		//		LVS_EX_TRANSPARENTBKGND |		//表格背景透明
		//		LVS_EX_ONECLICKACTIVATE |		//单击激活（鼠标划过时变为手型，单击时响应LVN_ITEMACTIVATE事件）
		//		LVS_EX_TWOCLICKACTIVATE |		//双击激活（鼠标单击时变为手型，再单击则响应LVN_ITEMACTIVATE事件）
		//		LVS_EX_UNDERLINECOLD |			//选中时带有文字下划线
		//		LVS_EX_UNDERLINEHOT |			//选中时带有文字下划线，离开时颜色变浅
		NULL							//占位，便于测试各个参数功能
		);
	//插入列
	m_ListStnRepo.InsertColumn(0, _T("台站类型"), LVCFMT_LEFT, 80);
	m_ListStnRepo.InsertColumn(1, _T("ID"), LVCFMT_LEFT, 50);
	m_ListStnRepo.InsertColumn(2, _T("网络地址"), LVCFMT_LEFT, 120);
	m_ListStnRepo.InsertColumn(3, _T("备注"), LVCFMT_LEFT, 100);
////初始化Edit控件
	m_EditChangeId.LimitText(3);	//限制修改ID的Edit控件只可以输入3个字符
////设置对话框图标
	HICON m_hIcon;
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	SetIcon(m_hIcon, TRUE); // Set big icon
	SetIcon(m_hIcon, FALSE); // Set small icon


	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常:  OCX 属性页应返回 FALSE
}


BOOL CModifyIdDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO:  在此添加专用代码和/或调用基类
	if ((GetFocus() == &m_EditChangeId) && (pMsg->message == WM_CHAR))	//若焦点在Edit控件上
	{
		if (pMsg->wParam <= '9' && pMsg->wParam >= '0' || pMsg->wParam == VK_BACK)	//屏蔽除数字键和退格键之外的所有按键
		{
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
	return CDialogEx::PreTranslateMessage(pMsg);
}


void CModifyIdDlg::OnBnClickedOk()
{
	// TODO:  在此添加控件通知处理程序代码
	CDialogEx::OnOK();
}


void CModifyIdDlg::OnBnClickedBtnStartScan()
{
	// TODO:  在此添加控件通知处理程序代码
////将ProcessFlag置为2（扫描已有台站）
	CAutoWeatherStationDlg* pDlg = (CAutoWeatherStationDlg*)theApp.GetMainWnd();
	pDlg->m_pAwsdataStorage->m_ProcessFlag = 2;
////清空列表
	m_ListStnRepo.DeleteAllItems();
	// Delete every other item from the list box.
	int count = pDlg->m_List.GetCount();
	for (int i = 0; i <= count; i++)	//【注意】此处有问题，虽然功能实现了，但是删除所有列需要多删除一次，似乎是控件的bug，待完善
	{
		pDlg->m_List.DeleteString(0);	//注意，参数不是i
	}
////创建扫描线程
	CString* pCString = new CString;	//在堆中分配一个CString类型的空间用以存放站点扫描线程中当前扫描台站信息
	CWinThread* pThread = AfxBeginThread(StationScanThread, pCString);

//	SetTimer(TIMERID_CHANGEID, 500, NULL);	//扫描间隔，【注意】SetTimer函数定时最小间隔为18ms左右，并且低于55ms的定时可能导致丢失或不精确
	
}


void CModifyIdDlg::OnBnClickedBtnChangeId()
{
	// TODO:  在此添加控件通知处理程序代码
////将ProcessFlag置为6（修改已有台站ID）
	CAutoWeatherStationDlg* pDlg = (CAutoWeatherStationDlg*)theApp.GetMainWnd();
	pDlg->m_pAwsdataStorage->m_ProcessFlag = 6;
////约束条件，并确保命令格式正确
	CString strStn, strCur, strChg, strCombin;
	m_ComboStnList.GetWindowText(strStn);
	m_ComboCurrentId.GetWindowText(strCur);
	m_EditChangeId.GetWindowText(strChg);
	if (strChg.GetLength() == 0)
	{
		AfxMessageBox(_T("请输入新ID！"));
	}
	else if (strChg.GetLength() == 1)
	{
		strChg = _T("00") + strChg;
	}
	else if (strChg.GetLength() == 2)
	{
		strChg = _T("0") + strChg;
	}
	else	//如果已经是3位数，则不进行任何操作（注意Edit控件中已经限制其最多只能输入3个数字，并且无法输入字母）
	{
		;
	}
	strCombin = strStn + strCur + _T(" CHG_ID ") + strStn + strChg + _T("\r\n");
////发送命令
	CStringA stra(strCombin);	//转为非UNICODE
	pDlg->m_com.Write(stra);
}


LRESULT CModifyIdDlg::OnIdInfoReceived(WPARAM SeqNum, LPARAM pCstring)
{
	//列出扫描到的台站
	CString* pstr = (CString*)pCstring;
	m_ListStnRepo.InsertItem(SeqNum, _T(""));
	m_ListStnRepo.SetItemText(SeqNum, 0, pstr->Left(5));
	m_ListStnRepo.SetItemText(SeqNum, 1, pstr->Mid(5, 3));
	m_ListStnRepo.SetItemText(SeqNum, 2, _T("0.0.0.0"));
	m_ListStnRepo.SetItemText(SeqNum, 3, _T("五要素气象站"));
	//扫描到的台站信息显示在主窗口台站列表中
	CAutoWeatherStationDlg* pDlg = (CAutoWeatherStationDlg*)theApp.GetMainWnd();
	pDlg->m_List.AddString(pstr->Left(8));
	return 0;
}


//默认情况下双击某个item则激活此消息，可利用SetExtendedStyle()函数更改
void CModifyIdDlg::OnLvnItemActivateListStationReport(NMHDR *pNMHDR, LRESULT *pResult)	
{
	LPNMITEMACTIVATE pNMIA = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO:  在此添加控件通知处理程序代码
	//获取当前激活的行号
	m_SelRow = m_ListStnRepo.GetSelectionMark();
	//当前选择的台站型号和台站ID存入ComboBox中
	CString str;
	str = m_ListStnRepo.GetItemText(m_SelRow, 0);
	m_ComboStnList.SetWindowText(str);
	str = m_ListStnRepo.GetItemText(m_SelRow, 1);
	m_ComboCurrentId.SetWindowText(str);

	*pResult = 0;
}


void CModifyIdDlg::OnBnClickedBtnRegisterNew()
{
	// TODO:  在此添加控件通知处理程序代码
////将ProcessFlag置为3（增加新台站）
	CAutoWeatherStationDlg* pDlg = (CAutoWeatherStationDlg*)theApp.GetMainWnd();
	pDlg->m_pAwsdataStorage->m_ProcessFlag = 3;
////扫描新台站
	//删除上次扫描结果
	SetDlgItemText(IDC_EDIT_NEW_STATION, _T(""));
	//向串口写入数据
	pDlg->m_com.Write("MS50U000 ST_CLK\r\n");
	//切换状态提示信息
	SetDlgItemText(IDC_STATIC_SCAN_NEW, _T("已扫描..."));
}


void CModifyIdDlg::OnBnClickedBtnAssignNew()
{
	// TODO:  在此添加控件通知处理程序代码
////将ProcessFlag置为5（修改新台站ID）
	CAutoWeatherStationDlg* pDlg = (CAutoWeatherStationDlg*)theApp.GetMainWnd();
	pDlg->m_pAwsdataStorage->m_ProcessFlag = 5;
////约束条件，确保命令格式正确
	CString strStation;
	int iID = 0;
	BOOL err;
	GetDlgItemText(IDC_EDIT_NEW_STATION, strStation);
	if (strStation == _T(""))
	{
		AfxMessageBox(_T("请先执行新台站扫描！"));
		return;
	}
	iID = GetDlgItemInt(IDC_EDIT_NEW_ID, &err, 1);	//err为非0则函数正常返回，为0则说明检测到非数字或者数字超过有效范围
	if (!err)	//新ID输入框中检测到非数字，则提示更改
	{
		AfxMessageBox(_T("设置失败，分配ID时请只输入数字即可。"));
		return;
	}

	if (iID == 0 || iID >= 1000)
	{
		AfxMessageBox(_T("设置失败，新ID应为1~999之间的数字！"));
		return;
	}
////发送命令
	CString strID;
	strID.Format(_T("%03d"), iID);	//数字格式化为字符串
	strStation = strStation + _T(" CHG_ID ") + strStation.Left(5) + strID + _T("\r\n");
	CStringA stra(strStation);	//转为非UNICODE
	pDlg->m_com.Write(stra);
}


//void CModifyIdDlg::OnLvnItemchangedListStationReport(NMHDR *pNMHDR, LRESULT *pResult)
//{
//	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
//	// TODO:  在此添加控件通知处理程序代码
//	MessageBox(_T("test"));
//	int sm = m_ListStnRepo.GetSelectionMark();
//	CString str;
//	str = m_ListStnRepo.GetItemText(sm, 0);
//	m_ComboStnList.SetWindowText(str);
//	str = m_ListStnRepo.GetItemText(sm, 1);
//	m_ComboCurrentId.SetWindowText(str);
//	*pResult = 0;
//}


//void CModifyIdDlg::OnTimer(UINT_PTR nIDEvent)
//{
//	// TODO:  在此添加消息处理程序代码和/或调用默认值
//	switch (nIDEvent)
//	{
//	case TIMERID_CHANGEID:
//
//		break;
//	default:
//		break;
//	}
//
//	CDialogEx::OnTimer(nIDEvent);
//}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//子线程，全局函数
//
//已存在台站扫描线程
UINT StationScanThread(LPVOID pParam)
{
	CAutoWeatherStationDlg* pDlg = (CAutoWeatherStationDlg*)theApp.GetMainWnd();
	HWND hMIDdlg = FindWindow(NULL, _T("更改台站ID"));	//获取子对话框句柄
	CString* pCstring = (CString*)pParam;
	//扫描过程中禁止点击扫描按钮,同时文字提示"扫描中"
	HWND hCtrlBtn = ::GetDlgItem(hMIDdlg, IDC_BTN_START_SCAN);
	::EnableWindow(hCtrlBtn, FALSE);
	HWND hCtrlSta = ::GetDlgItem(hMIDdlg, IDC_STATIC_SCAN);
	::SetWindowText(hCtrlSta, _T("登记中~"));
////开始扫描
	CString str, strTemp;
	str = _T("MS50U000 ST_CLK\r\n");	//【注】这里故意将ST_CLK后面的时间去掉，使得台站反馈回来Please check the time!信息即可表示扫描到对应台站而又不重新设置时间
	static int SeqNum;	//用于保存扫描到的台站序号
	SeqNum = 0;
	for (int i = 0; i < 20; i++)
	{
		strTemp.Format(_T("%03d"), i + 1);
		str.Replace(str.Mid(5, 3), strTemp);	//仅替换编号
		//Write串口需要传送StringA格式的字串
		CStringA stra(str);
		pDlg->m_com.Write(stra);
		//从Config.ini文件中恢复上次保存的【空ID扫描数上限】和【已存在台站扫描超时时间】
		UINT uCount, uScanWaitTime;
		uCount = GetPrivateProfileInt(_T("Global"), _T("TmOutCount"), 5, _T(".\\Config.ini"));	//注意此函数直接返回UINT类型,默认返回5
		uScanWaitTime = GetPrivateProfileInt(_T("Global"), _T("ScanWaitTime"), 600, _T(".\\Config.ini"));	//注意此函数直接返回UINT类型,默认返回600
		//台站扫描超时判定
		static int TmOutCount = uCount;	//连续空ID超过uCount数量即停止扫描
		if (::WaitForSingleObject(pDlg->m_pAwsdataStorage->m_ProcessEvent, uScanWaitTime) != WAIT_OBJECT_0)	//超时时间600ms
		{
			if (--TmOutCount == 0)
			{
				TmOutCount = 5;
				break;	//直接跳出for循环
			}
		}
		else	//未超时，说明发送的命令有回信，且PreProcess()成功处理，于是向ModifyIdDlg对话框发送带参数的自定义消息，并在消息相应函数中将台站信息显示在ListCtrl控件中
		{
			*pCstring = str;
			::PostMessage(hMIDdlg, WM_ID_INFO_RECEIVED, SeqNum, (LPARAM)pCstring);	//函数不等待消息处理而是直接返回，注意与SendMessage的区别
			SeqNum++;
		}
	}
	delete pParam;	//扫描结束，释放掉创建线程之前分配的CString型内存
	::EnableWindow(hCtrlBtn, TRUE);	//重新允许点击扫描按钮
	::SetWindowText(hCtrlSta, _T("完成！"));
	return 0;
}






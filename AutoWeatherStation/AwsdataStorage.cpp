#include "stdafx.h"
#include "AwsdataStorage.h"
#include "AutoWeatherStation.h"
#include "AutoWeatherStationDlg.h"
#include "ModifyIdDlg.h"

#define WM_REFRESH_VIRTUAL_METER	WM_USER + 3		//主对话框向Meter对话框发送新数据的自定义消息

CAwsdataStorage::CAwsdataStorage()
{

	m_pAwsdata = new CAwsdata;	//在构造函数中创建操作数据库的实例，确保只创建一次，提高效率，减少内存碎片
}


CAwsdataStorage::~CAwsdataStorage()
{
	//释放内存空间，防止内存泄漏
	delete m_pAwsdata;	//析构函数中销毁操作数据库的实例
	m_pAwsdata = NULL;
}


// 将接收到的CString类型的数据分解成数据库中的元素，保存到一个CArray模板数组中
BOOL CAwsdataStorage::AnalyzeString(CString strReceive, CArray<CString, CString>& arrElement)
{
//	TRACE(strReceive);	//调试用
	////拆分算法：查找空格的位置，将两个空格之间的字符存入模板数组
	int iStart = 0, iEnd = 0;
	CString strTemp;
	while (iEnd != -1)	//若前一次找到空格符，则继续循环查找
	{
		iEnd = strReceive.Find(_T(' '), iStart);	//从前一次找到的空格符的后一位开始查找（避免重复）
		if (iEnd != -1)	//如果找到空格
		{
			strTemp = strReceive.Mid(iStart, iEnd - iStart);	//得到两个空格符之间的字符串
			arrElement.Add(strTemp);							//加入数组中
			iStart = iEnd + 1;									//下次搜索的起始位置
		}
		else	//如果找不到空格（即已经找完了所有空格）
		{
			strTemp = strReceive.Right(strReceive.GetLength() - iStart);	//得到最后一个空格符之后的字符串
			arrElement.Add(strTemp);										//加入数组中
		}
	}
	////检查原字符串是否拆分完（以发现\r\n为准）
	int i;
	CString& strLast = arrElement.GetAt(arrElement.GetUpperBound());	//数组最后一个元素映射到strLast中（注意要加引用符号&，否则无法操作原始数据！）
	i = strLast.Find(_T("\r\n"));
	if (i != -1)	//如果在数组的最后一个元素中找到了\r\n，则将其删除并返回TRUE，否则返回FALSE
	{
		strLast.Delete(i, 2);	//删除第i和第i+1两个字符
		return TRUE;
	}
	else
		return FALSE;
}


//预处理函数，所有从下位机台站反馈回来的信息将首先在这里进行过滤和处理
bool CAwsdataStorage::PreProcess(CString strText)
{
////获取主对话框指针，方便后面操作控件
	CAutoWeatherStationDlg* pDlg = (CAutoWeatherStationDlg*)theApp.GetMainWnd();
////根据m_ProcessFlag标志位判断对接收到的数据进行何种处理
	//普通模式下，接收到的是数据，不用预处理，直接将数据显示出来后，返回false
	if (m_ProcessFlag == 0)
	{
//		pDlg->m_RichEdit2Info.ReplaceSel(strText);	//【调试用】
		//由于数据过长，可能需要分几次接收，因此首先将数据合并，直到找到\r\n结束符
		if (strText.Find(_T("\r\n")) == -1)	//没找到\r\n，则将数据合并，并直接返回TRUE
		{
//			pDlg->m_RichEdit2Info.ReplaceSel(_T("->本次接收m_strDataMerge：") + m_strDataMerge + _T("<-"));	//【调试用】
			m_strDataMerge += strText;
//			pDlg->m_RichEdit2Info.ReplaceSel(_T("->本次接收m_strDataMerge：") + m_strDataMerge + _T("<-"));	//【调试用】
			return true;
		}
		else	//找到\r\n，说明一串数据真正结束，则将字串合并后赋回给strText
		{
//			pDlg->m_RichEdit2Info.ReplaceSel(_T("->本次接收m_strDataMerge：") + m_strDataMerge + _T("<-"));	//【调试用】
			m_strDataMerge += strText;
//			pDlg->m_RichEdit2Info.ReplaceSel(_T("->本次接收m_strDataMerge：") + m_strDataMerge + _T("<-"));	//【调试用】
//！错误！	strText = m_strDataMerge;	//【！注意，局部变量不可以这样用，strText出了此函数也不会发生任何变化！】
			return false;
		}
	}
	//调试模式下，有命令出错控制，且控制命令的返回直接显示
	if (m_ProcessFlag == 1)
	{
		//若命令错误，则弹出对话框并返回
		if (strText == _T("Command Error!\r\n"))
		{
			AfxMessageBox(strText);
			return true;
		}
		//若是控制命令，则将返回信息显示在RichEdit中
		else //if (strText.Find(_T("ST_CLK")) != -1)	//发现ST_CLK命令
		{
//			pDlg->m_RichEdit2Info.SetSel(-1, -1);	//光标置于最后
			pDlg->m_RichEdit2Info.ReplaceSel(_T("接收到的数据：\r\n"));
			pDlg->m_RichEdit2Info.ReplaceSel(strText);
//			pDlg->m_RichEdit2Info.SetSel(-1, -1);	//光标置于最后
			pDlg->m_RichEdit2Info.SetFocus();	//焦点设置在RichEdit控件上
			if (strText.Find(_T("QU_ALD")) != -1)
			{
				return false;
			}
			return true;
		}
	}
	//扫描已有台站ID
	if (m_ProcessFlag == 2)
	{
		if (strText == _T("Please check the time!\r\n"))
			m_ProcessEvent.PulseEvent();	// 接收到的数据是“请校验时间”提示，说明命令正确，则释放CModifyIdDlg::OnBnClickedBtnStartScan()中挂起的事件标志
			
		return true;
	}
	//扫描新台站
	if (m_ProcessFlag == 3)
	{
		HWND hWnd = ::FindWindow(NULL, _T("更改台站ID"));
		if (strText == _T("Please check the time!\r\n"))	//扫描到新台站，则在Edit中显示出来
		{
			//跨对话框设置控件，方法一
			HWND hCtrl = ::GetDlgItem(hWnd, IDC_EDIT_NEW_STATION);
			::SetWindowText(hCtrl, _T("MS50U000"));
			//跨对话框设置控件，方法二
			::SetDlgItemText(hWnd, IDC_STATIC_SCAN_NEW, _T("发现新台站！"));
		}
		else	//接收到数据但不能确定是否扫描成功
		{
			::SetDlgItemText(hWnd, IDC_STATIC_SCAN_NEW, _T("扫描错误！"));
		}
		return true;
	}
	//台站时钟初始化
	if (m_ProcessFlag == 4)
	{
		if (strText.Find(_T("ST_CLK")) != -1)
			m_ClockInitEvent.PulseEvent();	// 接收到的数据是台站返回的设定成功的时间，则释放子线程ClockInitThread()中挂起的事件标志

		return true;
	}
	//修改新台站ID
	if (m_ProcessFlag == 5)
	{
		HWND hWnd = ::FindWindow(NULL, _T("更改台站ID"));
		if (strText.Find(_T("OK")) != -1)	//分配新ID成功，则提示
		{
			//清空对话框中的台站和ID信息
			::SetDlgItemText(hWnd, IDC_EDIT_NEW_STATION, _T(""));
			::SetDlgItemText(hWnd, IDC_EDIT_NEW_ID, _T(""));
			//MessageBox提示
			strText = _T("分配新ID成功！\r\n") + strText;
			AfxMessageBox(strText);
		}
		else if(strText.Find(_T("ERROR")) != -1)	//分配新ID失败
		{
			strText = _T("分配新ID失败！请检查命令！\r\n") + strText;
			AfxMessageBox(strText);
		}
		else	//反馈信息未识别，无法确定新ID是否分配成功
		{
			strText = _T("反馈信息校验错误，建议重新分配ID！\r\n") + strText;
			AfxMessageBox(strText);
		}
		return true;
	}
	//修改已有台站ID
	if (m_ProcessFlag == 6)
	{	//先获取窗口句柄，再根据窗口句柄获取窗口指针
		HWND hWnd = ::FindWindow(NULL, _T("更改台站ID"));
		CModifyIdDlg* pWnd = (CModifyIdDlg*)CModifyIdDlg::FromHandle(hWnd);
		//开始处理
		if (strText.Find(_T("OK")) != -1)	//修改ID成功，则提示
		{
			//更新列表中的台站ID信息（2个）
			pWnd->m_ListStnRepo.SetItemText(pWnd->m_SelRow, 1, strText.Mid(21, 3));	//从返回信息中取出新台站ID
			pDlg->m_List.DeleteString(pWnd->m_SelRow);
			pDlg->m_List.InsertString(pWnd->m_SelRow, strText.Mid(16, 8));	//从返回信息中取出新台站类型和ID
			//清除修改ID的Edit框
			pWnd->m_EditChangeId.SetWindowText(_T(""));
			//MessageBox提示
			strText = _T("修改ID成功！\r\n") + strText;
			AfxMessageBox(strText);
		}
		else if (strText.Find(_T("ERROR")) != -1)	//修改ID失败，可能是发送出去的指令受到干扰，一般不会出现此错误
		{
			strText = _T("分配新ID失败！请检查发送出去的命令！\r\n") + strText;
			AfxMessageBox(strText);
		}
		else	//反馈信息未识别，无法确定ID是否修改成功
		{
			strText = _T("反馈信息校验错误，建议重新修改ID！\r\n") + strText;
			AfxMessageBox(strText);
		}
		return true;
	}
	//网络方式下获取数据采集器连接的所有子台站信息
	if (m_ProcessFlag == 7)
	{
		if (strText.Find(_T(".")) != -1)	//如果找到小数点，则说明的确收到了子台站IP地址信息，否则可能收到的是空字串（DTU设备模块即以此方式发送，原因未知）
		{
			//将接收到的子台站IP包（格式：xxxx.xxxx\r\nxxxx.xxxx\r\n ...）拆开，并显示在主窗口的台站列表中
			int iStart = 0, iEnd = 0;
			int iStationCount = 0;	//子台站ID计数
			CString strTemp;
			do
			{
				iStationCount++;	//台站ID计数加1
				iEnd = strText.Find(_T("\r\n"), iStart);	//从前一次找到的空格符的后一位开始查找（避免重复）
				if (iEnd != -1)
				{
					strTemp = strText.Mid(iStart, iEnd - iStart);	//得到两个\r\n符之间的字符串
					if (strTemp != "0.0")	//数据采集器会多发送预置位ip：0.0\r\n，因此为匹配数据采集器的设计，此处将多发送的0.0过滤掉
					{
						CString str;
						str.Format(_T("┖MS50U%03d"), iStationCount);
						pDlg->m_List.AddString(str + _T("(") + strTemp + _T(")"));	//显示在台站列表中
					}
					iStart = iEnd + 2;	//下次搜索的起始位置
				}
			} while (iEnd != -1);	//若前一次找到\r\n，则继续循环查找
			m_ProcessFlag = 0;	//切回普通模式
			m_DevIpSuccessEvent.PulseEvent();	//释放事件标识，向发送获取命令的线程提示操作成功
			pDlg->m_RichEdit2Info.ReplaceSel(_T("成功\r\n"));
			return true;
		}
		else	//若未发现小数点，则说明子台站信息未发来，这时候不切换回普通模式，而是直接返回true，直到等到子台站IP信息为止
		{
			pDlg->m_RichEdit2Info.ReplaceSel(_T("[") + strText + "]");	//调试用，以确认DTU模块建立连接后是否会首先自动发送空字串
			return true;
		}
	/*
		m_iStationCount++;
		CString str;
		str.Format(_T("┖MS50U%03d"), m_iStationCount);
		pDlg->m_List.AddString(str + _T("(") + strText + _T(")"));	//显示在台站列表中
		
		//若台站数扫描完毕，则立刻将flag置零进入普通模式
		//从Config.ini文件中恢复上次保存的【数据采集器连接的子台站数量】
		UINT uChildStationNumber;
		uChildStationNumber = GetPrivateProfileInt(_T("Global"), _T("ChildStationNumber"), 2, _T(".\\Config.ini"));	//注意此函数直接返回UINT类型,默认返回2
		if (uChildStationNumber == m_iStationCount)
		{
			m_ProcessFlag = 0;
		}
		return true;
	*/
		
	}

	return false;	//默认返回false
}


void CAwsdataStorage::StoreString(CString strText)
{
////获取主对话框指针
	CAutoWeatherStationDlg* pDlg = (CAutoWeatherStationDlg*)theApp.GetMainWnd();
////预处理
	if (PreProcess(strText) == true)	//接收的数据已经得到预处理（说明接收到的是命令返回而不是需要存储的数据），则直接返回
	{
		return;
	}

	strText = m_strDataMerge;	//重要！合并后的数据赋回给strText
	m_strDataMerge = _T("");	//清空临时区域
//	pDlg->m_RichEdit2Info.ReplaceSel(_T('{') + strText + _T('}'));	//调试用，数据整合验证

////处理数据
	//原始数据格式：气象站型号 返回 命令 年 月 日 时分秒 当前温度 平均温度 最大温度 最小温度 当前湿度 平均湿度 最大湿度 最小湿度 当前气压 平均气压 最大气压 最小气压 当前风速 平均风速 最大风速 最小风速 矢量风速 当前风向 矢量风向
	//	strText = _T("MS50U000 QU_ALD OK 2014 09 03 22:56:12 7200 7200 7300 7100 60 60 65 55 10500 10500 11000 10000 0 0 0 0 1 0 1\r\n");	//测试用数据
	//	修改：矢量风速暂时删去，数据库中以数字“9999”代替
	//格式化接收到的数据
	CArray<CString, CString> strArray;
	if (AnalyzeString(strText, strArray) == FALSE)	// 将接收到的CString类型的数据分解成可以在数据库中存储的元素，存储在strArray中，如果拆分失败，则直接打印出接收到的数据
	{
//		AfxMessageBox(_T("数据处理出现错误！"));
		//在主窗口中提示
		pDlg->m_RichEdit2Info.ReplaceSel(_T("*\r\n"));	//*号表示前行数据不存入数据库
		return;
	}

	if (strArray.GetCount() != 25)	//检查接收的数据与数据库的项数是否匹配，注意：数据不同则需要依情况修改
	{
		//在主窗口中提示
		pDlg->m_RichEdit2Info.ReplaceSel(_T("#\r\n"));	//#号表示前行数据不存入数据库
///*
		int num = strArray.GetCount();
		CString strNum;
		strNum.Format(_T("解析到的元素数：%d\r\n"), num);
		pDlg->m_RichEdit2Info.ReplaceSel(strNum);
		for (int i = 0; i < num; i++)
		{	
			strNum.Format(_T("（%d）"), i);	//序号
			pDlg->m_RichEdit2Info.ReplaceSel(strNum + strArray.GetAt(i) + _T("\r\n"));
		}
		pDlg->m_RichEdit2Info.ReplaceSel(_T("#\r\n"));	
//*/
		//		pDlg->m_RichEdit2Info.SetSel(-1, -1);	//光标置于最后
		//		pDlg->m_RichEdit2Info.ReplaceSel(strText);
		//		pDlg->m_RichEdit2Info.SetSel(-1, -1);	//光标置于最后
		//		pDlg->m_RichEdit2Info.SetFocus();	//焦点设置在RichEdit控件上
		//		AfxMessageBox(_T("接收到的字符串数据格式有误！"));
		//		pDlg->m_RichEdit2Info.ReplaceSel(_T("接收到的字符串数据格式有误！\r\n"));
		return;
	}

	if (m_ProcessFlag != 0)	//防止非普通模式下代码执行到这里
	{
		pDlg->m_RichEdit2Info.ReplaceSel(_T("！！"));
		pDlg->m_RichEdit2Info.SetFocus();	//焦点设置在RichEdit控件上
		return;
	}

/*
	/////////////////////////
	///////////////////////////////
	strArray.GetAt(15) = L"100100";
	strArray.GetAt(16) = L"100100";
	strArray.GetAt(17) = L"100100";
	strArray.GetAt(18) = L"100100";
	/////////////////////////
	/////////////////////////
*/	
	//向Meter对话框发送自定义消息，将筛选好的数据传送过去以供显示调用
	m_strArrayForMeter.Copy(strArray);
	if (((CAutoWeatherStationDlg*)AfxGetApp()->GetMainWnd())->m_pVMdlg != NULL)
		((CAutoWeatherStationDlg*)AfxGetApp()->GetMainWnd())->m_pVMdlg->PostMessage(WM_REFRESH_VIRTUAL_METER, (WPARAM)&m_strArrayForMeter, 2);

	//将数据存入数据库（数据表）中
	if (!m_pAwsdata->IsOpen())
	{
		m_pAwsdata->Open();
	}
	m_pAwsdata->AddNew();
	//string类型可以直接赋值
	m_pAwsdata->m_AwsType = strArray.GetAt(0);
	m_pAwsdata->m_Command = strArray.GetAt(1);
	m_pAwsdata->m_CmdRet = strArray.GetAt(2);
	//将string类型转换为CTime类后才能赋值
	int
		tmpYear = _ttoi(strArray.GetAt(3)),
		tmpMonth = _ttoi(strArray.GetAt(4)),
		tmpDay = _ttoi(strArray.GetAt(5)),
		tmpHour = _ttoi(strArray.GetAt(6).Left(2)),
		tmpMinute = _ttoi(strArray.GetAt(6).Mid(3, 2)),
		tmpSecond = _ttoi(strArray.GetAt(6).Right(2));
	//CTime类内部会对时间格式进行检查，如果格式不对将会抛出异常，因此为避免程序崩溃，先对时间进行范围限制
	if (tmpYear < 2015 || tmpYear > 2050 || tmpMonth > 12 || tmpDay > 31 || tmpHour > 24 || tmpMinute > 60 || tmpSecond > 60 )
	{
		pDlg->m_RichEdit2Info.ReplaceSel(_T("读取到的日期时间数据错误，请确认台站时钟已被成功初始化！"));
		pDlg->m_RichEdit2Info.SetFocus();
		return;
	}
	CTime tmpDateAndTime(tmpYear, tmpMonth, tmpDay, tmpHour, tmpMinute, tmpSecond);	//CTime类只能通过构造函数手动赋值
	m_pAwsdata->m_DDate = tmpDateAndTime;
	m_pAwsdata->m_TTime = tmpDateAndTime;
	//将string类型转换为long类型后才能赋值
	m_pAwsdata->m_CurTemp = _ttol(strArray.GetAt(7));
	m_pAwsdata->m_AvgTemp = _ttol(strArray.GetAt(8));
	m_pAwsdata->m_MaxTemp = _ttol(strArray.GetAt(9));
	m_pAwsdata->m_MinTemp = _ttol(strArray.GetAt(10));
	m_pAwsdata->m_CurHumi = _ttol(strArray.GetAt(11));
	m_pAwsdata->m_AvgHumi = _ttol(strArray.GetAt(12));
	m_pAwsdata->m_MaxHumi = _ttol(strArray.GetAt(13));
	m_pAwsdata->m_MinHumi = _ttol(strArray.GetAt(14));
	m_pAwsdata->m_CurBaro = _ttol(strArray.GetAt(15));
	m_pAwsdata->m_AvgBaro = _ttol(strArray.GetAt(16));
	m_pAwsdata->m_MaxBaro = _ttol(strArray.GetAt(17));
	m_pAwsdata->m_MinBaro = _ttol(strArray.GetAt(18));
	m_pAwsdata->m_CurWindSpe = _ttol(strArray.GetAt(19));
	m_pAwsdata->m_AvgWindSpe = _ttol(strArray.GetAt(20));
	m_pAwsdata->m_MaxWindSpe = _ttol(strArray.GetAt(21));
	m_pAwsdata->m_MinWindSpe = _ttol(strArray.GetAt(22));
	m_pAwsdata->m_VectWindSpe = 9999;						//矢量风速暂时不用，以“9999”代替
	m_pAwsdata->m_CurWindDir = _ttol(strArray.GetAt(23));
	m_pAwsdata->m_VectWindDir = _ttol(strArray.GetAt(24));
	//更新数据库
	m_pAwsdata->Update();

////前台显示
/*	测试用
	pDlg->m_RichEdit2Info.ReplaceSel(strText);
	for (int i = 0; i < strArray.GetSize(); i++)
	{
		pDlg->m_RichEdit2Info.ReplaceSel(_T("\r\n"));
		pDlg->m_RichEdit2Info.ReplaceSel(strArray.GetAt(i));
	}
*/
	m_StoreSuccessEvent.PulseEvent();	//普通模式下，数据存储成功，则释放自动检测线程中挂起的事件标识

	//原始数据转换
	//公式：	温度，(x-5000)/100 ℃
	//			湿度，x %
	//			压力，x Pa
	//			风速，x/100 m/s
	//			风向，x/10 °
	CString strTemp, strBaro, strWindSpe, strWindDir;
	float fTemp = (_ttol(strArray.GetAt(7)) - 5000) / 100.0;
	strTemp.Format(_T("%.1f"), fTemp);
	float fBaro = _ttol(strArray.GetAt(15)) / 1000.0;
	strBaro.Format(_T("%.1f"), fBaro);
	float fWindSpe = _ttol(strArray.GetAt(19)) / 100.0;
	strWindSpe.Format(_T("%.1f"), fWindSpe);
	float fWindDir = _ttol(strArray.GetAt(23)) / 10.0;
	strWindDir.Format(_T("%.1f"), fWindDir);
	//写入成功提示
	pDlg->m_RichEdit2Info.ReplaceSel(strArray.GetAt(0));
	pDlg->m_RichEdit2Info.ReplaceSel(_T("号台站数据写入数据库...\t  成功"));
	pDlg->m_RichEdit2Info.ReplaceSel(_T("\r\n"));
	pDlg->m_RichEdit2Info.ReplaceSel(_T("时间：") + tmpDateAndTime.Format("%Y-%m-%d %H:%M:%S") + _T("\r\n"));
	pDlg->m_RichEdit2Info.ReplaceSel(_T("当前温度：") + strTemp + _T(" ℃") + _T("\r\n"));
	pDlg->m_RichEdit2Info.ReplaceSel(_T("当前湿度：") + strArray.GetAt(11) + _T(" %") + _T("\r\n"));
	pDlg->m_RichEdit2Info.ReplaceSel(_T("当前气压：") + strBaro + _T(" kPa") + _T("\r\n"));
	pDlg->m_RichEdit2Info.ReplaceSel(_T("当前风速：") + strWindSpe + _T(" m/s") + _T("\r\n"));
	pDlg->m_RichEdit2Info.ReplaceSel(_T("当前风向：") + strWindDir + _T(" °") + _T("\r\n"));
	pDlg->m_RichEdit2Info.SetFocus();	//焦点设置在RichEdit控件上
}



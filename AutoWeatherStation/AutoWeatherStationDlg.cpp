
// AutoWeatherStationDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "AutoWeatherStation.h"
#include "AutoWeatherStationDlg.h"
#include "afxdialogex.h"
#include "ServerSocket.h"
#include "ClientSocket.h"
#include "AwsdataStorage.h"
#include "ModifyIdDlg.h"
#include "VirtualMeterDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//Timer定时器响应宏定义
#define TIMERID_AUTOSEND 1
#define TIMERID_AUTOSERV 2
#define TIMERID_REFRESHTIME 3

//自定义消息
#define WM_LISTBOX_INFO				WM_USER + 2		//自动监测线程用于获取主对话框Listbox控件信息的自定义消息
#define WM_REFRESH_VIRTUAL_METER	WM_USER + 3		//主对话框向Meter对话框发送新数据的自定义消息

/*************************************************全局变量定义区*******************************************************/

volatile int g_Count = 0;	//自动监测线程和向采集器发送获取子台站信息的命令的线程从此处读取listbox中的元素个数
CString g_String = _T("");	//自动监测线程从此处读取listbox中对应列的字符串值

/**********************************************************************************************************************/

// 用于应用程序“关于”菜单项的 CAboutDlg 对话框
class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CAutoWeatherStationDlg 对话框



CAutoWeatherStationDlg::CAutoWeatherStationDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CAutoWeatherStationDlg::IDD, pParent)
	, m_uEditPort(6000)
	, m_lLastSel(0)
	, m_nBaudrate(4)
	, m_nCheckbit(0)
	, m_nCom(0)
	, m_nDatabit(0)
	, m_nStopbit(0)
	, m_uEditRpt(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
//	m_hIcon = AfxGetApp()->LoadIcon(IDI_MAINICON);
	ConnectionModeFlag = 0;	//初始化连接方式标识
}

void CAutoWeatherStationDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BTN_LISTEN, m_BtnListen);
	DDX_Control(pDX, IDC_BTN_STOP, m_BtnStop);
	DDX_Text(pDX, IDC_EDIT_PORT, m_uEditPort);
	DDV_MinMaxUInt(pDX, m_uEditPort, 1000, 9999);
	DDX_Control(pDX, IDC_IPADDRESS_TCP, m_IpAddressTcp);
	DDX_Control(pDX, IDC_RICHEDIT2_INFO, m_RichEdit2Info);
	DDX_Control(pDX, IDC_LIST, m_List);
	DDX_CBIndex(pDX, IDC_COMBO_BAUDRATE, m_nBaudrate);
	DDX_CBIndex(pDX, IDC_COMBO_CHECKBIT, m_nCheckbit);
	DDX_CBIndex(pDX, IDC_COMBO_COM, m_nCom);
	DDX_CBIndex(pDX, IDC_COMBO_DATABIT, m_nDatabit);
	DDX_CBIndex(pDX, IDC_COMBO_STOPBIT, m_nStopbit);
	DDX_Control(pDX, IDC_BTN_COMOPEN, m_BtnComopen);
	DDX_Control(pDX, IDC_BTN_COMCLOSE, m_BtnComclose);
	DDX_Text(pDX, IDC_EDIT_RPT, m_uEditRpt);
	DDX_Control(pDX, IDC_CHECK_RPT, m_CheckRpt);
	DDX_Control(pDX, IDC_COMBO_CMD, m_ComboCmd);
}

BEGIN_MESSAGE_MAP(CAutoWeatherStationDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BTN_LISTEN, &CAutoWeatherStationDlg::OnBnClickedBtnListen)
	ON_BN_CLICKED(IDC_BTN_STOP, &CAutoWeatherStationDlg::OnBnClickedBtnStop)
	ON_EN_CHANGE(IDC_RICHEDIT2_INFO, &CAutoWeatherStationDlg::OnEnChangeRichedit2Info)
	ON_BN_CLICKED(IDC_BTN_OPENHISTORY, &CAutoWeatherStationDlg::OnBnClickedBtnOpenhistory)
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_BTN_COMOPEN, &CAutoWeatherStationDlg::OnBnClickedBtnComopen)
	ON_BN_CLICKED(IDC_BTN_COMCLOSE, &CAutoWeatherStationDlg::OnBnClickedBtnComclose)
	ON_MESSAGE(ON_COM_RECEIVE, &CAutoWeatherStationDlg::OnComReceive)	//自定义消息，用于接收CnComm串口数据
	ON_BN_CLICKED(IDC_BUTTON_SEND, &CAutoWeatherStationDlg::OnBnClickedButtonSend)
	ON_BN_CLICKED(IDC_CHECK_RPT, &CAutoWeatherStationDlg::OnBnClickedCheckRpt)
	ON_WM_TIMER()
	ON_CBN_SELCHANGE(IDC_COMBO_CMD, &CAutoWeatherStationDlg::OnSelchangeComboCmd)
	ON_COMMAND(ID_MENU_MODIFY_ID, &CAutoWeatherStationDlg::OnMenuModifyId)
	ON_COMMAND(ID_MENU_INITIALIZE_CLOCK, &CAutoWeatherStationDlg::OnMenuInitializeClock)
	ON_COMMAND(ID_MENU_NORMAL_MODE, &CAutoWeatherStationDlg::OnMenuNormalMode)
	ON_COMMAND(ID_MENU_TEST_MODE, &CAutoWeatherStationDlg::OnMenuTestMode)
	ON_COMMAND(ID_MENU_AUTO_SERV_OPEN, &CAutoWeatherStationDlg::OnMenuAutoServOpen)
	ON_COMMAND(ID_MENU_AUTO_SERV_CLOSE, &CAutoWeatherStationDlg::OnMenuAutoServClose)
	ON_BN_CLICKED(IDC_BTN_CE, &CAutoWeatherStationDlg::OnBnClickedBtnCe)
	ON_COMMAND(ID_ABOUT, &CAutoWeatherStationDlg::OnAbout)
	ON_MESSAGE(WM_LISTBOX_INFO, &CAutoWeatherStationDlg::OnListboxInfo)
	ON_BN_CLICKED(IDC_BTN_OPEN_METER, &CAutoWeatherStationDlg::OnBnClickedBtnOpenMeter)
	ON_LBN_DBLCLK(IDC_LIST, &CAutoWeatherStationDlg::OnDblclkList)
	ON_WM_MOVE()
	ON_NOTIFY_EX(TTN_NEEDTEXT, 0, OnToolTipNotify)
//	ON_WM_CREATE()
ON_COMMAND(ID_MENU_CE, &CAutoWeatherStationDlg::OnMenuCe)
ON_COMMAND(ID_MENU_HISTORY, &CAutoWeatherStationDlg::OnMenuHistory)
ON_COMMAND(ID_MENU_METER, &CAutoWeatherStationDlg::OnMenuMeter)
END_MESSAGE_MAP()


// CAutoWeatherStationDlg 消息处理程序

////弃用，创建工具栏可在OnInitDialog中直接创建，而无需在OnCreate中创建，两者的区别是此处创建会使工具栏处于整个对话框的顶层，而在OnInitDialog中创建的工具栏处于底层
//int CAutoWeatherStationDlg::OnCreate(LPCREATESTRUCT lpCreateStruct)
//{
//	if (CDialogEx::OnCreate(lpCreateStruct) == -1)
//		return -1;
//
//	// TODO:  在此添加您专用的创建代码
//	m_toolbar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE
//		| CBRS_TOP	//初始停靠在客户区顶部
//		| CBRS_GRIPPER	//左边有一条竖线
//		| CBRS_TOOLTIPS //产生工具提示
//		| CBRS_FLYBY	//产生消息文本
//		| CBRS_SIZE_DYNAMIC	//动态改变大小
//		| CBRS_BORDER_TOP	//在工具栏上面显示边框
//		);//创建
//
//	m_toolbar.LoadToolBar(IDR_TOOLBAR);//加载资源
//	RepositionBars(AFX_IDW_CONTROLBAR_FIRST, AFX_IDW_CONTROLBAR_LAST, 0);	//不要忘了调用此函数，否则工具栏不显示。
//	//创建工具栏
///*	if (!m_MainToolbar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP | CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC))
//	{
//		TRACE0("未能创建工具栏\n");
//	}
//	if (!m_MainToolbar.LoadToolBar(IDR_TOOLBAR))
//	{
//		TRACE0("未能载入工具栏\n");
//	}
//	m_MainToolbar.ShowWindow(SW_SHOW);
//	RepositionBars(AFX_IDW_CONTROLBAR_FIRST, AFX_IDW_CONTROLBAR_LAST, 0);
//*/
///*
//	if (!m_toolbar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_ALIGN_TOP | CBRS_GRIPPER | CBRS_TOOLTIPS, CRect(2, 2, 0, 0)) || !m_toolbar.LoadToolBar(IDR_TOOLBAR))
//	{
//	TRACE0("failed to create toolbar\n");
//	return FALSE;
//	}
//	m_toolbar.ShowWindow(SW_SHOW);
//	RepositionBars(AFX_IDW_CONTROLBAR_FIRST, AFX_IDW_CONTROLBAR_LAST, 0);
//*/
///*	if (!m_MainToolbar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
//		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
//		!m_MainToolbar.LoadToolBar(IDR_TOOLBAR))
//	{
//		TRACE0("Failed to create toolbar\n");
//		return -1;      // fail to create
//	}
//
//	m_MainToolbar.EnableDocking(CBRS_ALIGN_ANY);
//	EnableDocking(CBRS_ALIGN_ANY);
//	DockControlBar(&m_wndMyToolBar);
//*/
//
//	return 0;
//}


BOOL CAutoWeatherStationDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。	////【注】系统菜单，是指点击软件左上角小图标时弹出的菜单！ -- Lee

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO:  在此添加额外的初始化代码
////变量初始化
	m_pstrStation = new CString;
////设置各个控件的初始化状态
	//按钮
	m_BtnStop.EnableWindow(FALSE);
	m_BtnComclose.EnableWindow(FALSE);
	CButton* pBtn = (CButton*)GetDlgItem(IDC_BUTTON_SEND);
	pBtn->EnableWindow(FALSE);
	m_CheckRpt.EnableWindow(FALSE);
	m_ComboCmd.EnableWindow(FALSE);
	CEdit* pEdit = (CEdit*)GetDlgItem(IDC_EDIT_CMD);
	pEdit->EnableWindow(FALSE);
	pEdit = (CEdit*)GetDlgItem(IDC_EDIT_RPT);
	pEdit->EnableWindow(FALSE);
	//菜单
	CMenu* pMenu = GetMenu();
	if (pMenu)
	{
		pMenu->EnableMenuItem(ID_MENU_NORMAL_MODE, //ID号，或者位置号
			MF_BYCOMMAND |		//以ID号识别（默认，可不写）
		//	MF_BYPOSITION |		//以位置识别
		//	MF_ENABLED |		//相应菜单项变亮，允许选择
			MF_DISABLED |		//不允许选择
		//	MF_GRAYED|			//相应菜单项变暗（在win7中实际效果似乎与MF_DISABLED相同）
			NULL);
		pMenu->EnableMenuItem(ID_MENU_MODIFY_ID, MF_DISABLED);
		pMenu->EnableMenuItem(ID_MENU_INITIALIZE_CLOCK, MF_DISABLED);
		pMenu->EnableMenuItem(ID_MENU_AUTO_SERV_OPEN, MF_DISABLED);
		pMenu->EnableMenuItem(ID_MENU_AUTO_SERV_CLOSE, MF_DISABLED);
	}
////创建工具栏，两种方法
	#define METHOD 2
#if METHOD == 1
	m_toolbar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE
		| CBRS_TOP	//初始停靠在客户区顶部
		| CBRS_GRIPPER	//左边有一条竖线
		| CBRS_TOOLTIPS //产生工具提示
		| CBRS_FLYBY	//产生消息文本
		| CBRS_SIZE_DYNAMIC	//动态改变大小
		| CBRS_BORDER_TOP	//在工具栏上面显示边框
		);//创建
	m_toolbar.LoadToolBar(IDR_TOOLBAR);	//加载资源，【注意】IDR_TOOLBAR一定要在VC中创建，并且图标使用MFC默认大小，即16x15、4位颜色深度，否则会抛异常
	RepositionBars(AFX_IDW_CONTROLBAR_FIRST, AFX_IDW_CONTROLBAR_LAST, 0);	//不要忘了调用此函数，否则工具栏不显示。
#endif
#if METHOD == 2
	m_toolbar.CreateEx(this, TBSTYLE_FLAT  , WS_CHILD | WS_VISIBLE
		| CBRS_TOP	//初始停靠在客户区顶部
		| CBRS_GRIPPER	//左边有一条竖线
		| CBRS_TOOLTIPS //产生工具提示
		| CBRS_FLYBY	//产生消息文本
		| CBRS_SIZE_DYNAMIC	//动态改变大小
		| CBRS_BORDER_TOP	//在工具栏上面显示边框
		);
	//使用CImageList作为图标容器
	CImageList m_ImageList;
	m_ImageList.Create(32, 32, ILC_COLOR24 | ILC_MASK, 10, 1);	//最后面两个参数4,1，不重要，可任意指定
	//载入ico图标的方法
//	m_ImageList.Add(AfxGetApp()->LoadIcon(IDI_ICONLOOK));
	//载入bmp图片，并将图片中的所有白色像素转为透明，然后存入ImageList中
	CBitmap bm0, bm1, bm2, bm3, bm4, bm5, bm6, bm7;	//注意要为每个图片创建一个对象
	int ret;
	ret = bm0.LoadBitmap(IDB_CONFMODE);
	m_ImageList.Add(&bm0, RGB(255, 255, 255));
	ret = bm1.LoadBitmap(IDB_INITCLOCK);
	m_ImageList.Add(&bm1, RGB(255, 255, 255));
	ret = bm2.LoadBitmap(IDB_START);
	m_ImageList.Add(&bm2, RGB(255, 255, 255));
	ret = bm3.LoadBitmap(IDB_STOP);
	m_ImageList.Add(&bm3, RGB(255, 255, 255));
	ret = bm4.LoadBitmap(IDB_CE);
	m_ImageList.Add(&bm4, RGB(255, 255, 255));
	ret = bm5.LoadBitmap(IDB_METER);
	m_ImageList.Add(&bm5, RGB(255, 255, 255));
	ret = bm6.LoadBitmap(IDB_REPORT);
	m_ImageList.Add(&bm6, RGB(255, 255, 255));
	ret = bm7.LoadBitmap(IDB_ID);
	m_ImageList.Add(&bm7, RGB(255, 255, 255));
	m_toolbar.GetToolBarCtrl().SetImageList(&m_ImageList);
	m_ImageList.Detach();
	//设置按钮数量
	m_toolbar.SetButtons(NULL, 10); //共10个按钮（包括SEPARATOR）
	//设置每个工具按钮文字，设置后，每个图标下面就显示相应的文字，也可以不设置。
	//【注意】如果设置了ButtonText，那么SetSizes()的时候应当为下方的文字留下足够的空位，否则后面EnableButton(FALSE)的时候会出现按钮图片向上错位的bug！而如果不设置ButtonText则不会出现此问题
	m_toolbar.SetButtonInfo(0, ID_MENU_TEST_MODE, TBSTYLE_CHECK, 0);	//样式为Check Button
//	m_toolbar.SetButtonText(0, _T("进入配置模式"));
	m_toolbar.SetButtonInfo(1, ID_SEPARATOR, TBBS_SEPARATOR, 0);
	m_toolbar.SetButtonInfo(2, ID_MENU_INITIALIZE_CLOCK, TBSTYLE_BUTTON, 1);
//	m_toolbar.SetButtonText(2, _T("初始化台站时钟"));
	m_toolbar.SetButtonInfo(3, ID_MENU_AUTO_SERV_OPEN, TBSTYLE_BUTTON, 2);
//	m_toolbar.SetButtonText(3, _T("开启自动检测服务"));
	m_toolbar.SetButtonInfo(4, ID_MENU_AUTO_SERV_CLOSE, TBSTYLE_BUTTON, 3);
//	m_toolbar.SetButtonText(4, _T("关闭自动检测服务"));
	m_toolbar.SetButtonInfo(5, ID_SEPARATOR, TBBS_SEPARATOR, 0);
	m_toolbar.SetButtonInfo(6, ID_MENU_CE, TBSTYLE_BUTTON, 4);
	m_toolbar.SetButtonInfo(7, ID_MENU_METER, TBSTYLE_CHECK, 5);
	m_toolbar.SetButtonInfo(8, ID_MENU_HISTORY, TBSTYLE_CHECK, 6);
	m_toolbar.SetButtonInfo(9, ID_MENU_MODIFY_ID, TBSTYLE_BUTTON, 7);
	//测试用
//	m_toolbar.GetToolBarCtrl().Indeterminate(1, FALSE);
//	m_toolbar.GetToolBarCtrl().SetState(1, TBSTATE_ENABLED);
	//设置按钮与图标大小，CSize(40,40)设置按钮大小， CSize(32,32)设置按钮上图标大小
	//注意，第一个CSize中的参数必须比第二个CSize中的参数大，经验证，应分别大至少7和6，否则会抛异常，即(CSize(cx1,cy1), CSize(cx2,cy2))，应满足cx1-cx2>=7，cy1-cy2>=6
	//另外，按钮大小要足够大才可以将设置的ButtonText显示出来，此处按钮大小设置为40x40是为了将ButtonText隐藏，如果设置成51x51则可以显示
	m_toolbar.SetSizes(CSize(40, 40), CSize(32, 32)); //此处要和图像列表中图像的实际大小一致(32,32)
/*	//根据buttontext来设置按钮大小
	CRect temp;
	m_toolbar.GetItemRect(0, &temp);
	m_toolbar.GetToolBarCtrl().SetButtonSize(CSize(temp.Width(), temp.Height()));
*/	//设置按钮初始化时的可用性
	m_toolbar.GetToolBarCtrl().EnableButton(ID_MENU_INITIALIZE_CLOCK, FALSE);	//初始化时禁用某些按钮
	m_toolbar.GetToolBarCtrl().EnableButton(ID_MENU_AUTO_SERV_OPEN, FALSE);
	m_toolbar.GetToolBarCtrl().EnableButton(ID_MENU_AUTO_SERV_CLOSE, FALSE);
	m_toolbar.GetToolBarCtrl().EnableButton(ID_MENU_MODIFY_ID, FALSE);
/*	//设置按钮ID_ORDER和ID_SORT具有下拉箭头
	m_toolbar.GetToolBarCtrl().SetExtendedStyle(TBSTYLE_EX_DRAWDDARROWS);
	DWORD dwStyle = m_toolbar.GetButtonStyle(m_toolbar.CommandToIndex(2));
	dwStyle |= TBSTYLE_DROPDOWN;
	m_toolbar.SetButtonStyle(m_toolbar.CommandToIndex(2), dwStyle);

	dwStyle = m_toolbar.GetButtonStyle(m_toolbar.CommandToIndex(1));
	dwStyle |= TBSTYLE_DROPDOWN;
	m_toolbar.SetButtonStyle(m_toolbar.CommandToIndex(1), dwStyle);
*/	//在对话框中显示工具栏
	EnableToolTips(TRUE);	//允许显示工具栏提示信息，似乎此句加不加都行，而且即使设置为FALSE，也会显示工具栏提示（因为Create的时候已经设定了CBRS_TOOLTIPS）
#endif
////创建状态栏
	//获得系统当前时间
	CTime time;
	time = time.GetCurrentTime();
	CString strtime;
	strtime.Format(_T("%s"), time.Format("当前时间：%Y-%m-%d %H:%M:%S"));
	//创建
	static UINT BASED_CODE array[] =
	{
//		ID_SEPARATOR,           // status line indicator   
/*		ID_INDICATOR_CAPS,
		ID_INDICATOR_NUM,
		ID_INDICATOR_SCRL,
*/		IDS_INDICATOR_COM_STATUS,
		IDS_INDICATOR_NET_STATUS,
		IDS_INDICATOR_TIME,
	};
	m_statusbar.Create(this);
	m_statusbar.SetIndicators(array, sizeof(array) / sizeof(UINT));
	CRect rect;
	GetWindowRect(rect);
	m_statusbar.SetPaneInfo(0, array[0], NULL, rect.Width() / 6);
	m_statusbar.SetPaneInfo(1, array[1], NULL, rect.Width() / 2);
	m_statusbar.SetPaneInfo(2, array[1], NULL, rect.Width() / 3);
	m_statusbar.SetPaneText(2, strtime);
	//开启1s定时器用于实时更新状态栏时钟
	SetTimer(TIMERID_REFRESHTIME, 1000, NULL);
	//将工具栏和状态栏显示出来
	RepositionBars(AFX_IDW_CONTROLBAR_FIRST, AFX_IDW_CONTROLBAR_LAST, 0);	//不要忘了调用此函数，否则工具栏和状态栏不显示
////从Config.ini文件中恢复上次保存的【重复时间Editbox】
	UINT uRepeatTime;
	uRepeatTime = GetPrivateProfileInt(_T("Global"), _T("RepeatTime"), 2000, _T(".\\Config.ini"));	//注意此函数直接返回UINT类型,默认返回2000
	//在对话框中显示出来
	m_uEditRpt = uRepeatTime;
	UpdateData(FALSE);
////设置RichEdit事件掩码
	m_RichEdit2Info.SetEventMask(m_RichEdit2Info.GetEventMask() | ENM_CHANGE);
	m_RichEdit2Info.SetReadOnly();
////初始化数据（库）存储与操作类
	m_pAwsdataStorage = new CAwsdataStorage;	//【注意】：此对象在整个程序周期中一直唯一存在！
	m_pAwsdataStorage->m_ProcessFlag = 0;	//初始状态为普通模式而不是调试模式
////创建历史数据对话框，并默认隐藏
	if (m_HistoryDlg.Create(IDD_DIALOG_HISTORY, GetDesktopWindow()) == NULL)
	{
		AfxMessageBox(_T("历史数据对话框创建失败！"));
		return TRUE;
	}
	m_HistoryDlg.ShowWindow(FALSE);
////创建Meter对话框，默认隐藏（注意在对话框编辑器中将Visible属性设置为FALSE，这样程序启动后才不会有瞬间闪烁的情况发生）
	m_pVMdlg = new CVirtualMeterDlg;
	if (m_pVMdlg->Create(IDD_VIRTUALMETER_DIALOG, GetDesktopWindow()) == NULL)	//使用GetDesktopWindow()将父对话框直接设置为桌面，使得Alt+Tab键可以看到
	{
		AfxMessageBox(_T("Meter对话框创建失败！"));
		return TRUE;
	}
	//相当于在对话框资源编辑器中同时修改了Tool Window属性为true、Application Window属性为false
	//【总结】资源编辑器中的属性设置仅仅改变资源的外观，而通过代码修改则既可以改变资源外观，同时又可以修改一些行为，更加灵活方便
//	m_pVMdlg->ModifyStyleEx(WS_EX_APPWINDOW, WS_EX_TOOLWINDOW, 0);	//修改对话框样式为ToolWindow，使得此对话框任务栏图标隐藏，flag参数默认0即可
//	m_pVMdlg->ModifyStyleEx(NULL, WS_EX_APPWINDOW, 1);
//	m_pVMdlg->ShowWindow(SW_HIDE);	//对话框属性已设置为默认隐藏，不用再重复设置
////创建自动检测线程，此线程在整个程序周期中一直运行
	CWinThread* pThread = AfxBeginThread(AutoServThread, NULL);	//不向新线程传递任何参数
////创建网络模式下Socket接收数据log文件
	//创建txt文本
	try
	{
		m_LogFile.Open(_T("OnRecvData.log"),
			CFile::modeCreate |				//创建新文件，但如果文件已存在则将其打开
			CFile::modeWrite |					//只写模式
			CFile::typeText |					//文本模式（检测换行回车，即\r\n）
			NULL);
	}
	catch (CFileException* pe)
	{
		TRACE(_T("File could not be opened, cause = %d\n"),
			pe->m_cause);
		pe->Delete();
	}
//// 设置对话框的寻找标记，用于OnInitInstance()中的实例寻找
	::SetProp(m_hWnd, AfxGetApp()->m_pszExeName, (HANDLE)1);

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}



BOOL CAutoWeatherStationDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO:  在此添加专用代码和/或调用基类
	if ((GetFocus() == GetDlgItem(IDC_EDIT_CMD)) && (pMsg->message == WM_CHAR))	//若焦点在发送Edit控件上
	{
		if (pMsg->wParam == VK_RETURN)	//响应回车键
		{
			CButton* pBtn = (CButton*)GetDlgItem(IDC_BUTTON_SEND);
			if (pBtn->IsWindowEnabled())	//如果发送按钮可点击
			{
				//	OnBnClickedButtonSend();	//实现与发送函数相同的功能
				PostMessage(WM_COMMAND, MAKEWPARAM(IDC_BUTTON_SEND, BN_CLICKED), NULL);	//模拟点击“发送”按钮
			}
			else	//发送按钮不可点击
			{
				AfxMessageBox(_T("发送控制命令前，请将软件切换至测试模式！"));
			}
		}
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}


BOOL CAutoWeatherStationDlg::OnToolTipNotify(UINT id, NMHDR *pNMHDR, LRESULT *pResult)
{
	//	static CString str;	//用于存储字串，注意设置成静态变量、全局变量或类成员变量，总之不可以是在栈上创建的临时变量

	TOOLTIPTEXT *pTTT = (TOOLTIPTEXT *)pNMHDR;
	UINT nID = pNMHDR->idFrom; //获取工具栏按钮ID
	if (nID)
	{
		int nIndex = m_toolbar.CommandToIndex(nID); //根据ID获取按钮索引
		if (nIndex != -1)
		{
//			m_toolbar.GetButtonText(nIndex, str); //获取工具栏文本
//			pTTT->lpszText = str.GetBuffer(str.GetLength()); //设置提示信息文本

			switch (nIndex)
			{
			case 0:
				pTTT->lpszText = _T("测试模式/普通模式");
				break;
			case 2:
				pTTT->lpszText = _T("初始化台站时钟");
				break;
			case 3:
				pTTT->lpszText = _T("启动自动检测服务");
				break;
			case 4:
				pTTT->lpszText = _T("停止自动检测服务");
				break;
			case 6:
				pTTT->lpszText = _T("清空信息显示区");
				break;
			case 7:
				pTTT->lpszText = _T("查看实时数据");
				break;
			case 8:
				pTTT->lpszText = _T("查询历史数据");
				break;
			case 9:
				pTTT->lpszText = _T("更改台站ID（仅串口方式下有效）");
				break;
			default:
				break;
			}

			pTTT->hinst = AfxGetResourceHandle();

			return(TRUE);
		}
	}
	return(FALSE);
}


void CAutoWeatherStationDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CAutoWeatherStationDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CAutoWeatherStationDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


CServerSocket* CAutoWeatherStationDlg::GetServerSocket() const
{
	ASSERT(m_iSocket);
	return m_iSocket;
}


void CAutoWeatherStationDlg::OnBnClickedBtnListen()
{
	// TODO:  在此添加控件通知处理程序代码
	m_RichEdit2Info.SetFocus();	//获取焦点，否则RichEdit控件无法自动滚动
	UpdateData(TRUE);
//	CString TextData();

	//初始化winSock库
	m_RichEdit2Info.ReplaceSel(_T("初始化Socket库..."));
	WSAData wsData;
	if (!AfxSocketInit(&wsData))
	{
		m_RichEdit2Info.ReplaceSel(_T("失败\r\n"));
		AfxMessageBox(_T("初始化Socket库失败！"));
		return;
	}
	m_RichEdit2Info.ReplaceSel(_T("成功\r\n"));

	//创建服务器端Socket、采用TCP
	m_iSocket = new CServerSocket();

	m_RichEdit2Info.ReplaceSel(_T("创建服务器接口..."));
	if (!m_iSocket)
	{
		m_RichEdit2Info.ReplaceSel(_T("失败\r\n"));
		AfxMessageBox(_T("动态创建服务器套接字出错！"));
		return;
	}
	m_RichEdit2Info.ReplaceSel(_T("成功\r\n"));

	m_RichEdit2Info.ReplaceSel(_T("绑定端口..."));
	if (!m_iSocket->Create(m_uEditPort))
	{
		m_RichEdit2Info.ReplaceSel(_T("失败\r\n"));
		AfxMessageBox(_T("端口绑定错误！"));
		m_iSocket->Close();
		return;
	}
	m_RichEdit2Info.ReplaceSel(_T("成功\r\n"));

	m_RichEdit2Info.ReplaceSel(_T("开始监听..."));
	if (!m_iSocket->Listen())
	{
		m_RichEdit2Info.ReplaceSel(_T("失败\r\n"));
		AfxMessageBox(_T("监听失败！"));
		m_iSocket->Close();
		return;
	}
	m_RichEdit2Info.ReplaceSel(_T("成功\r\n"));

	//在IP控件中显示本地IP地址
	CStringA ipAddr;
	char HostName[100];
	::gethostname(HostName, sizeof(HostName));// 获得本机主机名.
	hostent* hn;
	hn = ::gethostbyname(HostName);//根据本机主机名得到本机ip
	ipAddr = inet_ntoa(*(struct in_addr *)hn->h_addr_list[0]);//把ip换成字符串形式
	//将CStringA型IP地址在IPAddressCtrl中显示 
	DWORD dwIP;
	dwIP = inet_addr(ipAddr);
	unsigned char* pIP = (unsigned char*)&dwIP;
	m_IpAddressTcp.SetAddress(*pIP, *(pIP + 1), *(pIP + 2), *(pIP + 3));

////初始化子台站自增标识（m_ProcessFlag == 7 中的情况）
//	m_pAwsdataStorage->m_iStationCount = 0;
////更改界面交互状态
	m_BtnComopen.EnableWindow(FALSE);
	m_BtnComclose.EnableWindow(FALSE);
	m_BtnListen.EnableWindow(FALSE);
	m_BtnStop.EnableWindow(TRUE);
	CMenu* pMenu = GetMenu();
	if (pMenu)
	{
		pMenu->EnableMenuItem(ID_MENU_NORMAL_MODE, MF_DISABLED);
		pMenu->EnableMenuItem(ID_MENU_TEST_MODE, MF_DISABLED);
		m_toolbar.GetToolBarCtrl().EnableButton(ID_MENU_TEST_MODE, FALSE);
		//仅允许从普通模式中进行以下操作
		if (m_pAwsdataStorage->m_ProcessFlag == 0)
		{
		//	pMenu->EnableMenuItem(ID_MENU_MODIFY_ID, MF_ENABLED);	//此处暂不需要，因为台站更改模块暂未支持网络模式下更改
			pMenu->EnableMenuItem(ID_MENU_INITIALIZE_CLOCK, MF_ENABLED);
			pMenu->EnableMenuItem(ID_MENU_AUTO_SERV_OPEN, MF_ENABLED);
		//	pMenu->EnableMenuItem(ID_MENU_AUTO_SERV_CLOSE, MF_ENABLED);
		//	m_toolbar.GetToolBarCtrl().EnableButton(ID_MENU_MODIFY_ID, TRUE);	//此处暂不需要，因为台站更改模块暂未支持网络模式下更改
			m_toolbar.GetToolBarCtrl().EnableButton(ID_MENU_INITIALIZE_CLOCK, TRUE);
			m_toolbar.GetToolBarCtrl().EnableButton(ID_MENU_AUTO_SERV_OPEN, TRUE);
		//	m_toolbar.GetToolBarCtrl().EnableButton(ID_MENU_AUTO_SERV_CLOSE, TRUE);
		}
	}
	//更新状态栏
	m_statusbar.SetPaneText(1, _T("网络通信：已开启"));
////更改连接方式标识
	ConnectionModeFlag = 2;
}


void CAutoWeatherStationDlg::OnBnClickedBtnStop()
{
	// TODO:  在此添加控件通知处理程序代码
	m_RichEdit2Info.SetFocus();	//获取焦点，否则RichEdit控件无法自动滚动
////通知数据采集器，TCP监听将要停止
//	CStringA stra = "GPRS Closed";	//向数据采集器获取子台站IP信息的命令，多个台站IP将分多次返回，每次返回的数据格式为xxxx.xxx
//	CClientSocket* cc = (CClientSocket*)m_iSocket->m_listSockets.GetTail();	//暂时只取最后一个客户端进行操作（位于List的尾部）
//	cc->Send(stra, stra.GetLength());
//	::Sleep(3000);	//延时3s
////关闭所有的ClientSockets
	m_RichEdit2Info.ReplaceSel(_T("关闭Client Socket..."));
	POSITION ps = m_iSocket->m_listSockets.GetHeadPosition();
	while (ps != NULL)
	{
		CClientSocket* pTemp = (CClientSocket*)m_iSocket->m_listSockets.GetNext(ps);
		pTemp->Close();
	}
	m_RichEdit2Info.ReplaceSel(_T("成功\r\n"));
////关闭ServerSockets并清除IP控件
	m_RichEdit2Info.ReplaceSel(_T("关闭Server Socket..."));
	m_iSocket->Close();
	m_RichEdit2Info.ReplaceSel(_T("成功\r\n"));
	m_IpAddressTcp.ClearAddress();
////清空在线列表
/*	
	//【注意】【BUG修复】若自动扫描线程运行的过程中，点击了停止监听按钮，则有可能发生两个线程同时操作m_List控件的情况，
	//由于MFC的控件操作通常不是线程安全的，因此应当避免这种情况。其他线程操作控件推荐向主线程发送自定义消息，然后主线程在自定义消息中操作控件
	int Count = m_List.GetCount();
	for (int i = 0; i < Count; i++)
	{
		m_List.DeleteString(0);	//因为每删除一行，后一行会自动前移，因此每次删除第一行即可，即令nIndex == 0
	}
*/
	m_List.ResetContent();	//清空ListBox
////清除子台站自增标识（m_ProcessFlag == 7 中的情况）
//	m_pAwsdataStorage->m_iStationCount = 0;
////更改界面交互状态
	m_BtnComopen.EnableWindow(TRUE);
	m_BtnComclose.EnableWindow(FALSE);
	m_BtnListen.EnableWindow(TRUE);
	m_BtnStop.EnableWindow(FALSE);
	CMenu* pMenu = GetMenu();
	if (pMenu)
	{
	//	pMenu->EnableMenuItem(ID_MENU_MODIFY_ID, MF_DISABLED);	//无需更改，因为网络模式下暂不支持台站ID修改，因此此项一直是Disable状态
		pMenu->EnableMenuItem(ID_MENU_INITIALIZE_CLOCK, MF_DISABLED);
		pMenu->EnableMenuItem(ID_MENU_AUTO_SERV_OPEN, MF_DISABLED);
		pMenu->EnableMenuItem(ID_MENU_AUTO_SERV_CLOSE, MF_DISABLED);
		m_toolbar.GetToolBarCtrl().EnableButton(ID_MENU_TEST_MODE, TRUE);
	//	m_toolbar.GetToolBarCtrl().EnableButton(ID_MENU_MODIFY_ID, FALSE);	//无需更改，因为网络模式下暂不支持台站ID修改，因此此项一直是Disable状态
		m_toolbar.GetToolBarCtrl().EnableButton(ID_MENU_INITIALIZE_CLOCK, FALSE);
		m_toolbar.GetToolBarCtrl().EnableButton(ID_MENU_AUTO_SERV_OPEN, FALSE);
		m_toolbar.GetToolBarCtrl().EnableButton(ID_MENU_AUTO_SERV_CLOSE, FALSE);
	}
////根据工具栏第一个按钮状态来判断应当还原回普通模式还是测试模式
	if (m_toolbar.GetToolBarCtrl().IsButtonChecked(ID_MENU_TEST_MODE))
	{
		m_pAwsdataStorage->m_ProcessFlag = 1;	//还原回测试模式
		pMenu->EnableMenuItem(ID_MENU_NORMAL_MODE, MF_ENABLED);	//普通模式菜单应置为可用
	}
	else
	{
		m_pAwsdataStorage->m_ProcessFlag = 0;	//还原回普通模式
		pMenu->EnableMenuItem(ID_MENU_TEST_MODE, MF_ENABLED);	//测试模式菜单应置为可用
	}
	//更新状态栏
	m_statusbar.SetPaneText(1, _T("网络通信：已关闭"));
////更改连接方式标识
	ConnectionModeFlag = 0;
}


void CAutoWeatherStationDlg::OnBnClickedBtnCe()
{
	// TODO:  在此添加控件通知处理程序代码
	//清空信息显示控件
	m_RichEdit2Info.SetWindowText(_T(""));
}


void CAutoWeatherStationDlg::OnEnChangeRichedit2Info()
{
	// TODO:  如果该控件是 RICHEDIT 控件，它将不
	// 发送此通知，除非重写 CDialogEx::OnInitDialog()
	// 函数并调用 CRichEditCtrl().SetEventMask()，
	// 同时将 ENM_CHANGE 标志“或”运算到掩码中。

	// TODO:  在此添加控件通知处理程序代码

	//////////////////////////////////////////////////
	//主要功能是实现RichEdit控件中文字的自动变色
	//
////配置文字属性：蓝色
	CHARFORMAT cf;
	ZeroMemory(&cf, sizeof(CHARFORMAT));
	cf.cbSize = sizeof(CHARFORMAT);
	cf.dwMask = CFM_BOLD | CFM_COLOR | CFM_FACE | CFM_ITALIC | CFM_SIZE | CFM_UNDERLINE;
//	cf.dwEffects = CFE_AUTOCOLOR;
//	cf.dwEffects = CFE_BOLD | CFE_LINK;
	cf.yHeight = 180;	//文字高度
	cf.crTextColor = RGB(0, 0, 255); //文字颜色：蓝色
	_tcscpy_s(cf.szFaceName, _T("宋体"));//设置字体

////查找字符串“成功”，并将其设置为绿色
	FINDTEXTEX ft;
	ft.chrg.cpMin = m_lLastSel;	//从上次的光标末位开始查找
	ft.chrg.cpMax = -1;
	ft.lpstrText = _T("成功");
//	long n = m_RichEdit2Info.FindText(FR_DOWN | FR_MATCHCASE | FR_WHOLEWORD, &ft);	//向后搜索 | 区分大小写 | 全字匹配
	long n = m_RichEdit2Info.FindText(FR_DOWN, &ft);	//向后搜索（必须）
	if (n != -1)
	{
		m_RichEdit2Info.SetSel(ft.chrgText);
		m_RichEdit2Info.SetSelectionCharFormat(cf);	//设置选中的字符串
	}
////查找字符串“失败”，并将其设置为红色
	cf.crTextColor = RGB(255, 0, 0); //文字颜色：红色
	ft.lpstrText = _T("失败");
	n = m_RichEdit2Info.FindText(FR_DOWN, &ft);
	if (n != -1)
	{  
		m_RichEdit2Info.SetSel(ft.chrgText);
		m_RichEdit2Info.SetSelectionCharFormat(cf);
	}
////将光标置于最后，同时恢复默认的黑色文字
	m_RichEdit2Info.SetSel(-1, -1);	//取消选中并将光标置于最后
	cf.crTextColor = RGB(0, 0, 0);	//文字颜色
	m_RichEdit2Info.SetSelectionCharFormat(cf);
////获取光标坐标并将其存储
	long nStartChar, nEndChar;
	m_RichEdit2Info.GetSel(nStartChar, nEndChar);
	if (nStartChar == nEndChar)
		m_lLastSel = nStartChar;	//如果未选中任何文字，则将光标位置存入对话框成员变量m_lLastSel中
}


void CAutoWeatherStationDlg::OnBnClickedBtnOpenhistory()
{
	// TODO:  在此添加控件通知处理程序代码
////创建历史数据对话框
	if (m_HistoryDlg.Create(IDD_DIALOG_HISTORY) == NULL)
	{
		AfxMessageBox(_T("历史数据对话框创建失败！"));
		return;
	}
//	m_HistoryDlg.SetBackgroundColor(RGB(255, 255, 255));	//设置背景色为全白
	m_HistoryDlg.ShowWindow(TRUE);
}


void CAutoWeatherStationDlg::OnBnClickedBtnOpenMeter()	//动态创建Meter对话框，暂时一次只能创建一个，即只能点此按钮一下
{
	// TODO:  在此添加控件通知处理程序代码
	CRect rectMain, rectMeter;
	//获取屏幕大小，不包括任务栏
	int cx = ::GetSystemMetrics(SM_CXFULLSCREEN);
	int cy = ::GetSystemMetrics(SM_CYFULLSCREEN);
	//显示或隐藏Meter对话框，并调整位置
	if (m_pVMdlg->IsWindowVisible())
	{
		GetWindowRect(rectMain);
		rectMain.MoveToXY((cx - rectMain.Width()) / 2, (cy - rectMain.Height()) / 2);	//恢复居中显示
		MoveWindow(rectMain);

		m_pVMdlg->ShowWindow(SW_HIDE);
	}
	else
	{
		//获取两个对话框位置信息
		GetWindowRect(rectMain);
		m_pVMdlg->GetWindowRect(rectMeter);
		//重新布置对话框位置
		rectMain.MoveToXY((cx - rectMain.Width() - rectMeter.Width()) / 2, (cy - rectMain.Height()) / 2);
		MoveWindow(rectMain);
		rectMeter.MoveToXY(rectMain.right, rectMain.top);
		m_pVMdlg->MoveWindow(rectMeter);

		m_pVMdlg->ShowWindow(SW_SHOW);
	}
}


void CAutoWeatherStationDlg::OnCancel()	//为空，回车按钮不处理
{
	// TODO:  在此添加专用代码和/或调用基类

//	CDialogEx::OnCancel();
}


void CAutoWeatherStationDlg::OnOK()	//为空，Esc按钮不处理
{
	// TODO:  在此添加专用代码和/或调用基类

//	CDialogEx::OnOK();
}


void CAutoWeatherStationDlg::OnClose()	//处理WM_CLOSE消息，不为空，使用默认调用，使得点击右上角关闭按钮的时候程序可以关闭
{
	// TODO:  在此添加消息处理程序代码和/或调用默认值
////销毁InitialDialog中new出来的资源
	delete m_pAwsdataStorage;
	m_pAwsdataStorage = NULL;
	delete m_pVMdlg;
	m_pVMdlg = NULL;
	delete m_pstrStation;
	m_pstrStation = NULL;
////关闭log文件句柄
	try
	{
		m_LogFile.Close();	//关闭文件并销毁对象
	}
	catch (CFileException* pe)
	{
		TRACE(_T("File could not be closed, cause = %d\n"),
			pe->m_cause);
		pe->Delete();
	}
//// 删除窗口的寻找标记
::RemoveProp(m_hWnd, AfxGetApp()->m_pszExeName);
	
////销毁窗口	
//	DestroyWindow();		//用于销毁非模态窗口
	//用于销毁模态窗口，【注】EndDialog会使dlg.DoModal()返回，即结束模态对话框，而IDCANCEL或IDOK则是Domodal()的返回值，可以自定义，
	//此处返回IDCANCEL，将会传回给CAutoWeatherStationApp::InitInstance()中的nResponse，然后使得InitInstance()正常结束并返回FALSE，最终导致主线程退出，程序退出
	EndDialog(IDCANCEL);	//IDCANCEL或IDOK均可，也可以是其他任意值，因为此处只要使主模态对话框返回即可终止整个程序
	CDialogEx::OnClose();	//模态对话框返回，程序不会运行到这里
}


void CAutoWeatherStationDlg::OnBnClickedBtnComopen()
{
	// TODO:  在此添加控件通知处理程序代码
	UpdateData(TRUE);
	m_RichEdit2Info.SetFocus();	//获取焦点，否则RichEdit控件无法自动滚动
////初始化CnComm串口
	UINT mBaudrate[11] = { 300, 600, 1200, 4800, 9600, 19200, 38400, 43000, 56000, 57600, 115200 };
	UINT mDatabit[3] = { 8, 7, 6 };
	UINT mCom = m_nCom + 1;
	if (m_com.IsOpen())
	{
		m_RichEdit2Info.ReplaceSel(_T("打开串口...失败\r\n"));
		AfxMessageBox(_T("打开串口失败，可能串口已被其他程序占用！"));
		return;
	}
	//【注意】CnComm中的所有Open函数均调用了串口超时函数SetupPort()，而SetupPort函数中则直接调用了串口超时API：GetCommTimeouts()和SetCommTimeouts()
	m_RichEdit2Info.ReplaceSel(_T("打开串口..."));
	if(m_com.Open(mCom, mBaudrate[m_nBaudrate], m_nCheckbit, mDatabit[m_nDatabit], m_nStopbit) != true)	//设置串口参数并打开串口
	{
		m_RichEdit2Info.ReplaceSel(_T("失败\r\n"));
		AfxMessageBox(L"打开串口失败，请重试！");
		return;
	}
	m_RichEdit2Info.ReplaceSel(_T("\t\t\t\t  成功\r\n"));

	m_com.SetWnd(AfxGetMainWnd()->m_hWnd);	//！！！！【必需】！！！！设置消息处理窗口为主窗口（处理接收到数据的函数在主窗口的OnComReceive中）
////更改界面交互状态
	m_BtnComopen.EnableWindow(FALSE);
	m_BtnComclose.EnableWindow(TRUE);
	m_BtnListen.EnableWindow(FALSE);
	m_BtnStop.EnableWindow(FALSE);
	CMenu* pMenu = GetMenu();
	if (pMenu)
	{
		pMenu->EnableMenuItem(ID_MENU_NORMAL_MODE, MF_DISABLED);
		pMenu->EnableMenuItem(ID_MENU_TEST_MODE, MF_DISABLED);
		m_toolbar.GetToolBarCtrl().EnableButton(ID_MENU_TEST_MODE, FALSE);
		//仅允许从普通模式中进行以下操作
		if (m_pAwsdataStorage->m_ProcessFlag == 0)
		{
			pMenu->EnableMenuItem(ID_MENU_MODIFY_ID, MF_ENABLED);
			pMenu->EnableMenuItem(ID_MENU_INITIALIZE_CLOCK, MF_ENABLED);
			pMenu->EnableMenuItem(ID_MENU_AUTO_SERV_OPEN, MF_ENABLED);
		//	pMenu->EnableMenuItem(ID_MENU_AUTO_SERV_CLOSE, MF_ENABLED);
			m_toolbar.GetToolBarCtrl().EnableButton(ID_MENU_MODIFY_ID, TRUE);
			m_toolbar.GetToolBarCtrl().EnableButton(ID_MENU_INITIALIZE_CLOCK, TRUE);
			m_toolbar.GetToolBarCtrl().EnableButton(ID_MENU_AUTO_SERV_OPEN, TRUE);
		//	m_toolbar.GetToolBarCtrl().EnableButton(ID_MENU_AUTO_SERV_CLOSE, TRUE);
		}
	}
	//更新状态栏
	m_statusbar.SetPaneText(0, _T("串口通信：已开启"));
////更改连接方式标识
	ConnectionModeFlag = 1;	//供其他对象查询，指示串口连接已建立
	//焦点移至输入框
	GetDlgItem(IDC_EDIT_CMD)->SetFocus();
//	UpdateData(FALSE);
}


void CAutoWeatherStationDlg::OnBnClickedBtnComclose()
{
	// TODO:  在此添加控件通知处理程序代码
	m_RichEdit2Info.SetFocus();	//获取焦点，否则RichEdit控件无法自动滚动
////关闭串口
	m_RichEdit2Info.ReplaceSel(_T("关闭串口>>>"));
	m_com.Close();
	if (m_com.IsOpen())
	{
		m_RichEdit2Info.ReplaceSel(_T("失败\r\n"));
		AfxMessageBox(L"关闭串口失败，请重试！");
		return;
	}
	m_RichEdit2Info.ReplaceSel(_T("\t\t\t\t  成功\r\n"));
////更改界面交互状态
	m_BtnComopen.EnableWindow(TRUE);
	m_BtnComclose.EnableWindow(FALSE);
	m_BtnListen.EnableWindow(TRUE);
	m_BtnStop.EnableWindow(FALSE);
	CMenu* pMenu = GetMenu();
	if (pMenu)
	{
		pMenu->EnableMenuItem(ID_MENU_MODIFY_ID, MF_DISABLED);
		pMenu->EnableMenuItem(ID_MENU_INITIALIZE_CLOCK, MF_DISABLED);
		pMenu->EnableMenuItem(ID_MENU_AUTO_SERV_OPEN, MF_DISABLED);
		pMenu->EnableMenuItem(ID_MENU_AUTO_SERV_CLOSE, MF_DISABLED);
		m_toolbar.GetToolBarCtrl().EnableButton(ID_MENU_TEST_MODE, TRUE);
		m_toolbar.GetToolBarCtrl().EnableButton(ID_MENU_MODIFY_ID, FALSE);
		m_toolbar.GetToolBarCtrl().EnableButton(ID_MENU_INITIALIZE_CLOCK, FALSE);
		m_toolbar.GetToolBarCtrl().EnableButton(ID_MENU_AUTO_SERV_OPEN, FALSE);
		m_toolbar.GetToolBarCtrl().EnableButton(ID_MENU_AUTO_SERV_CLOSE, FALSE);
	}
	//根据工具栏第一个按钮状态来判断应当还原回普通模式还是测试模式
	if (m_toolbar.GetToolBarCtrl().IsButtonChecked(ID_MENU_TEST_MODE))
	{
		m_pAwsdataStorage->m_ProcessFlag = 1;	//还原回测试模式
		pMenu->EnableMenuItem(ID_MENU_NORMAL_MODE, MF_ENABLED);	//普通模式菜单应置为可用
	}
	else
	{
		m_pAwsdataStorage->m_ProcessFlag = 0;	//还原回普通模式
		pMenu->EnableMenuItem(ID_MENU_TEST_MODE, MF_ENABLED);	//测试模式菜单应置为可用
	}
	//更新状态栏
	m_statusbar.SetPaneText(0, _T("串口通信：已关闭"));
////更改连接方式标识
	ConnectionModeFlag = 0;
}


////CnComm串口类的接收数据响应函数
afx_msg LRESULT CAutoWeatherStationDlg::OnComReceive(WPARAM wParam, LPARAM lParam)
{
////读取串口上的字符
	char str[256];
	memset(str, '\0', 256);
	m_com.ReadString(str, 256);	//【注意】接收到的字符串不能超限

////【！重要！】利用CStringT模板类实现CStringW与CStringA的相互转换！（方法一）
	CString cstrTemp;
	CStringA cstraTemp = str;
	cstrTemp = cstraTemp;	//CStingA转换为CString
////将数据处理后存入数据库
	TRACE(cstrTemp);
	m_pAwsdataStorage->StoreString(cstrTemp);	//调用CAwsdataStorage类中的StoreString方法，将接收到的数据处理后存入数据库中

	return 0;
}


void CAutoWeatherStationDlg::OnBnClickedButtonSend()
{
	// TODO:  在此添加控件通知处理程序代码
	if (ConnectionModeFlag == 0)	//连接方式标识为0出错
	{
//		AfxMessageBox(_T("请首先打开串口或网络！"));
		m_RichEdit2Info.ReplaceSel(_T("操作失败，请首先打开串口或网络！\r\n"));
		return;
	}
	//将ProcessFlag置为1（进入调试模式）
//	m_pAwsdataStorage->m_ProcessFlag = 1;
	//获取发送编辑框文本并存入CString中
	CString strTemp;
	GetDlgItemText(IDC_EDIT_CMD, strTemp);
	CStringA strText(strTemp);	//利用CStringA的构造函数，初始化strText的同时进行类型转换，将CString（Unicode）型转换为多字节的char型
	if (strText == "")
	{
		AfxMessageBox(_T("发送内容不能为空！"));
		return;
	}
	if (ConnectionModeFlag == 1)
	{
		m_com.Write(strText + "\r\n");	//通过串口发送（后台自动补上\r\n）
	}
	else if (ConnectionModeFlag == 2)
	{
		if (m_iSocket->m_listSockets.GetCount() != 0)	//有客户端建立连接
		{
			CClientSocket* cc = (CClientSocket*)m_iSocket->m_listSockets.GetTail();	//暂时只取最后一个客户端进行操作（位于List的尾部），这样连接断掉再新建仍可找到最新的Socket连接
			cc->Send(strText + "\r\n", strText.GetLength() + 2);
		}
	}
	//简单配置命令（长度<5）自动清屏
	if (strText.GetLength() < 5)
	{
		((CButton*)GetDlgItem(IDC_EDIT_CMD))->SetWindowText(_T(""));
	}
}


void CAutoWeatherStationDlg::OnBnClickedCheckRpt()
{
	// TODO:  在此添加控件通知处理程序代码
	if (m_CheckRpt.GetCheck())	//如果选中，则打开重复发送定时器
	{
		UpdateData(TRUE);
		SetTimer(TIMERID_AUTOSEND, m_uEditRpt, NULL);	//定时时间长度为m_uEditRpt
	}
	else						//否则，关闭重复发送定时器
	{
		KillTimer(TIMERID_AUTOSEND);
	}
}


void CAutoWeatherStationDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO:  在此添加消息处理程序代码和/或调用默认值
	//定义变量
	CTime time;
	CString strtime;
	//处理
	switch (nIDEvent)
	{
	case TIMERID_AUTOSEND:
		PostMessage(WM_COMMAND, MAKEWPARAM(IDC_BUTTON_SEND, BN_CLICKED), NULL);	//发送消息，模拟“发送”按钮的动作
		break;
	case TIMERID_AUTOSERV:
//		m_AutoServEvent.PulseEvent();	//按照设定的时间释放事件标识
		m_AutoServEvent.SetEvent();	//自动监测线程中多个WaitForSingleObject，因此无法确保PulseEvent每次都有效，因此修改为使用SetEvent和ResetEvent
		break;
	case TIMERID_REFRESHTIME:
		//获得系统当前时间	
		time = time.GetCurrentTime();
		strtime.Format(_T("%s"), time.Format("当前时间：%Y-%m-%d %H:%M:%S"));
		//更新状态栏时间
		m_statusbar.SetPaneText(2, strtime);
	default:
		break;
	}
	CDialogEx::OnTimer(nIDEvent);
}

////Combobox选择修改事件触发
void CAutoWeatherStationDlg::OnSelchangeComboCmd()
{
	// TODO:  在此添加控件通知处理程序代码
////将Combobox选中的内容显示到Edit控件中
	CString str;
	m_ComboCmd.GetWindowText(str);
//	str += "\r\n";
	SetDlgItemText(IDC_EDIT_CMD, str);
}

////菜单：更改台站ID
void CAutoWeatherStationDlg::OnMenuModifyId()
{
	// TODO:  在此添加命令处理程序代码
	CModifyIdDlg midlg;
	midlg.DoModal();
}

////菜单：初始化设备时钟
void CAutoWeatherStationDlg::OnMenuInitializeClock()
{
	// TODO:  在此添加命令处理程序代码
	int count = m_List.GetCount();
	if (count == 0)
	{
		m_RichEdit2Info.SetFocus();	//焦点转移至RichEdit控件
		m_RichEdit2Info.SetSel(-1, -1);	//光标置于最后
		m_RichEdit2Info.ReplaceSel(_T("初始化台站时钟失败，请先将台站信息扫描至软件！\r\n"));
		AfxMessageBox(_T("初始化台站时钟之前请先将台站信息扫描至软件！"));
	}
	else
	{
		//将ProcessFlag置为4（进入初始化台站时钟模式）
		m_pAwsdataStorage->m_ProcessFlag = 4;
		//创建新线程，用于初始化台站时钟
		CWinThread* pThread = AfxBeginThread(ClockInitThread, NULL);	//不向新线程传递任何参数
	}
}

////菜单：进入普通模式
void CAutoWeatherStationDlg::OnMenuNormalMode()
{
	// TODO:  在此添加命令处理程序代码
	//将ProcessFlag置为0（进入普通模式，数据直接从后台进入数据库）
	m_pAwsdataStorage->m_ProcessFlag = 0;
	//修改界面交互状态
	CButton* pBtn = (CButton*)GetDlgItem(IDC_BUTTON_SEND);
	pBtn->EnableWindow(FALSE);
	m_CheckRpt.EnableWindow(FALSE);
	m_ComboCmd.EnableWindow(FALSE);
	CEdit* pEdit = (CEdit*)GetDlgItem(IDC_EDIT_CMD);
	pEdit->EnableWindow(FALSE);
	pEdit = (CEdit*)GetDlgItem(IDC_EDIT_RPT);
	pEdit->EnableWindow(FALSE);
	CMenu* pMenu = GetMenu();
	if (pMenu)
	{
		pMenu->EnableMenuItem(ID_MENU_NORMAL_MODE, MF_DISABLED);
		pMenu->EnableMenuItem(ID_MENU_TEST_MODE, MF_ENABLED);
	}
	m_toolbar.GetToolBarCtrl().CheckButton(ID_MENU_TEST_MODE, FALSE);
}

////菜单：进入调试模式
void CAutoWeatherStationDlg::OnMenuTestMode()
{
	// TODO:  在此添加命令处理程序代码
	//配合ToolBar中的第一个模式按钮做判断
	if (m_pAwsdataStorage->m_ProcessFlag == 0)	//若之前是普通模式，则切换至Test Mode
	{
		//将ProcessFlag置为1（进入调试模式，手动控制命令的发送，并且接收数据直接显示在RichEdit中）
		m_pAwsdataStorage->m_ProcessFlag = 1;
		//修改界面交互状态
		CButton* pBtn = (CButton*)GetDlgItem(IDC_BUTTON_SEND);
		pBtn->EnableWindow(TRUE);
		m_CheckRpt.EnableWindow(TRUE);
		m_ComboCmd.EnableWindow(TRUE);
		CEdit* pEdit = (CEdit*)GetDlgItem(IDC_EDIT_CMD);
		pEdit->EnableWindow(TRUE);
		pEdit = (CEdit*)GetDlgItem(IDC_EDIT_RPT);
		pEdit->EnableWindow(TRUE);
		CMenu* pMenu = GetMenu();
		if (pMenu)
		{
			pMenu->EnableMenuItem(ID_MENU_NORMAL_MODE, MF_ENABLED);
			pMenu->EnableMenuItem(ID_MENU_TEST_MODE, MF_DISABLED);
		}
		m_toolbar.GetToolBarCtrl().CheckButton(ID_MENU_TEST_MODE, TRUE);
	}
	else if (m_pAwsdataStorage->m_ProcessFlag == 1)	//若之前是测试模式，则切换至Normal Mode
	{
		OnMenuNormalMode();
	}
	else
	{
		AfxMessageBox(_T("出现界面逻辑错误，请重试！"));	//程序不应当运行至此处，否则应查找用户界面控制逻辑的漏洞
	}
}

////菜单：自动检测服务开启
void CAutoWeatherStationDlg::OnMenuAutoServOpen()
{
	// TODO:  在此添加命令处理程序代码
	//将ProcessFlag置为0，进入普通模式
	m_pAwsdataStorage->m_ProcessFlag = 0;
	//从Config.ini文件中恢复上次保存的【自动监测服务循环扫描的时间间隔】
	UINT uLoopTime;
	uLoopTime = GetPrivateProfileInt(_T("Global"), _T("LoopTime"), 5000, _T(".\\Config.ini"));	//注意此函数直接返回UINT类型,默认返回5000
	//开启定时器TIMERID_AUTOSERV
	m_AutoServEvent.PulseEvent();	//为保证响应及时性，先释放一次事件标识
	SetTimer(TIMERID_AUTOSERV, uLoopTime, NULL);	//指定循环扫描的时间间隔【注意：此值应当参照自动监测服务超时时间AutoServWaitTime来进行确定！，即应满足LoopTime > AutoServWaitTime * 台站数】
	//修改界面交互状态
	CMenu* pMenu = GetMenu();
	if (pMenu)
	{
		pMenu->EnableMenuItem(ID_MENU_MODIFY_ID, MF_DISABLED);
		pMenu->EnableMenuItem(ID_MENU_INITIALIZE_CLOCK, MF_DISABLED);
		pMenu->EnableMenuItem(ID_MENU_AUTO_SERV_OPEN, MF_DISABLED);
		pMenu->EnableMenuItem(ID_MENU_AUTO_SERV_CLOSE, MF_ENABLED);
	}
	m_toolbar.GetToolBarCtrl().EnableButton(ID_MENU_MODIFY_ID, FALSE);
	m_toolbar.GetToolBarCtrl().EnableButton(ID_MENU_INITIALIZE_CLOCK, FALSE);
	m_toolbar.GetToolBarCtrl().EnableButton(ID_MENU_AUTO_SERV_OPEN, FALSE);
	m_toolbar.GetToolBarCtrl().EnableButton(ID_MENU_AUTO_SERV_CLOSE, TRUE);
	//约束条件，开启服务后不可以关闭串口或网络通信
	m_BtnComclose.EnableWindow(FALSE);
	m_BtnStop.EnableWindow(FALSE);
}

////菜单：自动检测服务关闭
void CAutoWeatherStationDlg::OnMenuAutoServClose()
{
	// TODO:  在此添加命令处理程序代码
	//关闭定时器TIMERID_AUTOSERV
	KillTimer(TIMERID_AUTOSERV);
	//修改界面交互状态
	CMenu* pMenu = GetMenu();
	if (pMenu)
	{
		if (ConnectionModeFlag == 1)	//只有当前为串口方式连接时，才应重新允许台站更改对话框
		{
			pMenu->EnableMenuItem(ID_MENU_MODIFY_ID, MF_ENABLED);
			m_toolbar.GetToolBarCtrl().EnableButton(ID_MENU_MODIFY_ID, TRUE);
		}
		pMenu->EnableMenuItem(ID_MENU_INITIALIZE_CLOCK, MF_ENABLED);
		pMenu->EnableMenuItem(ID_MENU_AUTO_SERV_OPEN, MF_ENABLED);
		pMenu->EnableMenuItem(ID_MENU_AUTO_SERV_CLOSE, MF_DISABLED);
	}
	m_toolbar.GetToolBarCtrl().EnableButton(ID_MENU_INITIALIZE_CLOCK, TRUE);
	m_toolbar.GetToolBarCtrl().EnableButton(ID_MENU_AUTO_SERV_OPEN, TRUE);
	m_toolbar.GetToolBarCtrl().EnableButton(ID_MENU_AUTO_SERV_CLOSE, FALSE);
	//约束条件，关闭服务后才可以关闭串口或网络通信
	if (ConnectionModeFlag == 1)	//若当前为串口方式连接
	{
		m_BtnComclose.EnableWindow(TRUE);
	}
	else if(ConnectionModeFlag == 2)	//若当前为网络方式连接
	{
		m_BtnStop.EnableWindow(TRUE);
	}
}

////菜单：清空信息显示区
void CAutoWeatherStationDlg::OnMenuCe()
{
	// TODO:  在此添加命令处理程序代码
	m_RichEdit2Info.SetWindowText(_T(""));
}

////菜单：查询历史数据
void CAutoWeatherStationDlg::OnMenuHistory()
{
	// TODO:  在此添加命令处理程序代码
	if (m_HistoryDlg.IsWindowVisible())
	{
		m_HistoryDlg.ShowWindow(SW_HIDE);
		m_toolbar.GetToolBarCtrl().CheckButton(ID_MENU_HISTORY, FALSE);	//同步Toolbox中的按钮
	}
	else
	{
		m_HistoryDlg.ShowWindow(SW_SHOW);
		m_toolbar.GetToolBarCtrl().CheckButton(ID_MENU_HISTORY, TRUE);	//同步Toolbox中的按钮
	}
}

////菜单：查看传感器状态
void CAutoWeatherStationDlg::OnMenuMeter()
{
	// TODO:  在此添加命令处理程序代码
	if (m_pVMdlg->IsWindowVisible())
	{
		m_pVMdlg->ShowWindow(SW_HIDE);
		m_toolbar.GetToolBarCtrl().CheckButton(ID_MENU_METER, FALSE);	//同步Toolbox中的按钮
	}
	else
	{
		m_pVMdlg->ShowWindow(SW_SHOW);
		m_toolbar.GetToolBarCtrl().CheckButton(ID_MENU_METER, TRUE);	//同步Toolbox中的按钮
	}
}


////菜单：“关于”对话框
void CAutoWeatherStationDlg::OnAbout()
{
	// TODO:  在此添加命令处理程序代码
	CAboutDlg dlg;
	dlg.DoModal();
}


////自定义消息响应，用于响应自动监测进程发出的获取Listbox控件信息的消息
afx_msg LRESULT CAutoWeatherStationDlg::OnListboxInfo(WPARAM wParam, LPARAM lParam)
{
	int flag = (int)wParam;
	if (flag == 1)	//收到获取ListBox列数的请求
	{
		g_Count = m_List.GetCount();
	}
	else if (flag == 2)	//收到获取ListBox字符串的请求
	{
		int i = (int)lParam;
		if (m_List.GetCount() > i)	//确认列表中仍有足够列的数据可读取，防止扫描过程中网络中断导致m_List被清空，从而导致本处操作控件出错！
		{
			m_List.GetText(i, g_String);
		}
		else	//否则提示错误
		{
			m_RichEdit2Info.ReplaceSel(_T("台站列表被更改，服务将自动重启，请稍后...\r\n"));
			m_RichEdit2Info.SetFocus();
			g_String = _T("");	//写入空值，使扫描线程空闲
		}
		m_AquireInfoEvent.PulseEvent();	//释放事件标志
	}
	else if (flag == 3)	//收到向信息显示框打印相关信息的请求
	{
		int id = (int)lParam;
		if (id == 1)
		{
			m_RichEdit2Info.ReplaceSel(_T("数据确认...\t\t成功\r\n"));
		}
		else if (id == 2)
		{
			m_RichEdit2Info.ReplaceSel(_T("子台站信息即将返回...\r\n"));
		}
		else if (id == 3)
		{
			m_RichEdit2Info.ReplaceSel(_T("子台站列表返回\t成功\r\n"));
		}
		else if (id == 4)
		{
			m_RichEdit2Info.ReplaceSel(_T("子台站列表返回\t失败\r\n即将进行下一次尝试\r\n"));
		}
		else if (id == 5)
		{
			m_RichEdit2Info.ReplaceSel(_T("5次尝试均失败，请检查是否存在网络线路问题，然后重启网络连接！\r\n"));
		}
	}

	return 0;
}


//双击ListBox弹出对应的Meter对话框
void CAutoWeatherStationDlg::OnDblclkList()
{
	// TODO:  在此添加控件通知处理程序代码
	int nIndex = m_List.GetCurSel();
	int nCount = m_List.GetCount();
	if ((nIndex != LB_ERR) && (nCount > 1))	//如果listbox中有1个以上的item，并且选中了一个
	{
		m_List.GetText(nIndex, *m_pstrStation);
		if (m_pstrStation->Find(_T("MS")) != -1)	//若点击的是子台站名
		{
			*m_pstrStation = m_pstrStation->Mid(1, 8);
			m_pVMdlg->PostMessage(WM_REFRESH_VIRTUAL_METER, (WPARAM)m_pstrStation, (LPARAM)1);
			m_pVMdlg->ShowWindow(SW_SHOW);
		}
	}
}


//Meter窗口跟随主窗口效果
void CAutoWeatherStationDlg::OnMove(int x, int y)
{
	
/*	//此函数暂时弃用，窗口跟随会产生副作用：主窗口最小化后，Meter窗口被隐藏
	CDialogEx::OnMove(x, y);

	// TODO:  在此处添加消息处理程序代码
	CRect rectMain, rectMeter;
	//获取两个对话框位置信息
	GetWindowRect(rectMain);
	m_pVMdlg->GetWindowRect(rectMeter);
	//Meter对话框跟随
	rectMeter.MoveToXY(rectMain.right, rectMain.top);
	m_pVMdlg->MoveWindow(rectMeter);
*/
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//子线程，全局函数
//
//台站时钟初始化线程
UINT ClockInitThread(LPVOID pParam)
{
	CAutoWeatherStationDlg* pDlg = (CAutoWeatherStationDlg*)theApp.GetMainWnd();
	int count = pDlg->m_List.GetCount();	//获取主界面对话框ListBox中的台站列表
	//从Config.ini文件中恢复上次保存的【初始化台站时钟超时时间】和【为匹配数据采集器(Data Acquisition Unit)的时序而设置的延时】
	UINT uInitClkWaitTime, uDelayForDAU;
	uInitClkWaitTime = GetPrivateProfileInt(_T("Global"), _T("InitClkWaitTime"), 3000, _T(".\\Config.ini"));	//注意此函数直接返回UINT类型,默认返回3000
	uDelayForDAU = GetPrivateProfileInt(_T("Global"), _T("DelayForDAU"), 1500, _T(".\\Config.ini"));	//注意此函数直接返回UINT类型,默认返回1500
////开始扫描
	pDlg->m_BtnStop.EnableWindow(FALSE);	//先禁止用户点击“停止监听”按钮
	CString strID, strTime, strCmd, strInfo;
	CTime tTime;
	for (int i = 0; i < count; i++)	//【注意】循环过程中不可以使ListBox列表为空（例如不可以点击主界面“停止监听”按钮！），否则会抛出异常
	{
		pDlg->m_List.GetText(i, strID);
		if (strID.Find(_T("MS")) == -1)	//没找到台站名前缀MS，则直接跳过本次循环
			continue;
		tTime = CTime::GetCurrentTime();
		strTime = tTime.Format(_T("%Y-%m-%d %H:%M:%S"));
		strCmd = strID.Mid(strID.Find(_T("MS")), 8) +_T(" ") + _T("ST_CLK") + _T(" ") + strTime;
		//Write串口需要传送StringA格式的字串
		CStringA stra(strCmd);
		if (pDlg->ConnectionModeFlag == 1)	//若连接方式为串口
		{
			pDlg->m_com.Write(stra + "\r\n");	//注意串口传输应以\r\n为结束标识
		}
		else if (pDlg->ConnectionModeFlag == 2)	//若连接方式为网络
		{
			stra += "\r\n";	//注意网络传输应以\r\n为结束标识
			CClientSocket* cc = (CClientSocket*)pDlg->m_iSocket->m_listSockets.GetTail();	//暂时只取最后一个客户端进行操作（位于List的尾部）
			cc->Send(stra, stra.GetLength());
		}
		//提示信息
		strInfo = _T("初始化台站") + strID.Mid(strID.Find(_T("MS")), 8) + _T("时钟...");
		pDlg->m_RichEdit2Info.SetSel(-1, -1);	//光标置于最后
		pDlg->m_RichEdit2Info.ReplaceSel(strInfo);
		//超时判定
		DWORD ret = ::WaitForSingleObject(pDlg->m_pAwsdataStorage->m_ClockInitEvent, uInitClkWaitTime);	//由于初始化时钟命令返回时间较长，此处超时时间设为3s
		if (ret == WAIT_OBJECT_0)		//如果未超时，即超时之前Event得以释放
		{
			pDlg->m_RichEdit2Info.ReplaceSel(_T("\t\t  成功\r\n"));
			pDlg->m_RichEdit2Info.SetFocus();
		}
		else if (ret == WAIT_TIMEOUT)	//如果超时
		{
			pDlg->m_RichEdit2Info.ReplaceSel(_T("\t\t  失败\r\n"));
			pDlg->m_RichEdit2Info.SetFocus();
		}
		else if (ret == WAIT_FAILED)	//如果出错
		{
			AfxMessageBox(_T("线程同步出错！请重试！"));
			break;	//直接跳出循环
		}
		if (pDlg->ConnectionModeFlag == 2)	//网络连接方式下需增加延时函数以匹配采集器的时序
		{
			::Sleep(uDelayForDAU);	//每个循环结尾延时一段时间以确保下次发送的命令能够成功执行
		}
	}
	pDlg->m_BtnStop.EnableWindow(TRUE);	//重新允许用户点击“停止监听”按钮

	return 0;
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//子线程，全局函数
//
//自动监测线程，注意此线程在OnInitDialog()中创建，且创建后不会退出
UINT AutoServThread(LPVOID pParam)
{
	CAutoWeatherStationDlg* pDlg = (CAutoWeatherStationDlg*)theApp.GetMainWnd();
	HWND hDlg = pDlg->GetSafeHwnd();	//获取主对话框句柄
	CString strID, strTime, strCmd;
	CTime tTime;
	//从Config.ini文件中恢复上次保存的【自动监测服务超时时间】和【为匹配数据采集器(Data Acquisition Unit)的时序而设置的延时】
	UINT uAutoServWaitTime, uDelayForDAU;
	uAutoServWaitTime = GetPrivateProfileInt(_T("Global"), _T("AutoServWaitTime"), 1000, _T(".\\Config.ini"));	//注意此函数直接返回UINT类型,默认返回1000
	uDelayForDAU = GetPrivateProfileInt(_T("Global"), _T("DelayForDAU"), 1500, _T(".\\Config.ini"));	//注意此函数直接返回UINT类型,默认返回1500
	while (true)	//死循环，线程函数不会返回
	{
		//无限超时，直到相应事件标识被释放（与主线程中的OnTimer()配合）
		::WaitForSingleObject(pDlg->m_AutoServEvent, INFINITE);
		pDlg->m_AutoServEvent.ResetEvent();	//与主线程定时器OnTimer中的SetEvent对应
	////开始扫描
		//为防止与其他线程操作ListBox控件冲突，本线程使用向主线程发送自定义消息，并通过全局变量返回的方式获取ListBox控件信息
	//	int count = pDlg->m_List.GetCount();	//获取主界面对话框ListBox中的台站列表
		::SendMessage(hDlg, WM_LISTBOX_INFO, 1, NULL);	//向主线程发送获取ListBox控件信息的消息
//		::Sleep(50);	//若上一句使用SendMessage，则无需Sleep；若上一句使用PostMessage，则为保证消息处理完成，需加上Sleep延时（注意存在50ms内未处理完成的情况）
		int count = g_Count;	//从全局变量区获取count最新值
		for (int i = 0; i < count; i++)
		{
		//	pDlg->m_List.GetText(i, strID);
//			::SendMessage(hDlg, WM_LISTBOX_INFO, 2, i);	//注意将i传出
			::PostMessage(hDlg, WM_LISTBOX_INFO, 2, i);	//若使用SendMessage，则无需利用WaitForSingleObject进行等待同步，原因是SendMessage会等待消息处理函数执行完毕才返回
			DWORD r = ::WaitForSingleObject(pDlg->m_AquireInfoEvent, 2000);	//等待主线程操作处理完成
			if (r == WAIT_TIMEOUT)
			{
				pDlg->m_RichEdit2Info.ReplaceSel(_T("线程同步超时！本次数据将被程序自动处理。\r\n"));	//【注】程序很小概率会运行到这里
				continue;
			}
			else if (r == WAIT_OBJECT_0)
			{
				;	//继续执行
			}
			strID = g_String;	//从全局变量区获取str_ID最新值
			if (strID.Find(_T("MS")) == -1)	//没找到台站名前缀MS，则直接跳过本次循环
				continue;
			strCmd = strID.Mid(strID.Find(_T("MS")), 8) + _T(" QU_ALD");
			//Write串口需要传送StringA格式的字串
			CStringA stra(strCmd);
			if (pDlg->ConnectionModeFlag == 1)	//若连接方式为串口
			{
				pDlg->m_com.Write(stra + "\r\n");	//注意串口传输应以\r\n为结束标识
			}
			else if (pDlg->ConnectionModeFlag == 2)	//若连接方式为网络
			{
				stra += "\r\n";	//注意网络传输应以\r\n为结束标识
				CClientSocket* cc = (CClientSocket*)pDlg->m_iSocket->m_listSockets.GetTail();	//暂时只取最后一个客户端进行操作（位于List的尾部）
				cc->Send(stra, stra.GetLength());
			}
			//超时判定
			DWORD ret = ::WaitForSingleObject(pDlg->m_pAwsdataStorage->m_StoreSuccessEvent, uAutoServWaitTime);	//超时时间可设定
			if (ret == WAIT_OBJECT_0)		//说明超时时间内数据存储成功
			{
				::SendMessage(hDlg, WM_LISTBOX_INFO, 3, 1);	//以发送消息的方式向主窗口信息显示窗打印数据存储成功的信息
			}
			else if (ret == WAIT_TIMEOUT)	//如果超时
			{
//				pDlg->m_RichEdit2Info.ReplaceSel(_T("\t\t  失败\r\n"));
			}
			else if (ret == WAIT_FAILED)	//如果出错
			{
				AfxMessageBox(_T("线程同步出错！"));
				break;	//直接跳出循环
			}
			if (pDlg->ConnectionModeFlag == 2)	//网络连接方式下需增加延时函数以匹配采集器的时序
			{
				::Sleep(uDelayForDAU);	//每个循环结尾延时一段时间以确保下次发送的命令能够成功执行
			}
		}
	}
	return 0;
}

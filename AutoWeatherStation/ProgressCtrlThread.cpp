// ProgressCtrlThread.h : 进度条对话框UI线程实现文件
//
#include "stdafx.h"
#include "ProgressCtrlThread.h"
#include "ProgressCtrlThreadDlg.h"


IMPLEMENT_DYNCREATE(CProgressCtrlThread, CWinThread)	//UI线程的实现


//自定义消息
#define WM_THREAD_MESSAGES		WM_USER + 99		//用于与UI线程（而不是其中的对话框）直接通信的自定义消息


BEGIN_MESSAGE_MAP(CProgressCtrlThread, CWinThread)
	ON_THREAD_MESSAGE(WM_THREAD_MESSAGES, &CProgressCtrlThread::OnThreadMessages)	//UI线程的消息应在这里映射！而不是在CDialogEx对话框类中映射
END_MESSAGE_MAP()



CProgressCtrlThread::CProgressCtrlThread()
{
	// TODO:  在此处添加构造代码，
	// 将所有重要的初始化放置在 InitInstance 中
}


// 唯一的一个 CProgressCtrlThread 对象
//CProgressCtrlThread theProgressCtrlThread;	//作为UI线程，此对象可以通过AfxBeginThread()的返回值直接获取，无需使用此全局对象


BOOL CProgressCtrlThread::InitInstance()
{
///*
	CWinThread::InitInstance();	//先启用线程的消息循环

	CProgressCtrlThreadDlg pctdlg;
	m_pMainWnd = &pctdlg;

	INT_PTR nResponse = pctdlg.DoModal();	//虽然是模态对话框，但其只会影响到此处下面的代码不会执行，而整个线程的消息循环仍然是运行的
	if (nResponse == IDOK)
	{
		// TODO:  在此放置处理何时用
		//  “确定”来关闭对话框的代码
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO:  在此放置处理何时用
		//  “取消”来关闭对话框的代码
	}
	else if (nResponse == -1)
	{
		TRACE(traceAppMsg, 0, "警告: 对话框创建失败，UI线程将意外终止。\n");
		TRACE(traceAppMsg, 0, "警告: 如果您在对话框上使用 MFC 控件，则无法 #define _AFX_NO_MFC_CONTROLS_IN_DIALOGS。\n");
	}

	// 由于对话框已关闭，所以将返回 FALSE 以便退出此UI线程，
	// 同时将会关闭线程的消息泵。
	// 如果返回TRUE，则仅仅退出此函数，而不会关闭线程的消息循环
	return FALSE;
//*/

/*	
	//测试非模态对话框用，此用法有问题，ShowWindow并不能及时将对话框显示出来
	CProgressCtrlThreadDlg* pctdlg = new CProgressCtrlThreadDlg;
	pctdlg->Create(IDD_DIALOG_PROGRESS_CTRL);
	pctdlg->ShowWindow(SW_SHOW);
	return TRUE;
*/
}


//本UI线程的自定义消息相应函数，注意线程间消息传递返回void而不是afx_msg LRESULT
void CProgressCtrlThread::OnThreadMessages(WPARAM wParam, LPARAM lParam)
{
//	AfxMessageBox(L"sss");
	CProgressCtrlThreadDlg* pdlg = ((CProgressCtrlThreadDlg*)GetMainWnd());	//注意先将指针从CWnd*强制转换回来！

	int flag = (int)wParam;
	int c = (int)lParam;
	CString str;
	switch (flag)
	{
	case 1:
		pdlg->m_Progress.SetRange(0, c);	//设置ProgressCtrl的总范围
	//	pdlg->m_Progress.SetMarquee(TRUE, 5000);
		pdlg->m_Progress.SetFocus();	//防止默认焦点落在按钮上而误响应回车键
		pdlg->m_StaticText.SetWindowText(_T("数据读取中，请稍后..."));
		break;
	case 2:
		c *= 1.2;	//显示速度加快20%（估算值、试验值）
		pdlg->m_Progress.SetPos(c);	//设置进度位置
		break;
	case 3:
		pdlg->m_Progress.SetPos(c);	//设置为满进度状态
		pdlg->EndDialog(IDCANCEL);	//退出此对话框
		break;
	case 4:
		switch (c)
		{
		case 1:
			pdlg->m_StaticText.SetWindowText(_T("Excel服务器启动中..."));
			break;
		case 2:
			pdlg->m_StaticText.SetWindowText(_T("创建工作簿...\r\nExcel Version: 2003"));
			break;
		case 3:
			pdlg->m_StaticText.SetWindowText(_T("创建工作簿...\r\nExcel Version: 2007"));
			break;
		case 4:
			pdlg->m_StaticText.SetWindowText(_T("创建工作簿...\r\nExcel Version: 2010"));
			break;
		case 5:
			pdlg->m_StaticText.SetWindowText(_T("创建工作簿...\r\nExcel Version: 2013"));
			break;
		case 6:
			pdlg->m_StaticText.SetWindowText(_T("创建工作簿...\r\nExcel Version: other"));
			break;
		case 7:
			pdlg->m_StaticText.GetWindowText(str);
			str = str.Right(str.GetLength() - str.Find(_T("\r\n")));
			str = _T("写入数据...") + str;
			pdlg->m_StaticText.SetWindowText(str);
			break;
		case 8:
			pdlg->m_StaticText.SetWindowText(_T("打开表格..."));
			break;
		default:
			break;
		}
	default:
		break;
	}

	return;
}

// StationSelectDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "AutoWeatherStation.h"
#include "StationSelectDlg.h"
#include "afxdialogex.h"
#include "Awsdata.h"


// CStationSelectDlg 对话框

IMPLEMENT_DYNAMIC(CStationSelectDlg, CDialogEx)

CStationSelectDlg::CStationSelectDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CStationSelectDlg::IDD, pParent)
{

}

CStationSelectDlg::~CStationSelectDlg()
{
}

void CStationSelectDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_STN_LEFT, m_ListStnLeft);
	DDX_Control(pDX, IDC_LIST_STN_RIGHT, m_ListStnRight);
}


BEGIN_MESSAGE_MAP(CStationSelectDlg, CDialogEx)
	ON_BN_CLICKED(IDC_MFCBTN_ADDSEL, &CStationSelectDlg::OnBnClickedMfcbtnAddsel)
	ON_BN_CLICKED(IDC_MFCBTN_DELSEL, &CStationSelectDlg::OnBnClickedMfcbtnDelsel)
	ON_BN_CLICKED(IDC_MFCBTN_ADDALL, &CStationSelectDlg::OnBnClickedMfcbtnAddall)
	ON_BN_CLICKED(IDC_MFCBTN_DELALL, &CStationSelectDlg::OnBnClickedMfcbtnDelall)
	ON_BN_CLICKED(IDOK, &CStationSelectDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// CStationSelectDlg 消息处理程序


BOOL CStationSelectDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  在此添加额外的初始化
	//从Config.ini文件中读取上次保存的已选择台站（需要查询的台站）列表
	TCHAR buf[2048] = { '\0' };
	GetPrivateProfileString(_T("HistoryData"), _T("SelectedStation"), _T(""), buf, 2048, _T(".\\Config.ini"));	//注意nSize = 2047 + 1，要为字符串结束符\0留位置
	//将列表导入到ListBox控件中
	//拆分算法：查找逗号间隔符‘,’的位置，将两个逗号之间的字符载入ListBox中
	int iStart = 0, iEnd = 0;
	CString strReceive(buf);	//字符数组转换为CString类型以便处理
	CString strTemp;
	while (iEnd != -1)	//若前一次找到逗号符，则继续循环查找
	{
		iEnd = strReceive.Find(_T(','), iStart);	//从前一次找到的逗号的后一位开始查找（避免重复）
		if (iEnd != -1)	//如果找到逗号
		{
			strTemp = strReceive.Mid(iStart, iEnd - iStart);	//得到两个逗号之间的字符串
			m_ListStnRight.AddString(strTemp);					//加入右边ListBox中
			iStart = iEnd + 1;									//下次搜索的起始位置
		}
		else	//如果找不到逗号（即已经找完了所有逗号）
		{
			strTemp = strReceive.Right(strReceive.GetLength() - iStart);	//得到最后一个逗号之后的字符串
			//只有字串不为空才应将其存入ListBox中
			if (strTemp != _T(""))
				m_ListStnRight.AddString(strTemp);								//加入右边ListBox中
		}
	}
	//打开数据库，并利用SQL语句SELECT DISTINCT将数据库中的所有台站名筛选出来
	//【注意】这里不能直接使用CAwsdata类进行操作，因为该类中绑定变量方式需要满足SQL语句选取的表格元素与CRecordset中绑定元素类型与数量完全相同，因此只能使用SELECT *，而不能使用SELECT DISTINCT
	//获取当前程序路径
	CString strConnect, strPath;
	TCHAR curPath[128];
	DWORD ret = ::GetCurrentDirectory(128, curPath);
	strPath.Format(_T("DBQ=%s\\AwsData.mdb;"), curPath);
	//直接从程序目录中查找数据库
	strConnect = _T("DSN=AwsData;UID=admin;PWD=admin;") + strPath;
	CDatabase db;
//	db.Open(_T("Awsdata"), false, false, _T("ODBC;DBQ=C:\\AutoWeatherStation\\AwsData.mdb;"), false);
	db.Open(_T("Awsdata"), false, false, strConnect, false);
	CRecordset rs(&db);
	BOOL bret = rs.Open(AFX_DB_USE_DEFAULT_TYPE, _T("SELECT DISTINCT AwsType FROM Awsdata"));
	if (!bret)	//若打开数据库失败，则直接返回
		AfxMessageBox(_T("读取数据库失败！"));
	else	//打开成功
	{
		while (!rs.IsEOF())
		{
			CDBVariant var;
			rs.GetFieldValue(_T("AwsType"), var, DEFAULT_FIELD_TYPE);
			//将数据库返回的台站名先与右侧已选台站列表比较，如果右边已有，则为防止重复，将不再将此台站存入左边
			if (m_ListStnRight.FindStringExact(-1, *var.m_pstring) == LB_ERR)	//找出完全符合要求的字符串，而不是用FindString找出符合前缀的字符串，若没找到，则
			{
				m_ListStnLeft.AddString(*var.m_pstring);	//将台站名存入左边
			}
			rs.MoveNext();
		}
		rs.Close();
		db.Close();
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常:  OCX 属性页应返回 FALSE
}


void CStationSelectDlg::OnBnClickedMfcbtnAddsel()
{
	// TODO:  在此添加控件通知处理程序代码
	CString str;
	int ret;
	ret = m_ListStnLeft.GetCurSel();
	if (ret == LB_ERR)	//未选中任何项
	{
		AfxMessageBox(_T("请在左边列表中选择一个台站！"));
	}
	else
	{
		//获取台站名
		m_ListStnLeft.GetText(ret, str);
		//在左边删除
		m_ListStnLeft.DeleteString(ret);
		//在右边添加
		m_ListStnRight.AddString(str);
	}
	
}


void CStationSelectDlg::OnBnClickedMfcbtnDelsel()
{
	// TODO:  在此添加控件通知处理程序代码
	CString str;
	int ret;
	ret = m_ListStnRight.GetCurSel();
	if (ret == LB_ERR)	//未选中任何项
	{
		AfxMessageBox(_T("请在右边列表中选择一个台站！"));
	}
	else
	{
		//获取台站名
		m_ListStnRight.GetText(ret, str);
		//在右边删除
		m_ListStnRight.DeleteString(ret);
		//在左边添加
		m_ListStnLeft.AddString(str);
	}
}


void CStationSelectDlg::OnBnClickedMfcbtnAddall()
{
	// TODO:  在此添加控件通知处理程序代码
	CString str;
/*【注】清空ListBox应使用ClearContent()方法！
	for (int i = 0; i < m_ListStnLeft.GetCount(); i++)
	{
		m_ListStnLeft.GetText(0, str);	//注意0不是i
		m_ListStnRight.AddString(str);
		m_ListStnLeft.DeleteString(0);	//注意0不是i
	}
*/	
	for (int i = 0; i < m_ListStnLeft.GetCount(); i++)
	{
		m_ListStnLeft.GetText(i, str);
		m_ListStnRight.AddString(str);
	}
	m_ListStnLeft.ResetContent();
}


void CStationSelectDlg::OnBnClickedMfcbtnDelall()
{
	// TODO:  在此添加控件通知处理程序代码
	CString str;
	for (int i = 0; i < m_ListStnRight.GetCount(); i++)
	{
		m_ListStnRight.GetText(i, str);
		m_ListStnLeft.AddString(str);
	}
	m_ListStnRight.ResetContent();
}


void CStationSelectDlg::OnBnClickedOk()
{
	// TODO:  在此添加控件通知处理程序代码
	//将ListBox中的选中台站信息存入buf中，然后存入Config.ini文件中
	CString str, strTemp;
	for (int i = 0; i < m_ListStnRight.GetCount() - 1; i++)	//除最后一项外，获得的台站名称后一律加逗号
	{
		m_ListStnRight.GetText(i, strTemp);
		str = str + strTemp + _T(",");
	}
	if (m_ListStnRight.GetCount() > 0)	//若列表为空，则应保证str也为空
	{
		m_ListStnRight.GetText(m_ListStnRight.GetCount() - 1, strTemp);
		str = str + strTemp;	//最后一项后面不加逗号
	}
//	TCHAR buf[2048] = { '\0' };
	LPTSTR pBuffer = str.GetBuffer();
//	int* pBuffer = new int[2048];
	WritePrivateProfileString(_T("HistoryData"), _T("SelectedStation"), pBuffer, _T(".\\Config.ini"));
//	str.ReleaseBuffer();	//因为并未对pBuffer内容做任何修改，因此无需ReleaseBuffer

	CDialogEx::OnOK();
}

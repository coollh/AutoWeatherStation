
// AutoWeatherStation.cpp : 定义应用程序的类行为。
//

#include "stdafx.h"
#include "AutoWeatherStation.h"
#include "AutoWeatherStationDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAutoWeatherStationApp

BEGIN_MESSAGE_MAP(CAutoWeatherStationApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CAutoWeatherStationApp 构造

CAutoWeatherStationApp::CAutoWeatherStationApp()
{
	// 支持重新启动管理器
	m_dwRestartManagerSupportFlags = AFX_RESTART_MANAGER_SUPPORT_RESTART;

	// TODO:  在此处添加构造代码，
	// 将所有重要的初始化放置在 InitInstance 中
}


// 唯一的一个 CAutoWeatherStationApp 对象

CAutoWeatherStationApp theApp;


// CAutoWeatherStationApp 初始化

BOOL CAutoWeatherStationApp::InitInstance()
{
////确保应用程序同一时刻仅有一个实例在运行
	// 用应用程序名创建信号量
	HANDLE hSem = CreateSemaphore(NULL, 1, 1, m_pszExeName);

	// 信号量已存在？
	// 信号量存在，则程序已有一个实例运行
	if (GetLastError() == ERROR_ALREADY_EXISTS)
	{
		//提示用户
		AfxMessageBox(_T("程序已经运行，请勿重复打开！"));
		// 关闭信号量句柄
		CloseHandle(hSem);

		// 寻找先前实例的主窗口
		HWND hWndPrevious = ::GetWindow(::GetDesktopWindow(), GW_CHILD);
		while (::IsWindow(hWndPrevious))
		{
			// 检查窗口是否有预设的标记?
			// 有，则是我们寻找的主窗
			if (::GetProp(hWndPrevious, m_pszExeName))
			{
				// 主窗口已最小化，则恢复其大小
				if (::IsIconic(hWndPrevious))
					::ShowWindow(hWndPrevious, SW_RESTORE);
				// 将主窗激活
				::SetForegroundWindow(hWndPrevious);
				// 将主窗的对话框激活
				::SetForegroundWindow(::GetLastActivePopup(hWndPrevious));
				// 退出本实例
				return FALSE;
			}

			// 继续寻找下一个窗口
			hWndPrevious = ::GetWindow(hWndPrevious, GW_HWNDNEXT);
		}

		// 前一实例已存在，但找不到其主窗
		// 可能出错了
		// 退出本实例
		return FALSE;
	}

////MFC默认初始化
	// 如果一个运行在 Windows XP 上的应用程序清单指定要
	// 使用 ComCtl32.dll 版本 6 或更高版本来启用可视化方式，
	//则需要 InitCommonControlsEx()。  否则，将无法创建窗口。
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// 将它设置为包括所有要在应用程序中使用的
	// 公共控件类。
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();

	if (!AfxSocketInit())
	{
		AfxMessageBox(IDP_SOCKETS_INIT_FAILED);
		return FALSE;
	}


	AfxEnableControlContainer();

	// 创建 shell 管理器，以防对话框包含
	// 任何 shell 树视图控件或 shell 列表视图控件。
	CShellManager *pShellManager = new CShellManager;

	// 激活“Windows Native”视觉管理器，以便在 MFC 控件中启用主题
	CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerWindows));

	// 标准初始化
	// 如果未使用这些功能并希望减小
	// 最终可执行文件的大小，则应移除下列
	// 不需要的特定初始化例程
	// 更改用于存储设置的注册表项
	// TODO:  应适当修改该字符串，
	// 例如修改为公司或组织名
	SetRegistryKey(_T("应用程序向导生成的本地应用程序"));

////初始化RichEdit2
	AfxInitRichEdit2();

/*	
	//【测试用】建立与主对话框同级的对话框，用于测试两个同级对话框最小化相互影响的问题
	CVirtualMeterDlg* ddlg = new CVirtualMeterDlg;
	if (ddlg->Create(IDD_VIRTUALMETER_DIALOG, ddlg->GetDesktopWindow()) == NULL)	//使用GetDesktopWindow()将父对话框直接设置为桌面，使得Alt+Tab键可以看到
	{
		AfxMessageBox(_T("Meter对话框创建失败！"));
		return TRUE;
	}
	ddlg->ShowWindow(SW_SHOW);
*/

	CAutoWeatherStationDlg dlg;
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO:  在此放置处理何时用
		//  “确定”来关闭对话框的代码
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO:  在此放置处理何时用
		//  “取消”来关闭对话框的代码
	}
	else if (nResponse == -1)
	{
		TRACE(traceAppMsg, 0, "警告: 对话框创建失败，应用程序将意外终止。\n");
		TRACE(traceAppMsg, 0, "警告: 如果您在对话框上使用 MFC 控件，则无法 #define _AFX_NO_MFC_CONTROLS_IN_DIALOGS。\n");
	}

	// 删除上面创建的 shell 管理器。
	if (pShellManager != NULL)
	{
		delete pShellManager;
	}

	// 由于对话框已关闭，所以将返回 FALSE 以便退出应用程序，
	//  而不是启动应用程序的消息泵。
	return FALSE;
}


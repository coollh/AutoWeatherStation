#pragma once

class CDoublePoint {
    public:
       CDoublePoint(){}
       CDoublePoint(double ix, double iy) {x = ix; y = iy; }
       double x;
       double y;
       operator CPoint() { CPoint pt; pt.x = (int)x; pt.y = (int)y; return pt; }
};

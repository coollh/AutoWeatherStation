#pragma once

#include "DoublePoint.h"

class CPolygon
   { 
    public:
       CPolygon() { heading = 0.0; }
       BOOL Read(const CString & filename);
       BOOL Load(UINT resid);
       BOOL Load(LPCTSTR resource);
       CRect GetInputBB();
       CRgn * GetRgn();
       CRect Transform(double angle, double scale, BOOL force = FALSE);
       void Draw(CDC & dc, CDoublePoint pt);
       bool IsDefined() { return points.GetSize() > 0; }
    protected:
       CArray<CDoublePoint, CDoublePoint> points;
       double heading; // current angle
       double scaling;
       CArray<CPoint, CPoint> transformed;
   };

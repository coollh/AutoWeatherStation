#if !defined(AFX_COMPASS_H__7E6C2866_B43C_11D5_A044_006067718D04__INCLUDED_)
#define AFX_COMPASS_H__7E6C2866_B43C_11D5_A044_006067718D04__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Compass.h : header file
//
#include "Polygon.h"

/////////////////////////////////////////////////////////////////////////////
// CCompass window

class CCompass : public CWnd
{
// Construction
public:
	CCompass();

// Attributes
public:
        void SetAngle(double angle);
	void SetShow(BOOL mode);
	BOOL GetShow() { return ArrowVisible; }
// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCompass)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CCompass();

	// Generated message map functions
protected:
        static BOOL hasclass;
	static BOOL RegisterMe();
	void NotifyParent(CPoint point);

	CFont NorthFont;
	CFont MajorFont;
	CFont MinorFont;
	CRect CreateClipRegion(CRgn & rgn);
	CPolygon arrow;
	double angle;
	BOOL ArrowVisible; // TRUE if arrow is visible
	void MapDC(CDC & dc);
	int innerRadius;

	class displayinfo {
	    public:
	       displayinfo(double a, LPCTSTR s, double sz, BOOL sp)
		  { angle = a; display = s; size = sz; special = sp; }
	       virtual ~displayinfo() {}
	       double GetSize() { return size; }
	       double GetAngle() { return angle; }
	       CString GetText() { return CString(display); }
	       CFont * CreateFont(double basesize, LPCTSTR facename) {
		  CFont * f = new CFont;
		  f->CreateFont((int)(basesize * size / 100.0), // font height
				       0,		     // font width
				       0,		     // escapement
				       0,		     // orientation
				       special ? FW_BOLD : FW_NORMAL, // weight
				       special,              // italic
				       FALSE,                // underline
				       FALSE,                // strikeout
				       ANSI_CHARSET,         // character set
				       OUT_TT_PRECIS,        // output precision
				       CLIP_TT_ALWAYS,       // clip precision
				       PROOF_QUALITY,        // quality
				       VARIABLE_PITCH | FF_DONTCARE, // pitch and family
				       facename);            // facename
		    return f;
		   }
	    protected:
	       double angle;
	       CString display;
	       double size;
	       BOOL special;
	       
	}; // displayinfo

	CArray<displayinfo *, displayinfo *> display;

#define COMPASS_CLASS_NAME _T("Compass")
	//{{AFX_MSG(CCompass)
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnPaint();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnEnable(BOOL bEnable);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/****************************************************************************
*                                  CPM_SET_ANGLE
* Inputs:
*       WPARAM: x-coordinate in mapped coordinates
*	LPARAM: y-coordinate in mapped coordinates
* Result: LRESULT
*       Logically void, 0, always
* Effect: 
*       Notifies the parent of the compass that the angle should be changed
****************************************************************************/

#define CPM_SET_ANGLE_MSG _T("CPM_SET_ANGLE-{66F100A0-BBB9-11d5-A044-006067718D04}")


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COMPASS_H__7E6C2866_B43C_11D5_A044_006067718D04__INCLUDED_)

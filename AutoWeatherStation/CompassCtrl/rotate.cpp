#include "../stdafx.h"
#include "DoublePoint.h"
#include "rotate.h"
#include "dtor.h"


/****************************************************************************
*                                   rotate
* Inputs:
*       CDoublePoint pt: Input point
*	double angle: Angle expressed in degrees
*	double scale: Scale factor
* Result: CDoublePoint
*       Point, transformed
* Effect: 
*       Rotates the point through the specified angle
* Notes:
*             _          _
*            |  m11  m12  |
*       [x y]|  m21  m22  | = [m11*x + m21*y, m12*x + m22*y]
*            |_ 0    0   _|
*        
*       Rotation Clockwise by ? scaling isotropic by s
*             _                    _
*            | s * cos ?   sin ?  |
*       [x y]|  -sin ?   s * cos ?|=[sx*(cos ?*x-(sin ?*y,
*            |_    0          0    _|  (sin ?*x+sy*(cos ?*y]
****************************************************************************/

CDoublePoint rotate(CDoublePoint pt, double angle, double scale)
    {
     CDoublePoint point;
     double theta = DegreesToRadians(angle);
     point.x = (scale * (cos(theta) * pt.x - sin(theta) * pt.y) + 0.5);
     point.y = (scale * (sin(theta) * pt.x +  cos(theta) * pt.y) +0.5);
     return point;
    } // rotate

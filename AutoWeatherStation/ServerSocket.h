#pragma once
#include "afxcoll.h"

// CServerSocket 命令目标

class CServerSocket : public CSocket
{
public:
	CServerSocket();
	virtual ~CServerSocket();
////成员变量
	CPtrList m_listSockets;		// 保存服务器与所有客户端连接成功后的Socket
////成员函数
	virtual void OnAccept(int nErrorCode);
};

//向采集器发送获取子台站信息的命令的线程
UINT DauMeasureThread(LPVOID pParam);


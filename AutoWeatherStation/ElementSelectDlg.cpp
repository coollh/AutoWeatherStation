// ElementSelectDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "AutoWeatherStation.h"
#include "ElementSelectDlg.h"
#include "afxdialogex.h"
#include "AutoWeatherStationDlg.h"

// CElementSelectDlg 对话框

IMPLEMENT_DYNAMIC(CElementSelectDlg, CDialogEx)

CElementSelectDlg::CElementSelectDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CElementSelectDlg::IDD, pParent)
/*	, m_AvgBaro(FALSE)
	, m_CurBaro(FALSE)
	, m_MaxBaro(FALSE)
	, m_MinBaro(FALSE)
	, m_AvgHumi(FALSE)
	, m_CurHumi(FALSE)
	, m_MaxHumi(FALSE)
	, m_MinHumi(FALSE)
	, m_AvgTemp(FALSE)
	, m_CurTemp(FALSE)
	, m_MaxTemp(FALSE)
	, m_MinTemp(FALSE)
	, m_CurWindDir(FALSE)
	, m_VectWindDir(FALSE)
	, m_AvgWindSpe(FALSE)
	, m_CurWindSpe(FALSE)
	, m_MaxWindSpe(FALSE)
	, m_MinWindSpe(FALSE)
	, m_VectWindSpe(FALSE)	*/
{

}

CElementSelectDlg::~CElementSelectDlg()
{
}

void CElementSelectDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
/*	DDX_Check(pDX, IDC_CHK_BARO_AVG, m_AvgBaro);
	DDX_Check(pDX, IDC_CHK_BARO_CUR, m_CurBaro);
	DDX_Check(pDX, IDC_CHK_BARO_MAX, m_MaxBaro);
	DDX_Check(pDX, IDC_CHK_BARO_MIN, m_MinBaro);
	DDX_Check(pDX, IDC_CHK_HUMI_AVG, m_AvgHumi);
	DDX_Check(pDX, IDC_CHK_HUMI_CUR, m_CurHumi);
	DDX_Check(pDX, IDC_CHK_HUMI_MAX, m_MaxHumi);
	DDX_Check(pDX, IDC_CHK_HUMI_MIN, m_MinHumi);
	DDX_Check(pDX, IDC_CHK_TEMP_AVG, m_AvgTemp);
	DDX_Check(pDX, IDC_CHK_TEMP_CUR, m_CurTemp);
	DDX_Check(pDX, IDC_CHK_TEMP_MAX, m_MaxTemp);
	DDX_Check(pDX, IDC_CHK_TEMP_MIN, m_MinTemp);
	DDX_Check(pDX, IDC_CHK_WIND_DIR_CUR, m_CurWindDir);
	DDX_Check(pDX, IDC_CHK_WIND_DIR_VECT, m_VectWindDir);
	DDX_Check(pDX, IDC_CHK_WIND_SPE_AVG, m_AvgWindSpe);
	DDX_Check(pDX, IDC_CHK_WIND_SPE_CUR, m_CurWindSpe);
	DDX_Check(pDX, IDC_CHK_WIND_SPE_MAX, m_MaxWindSpe);
	DDX_Check(pDX, IDC_CHK_WIND_SPE_MIN, m_MinWindSpe);
	DDX_Check(pDX, IDC_CHK_WIND_SPE_VECT, m_VectWindSpe);	*/
}


BEGIN_MESSAGE_MAP(CElementSelectDlg, CDialogEx)
	ON_BN_CLICKED(IDOK, &CElementSelectDlg::OnBnClickedOk)
	ON_CONTROL_RANGE(BN_CLICKED, IDC_CHK_TEMP, IDC_CHK_WIND, &CElementSelectDlg::OnButtonCheck)	//使下方的4个CheckBox控件响应同一个函数
END_MESSAGE_MAP()


// CElementSelectDlg 消息处理程序


BOOL CElementSelectDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	
	// TODO:  在此添加额外的初始化
////从Config.ini文件中读取上次CheckBox选中情况的数据存入buf中，同时在对话框中显示出来
	TCHAR buf[20] = { '\0' };
	GetPrivateProfileString(_T("HistoryData"), _T("ElementSelect"), _T(""), buf, 20, _T(".\\Config.ini"));	//注意nSize = 19 + 1，要为字符串结束符\0留位置
	//buf对应信息显示到CheckBox控件中
	for (int i = IDC_CHK_TEMP_CUR; i <= IDC_CHK_WIND_DIR_VECT; i++)	//利用CheckBox连续的ID号批量设置
	{
		if (buf[i - IDC_CHK_TEMP_CUR] == '1')
			((CButton*)GetDlgItem(i))->SetCheck(1);
		else
			((CButton*)GetDlgItem(i))->SetCheck(0);
	}
	//buf对应信息显示到底部批量选择CheckBox控件中
	int flag;
	//温度
	flag = 1;
	for (int i = 0; i < 4; i++)		//buf全为1，flag才为1
		if(buf[i] != '1')
			flag = 0;
	if (flag == 1)
		((CButton*)GetDlgItem(IDC_CHK_TEMP))->SetCheck(1);
	//湿度
	flag = 1;
	for (int i = 4; i < 8; i++)		//buf全为1，flag才为1
		if (buf[i] != '1')
			flag = 0;
	if (flag == 1)
		((CButton*)GetDlgItem(IDC_CHK_HUMI))->SetCheck(1);
	//压力
	flag = 1;
	for (int i = 8; i < 12; i++)	//buf全为1，flag才为1
		if (buf[i] != '1')
			flag = 0;
	if (flag == 1)
		((CButton*)GetDlgItem(IDC_CHK_BARO))->SetCheck(1);
	//风速风向
	flag = 1;
	for (int i = 12; i < 19; i++)	//buf全为1，flag才为1
		if (buf[i] != '1')
			flag = 0;
	if (flag == 1)
		((CButton*)GetDlgItem(IDC_CHK_WIND))->SetCheck(1);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常:  OCX 属性页应返回 FALSE
}


void CElementSelectDlg::OnBnClickedOk()
{
	// TODO:  在此添加控件通知处理程序代码
////将CheckBox选中情况的数据存入buf中，然后存入Config.ini文件中
	TCHAR buf[20] = { '\0' };
	for (int i = IDC_CHK_TEMP_CUR; i <= IDC_CHK_WIND_DIR_VECT; i++)
	{
		if (((CButton*)GetDlgItem(i))->GetCheck())
			buf[i - IDC_CHK_TEMP_CUR] = '1';
		else
			buf[i - IDC_CHK_TEMP_CUR] = '0';
	}
	WritePrivateProfileString(_T("HistoryData"), _T("ElementSelect"), buf, _T(".\\Config.ini"));
////重建数据显示列表
	CAutoWeatherStationDlg* pDlg = (CAutoWeatherStationDlg*)theApp.GetMainWnd();
	//清空ListCtrl
	pDlg->m_HistoryDlg.m_HistoryTable.DeleteAllItems();	
	//删除所有列 
	int nColumnCount = pDlg->m_HistoryDlg.m_HistoryTable.GetHeaderCtrl()->GetItemCount();
	for (int i = 0; i < nColumnCount; i++)
	{
		pDlg->m_HistoryDlg.m_HistoryTable.DeleteColumn(0);
	}
	//重新插入列
	TCHAR* m_pElementSelect = buf;	//注意，此处定义为m_pElementSelect仅仅是为了方便，与CHistoryDlg类中的同名成员并不是同一变量
	int i = 2;	//用于选择要素
	pDlg->m_HistoryDlg.m_HistoryTable.InsertColumn(0, _T("台站名"), LVCFMT_LEFT, 80);
	pDlg->m_HistoryDlg.m_HistoryTable.InsertColumn(1, _T("时间"), LVCFMT_LEFT, 150);
	if (*m_pElementSelect++ == _T('1'))
		pDlg->m_HistoryDlg.m_HistoryTable.InsertColumn(i++, _T("当前温度(℃)"), LVCFMT_LEFT, 80);
	if (*m_pElementSelect++ == _T('1'))
		pDlg->m_HistoryDlg.m_HistoryTable.InsertColumn(i++, _T("平均温度(℃)"), LVCFMT_LEFT, 80);
	if (*m_pElementSelect++ == _T('1'))
		pDlg->m_HistoryDlg.m_HistoryTable.InsertColumn(i++, _T("最大温度(℃)"), LVCFMT_LEFT, 80);
	if (*m_pElementSelect++ == _T('1'))
		pDlg->m_HistoryDlg.m_HistoryTable.InsertColumn(i++, _T("最小温度(℃)"), LVCFMT_LEFT, 80);
	if (*m_pElementSelect++ == _T('1'))
		pDlg->m_HistoryDlg.m_HistoryTable.InsertColumn(i++, _T("当前湿度(%)"), LVCFMT_LEFT, 80);
	if (*m_pElementSelect++ == _T('1'))
		pDlg->m_HistoryDlg.m_HistoryTable.InsertColumn(i++, _T("平均湿度(%)"), LVCFMT_LEFT, 80);
	if (*m_pElementSelect++ == _T('1'))
		pDlg->m_HistoryDlg.m_HistoryTable.InsertColumn(i++, _T("最大湿度(%)"), LVCFMT_LEFT, 80);
	if (*m_pElementSelect++ == _T('1'))
		pDlg->m_HistoryDlg.m_HistoryTable.InsertColumn(i++, _T("最小湿度(%)"), LVCFMT_LEFT, 80);
	if (*m_pElementSelect++ == _T('1'))
		pDlg->m_HistoryDlg.m_HistoryTable.InsertColumn(i++, _T("当前气压(kPa)"), LVCFMT_LEFT, 90);
	if (*m_pElementSelect++ == _T('1'))
		pDlg->m_HistoryDlg.m_HistoryTable.InsertColumn(i++, _T("平均气压(kPa)"), LVCFMT_LEFT, 90);
	if (*m_pElementSelect++ == _T('1'))
		pDlg->m_HistoryDlg.m_HistoryTable.InsertColumn(i++, _T("最大气压(kPa)"), LVCFMT_LEFT, 90);
	if (*m_pElementSelect++ == _T('1'))
		pDlg->m_HistoryDlg.m_HistoryTable.InsertColumn(i++, _T("最小气压(kPa)"), LVCFMT_LEFT, 90);
	if (*m_pElementSelect++ == _T('1'))
		pDlg->m_HistoryDlg.m_HistoryTable.InsertColumn(i++, _T("当前风速(m/s)"), LVCFMT_LEFT, 90);
	if (*m_pElementSelect++ == _T('1'))
		pDlg->m_HistoryDlg.m_HistoryTable.InsertColumn(i++, _T("平均风速(m/s)"), LVCFMT_LEFT, 90);
	if (*m_pElementSelect++ == _T('1'))
		pDlg->m_HistoryDlg.m_HistoryTable.InsertColumn(i++, _T("最大风速(m/s)"), LVCFMT_LEFT, 90);
	if (*m_pElementSelect++ == _T('1'))
		pDlg->m_HistoryDlg.m_HistoryTable.InsertColumn(i++, _T("最小风速(m/s)"), LVCFMT_LEFT, 90);
	if (*m_pElementSelect++ == _T('1'))
		pDlg->m_HistoryDlg.m_HistoryTable.InsertColumn(i++, _T("矢量风速(m/s)"), LVCFMT_LEFT, 90);
	if (*m_pElementSelect++ == _T('1'))
		pDlg->m_HistoryDlg.m_HistoryTable.InsertColumn(i++, _T("当前风向(°)"), LVCFMT_LEFT, 80);
	if (*m_pElementSelect++ == _T('1'))
		pDlg->m_HistoryDlg.m_HistoryTable.InsertColumn(i++, _T("矢量风向(°)"), LVCFMT_LEFT, 80);
	
	CDialogEx::OnOK();
}

void CElementSelectDlg::OnButtonCheck(UINT uID)
{
	switch (uID)
	{
	case IDC_CHK_TEMP:	//温
		if (((CButton*)GetDlgItem(IDC_CHK_TEMP))->GetCheck() == 1)
			for (int i = IDC_CHK_TEMP_CUR; i <= IDC_CHK_TEMP_MIN; i++)
			{
				((CButton*)GetDlgItem(i))->SetCheck(1);
			}
		else
			for (int i = IDC_CHK_TEMP_CUR; i <= IDC_CHK_TEMP_MIN; i++)
			{
				((CButton*)GetDlgItem(i))->SetCheck(0);
			}
		break;

	case IDC_CHK_HUMI:	//湿
		if (((CButton*)GetDlgItem(IDC_CHK_HUMI))->GetCheck() == 1)
			for (int i = IDC_CHK_HUMI_CUR; i <= IDC_CHK_HUMI_MIN; i++)
			{
				((CButton*)GetDlgItem(i))->SetCheck(1);
			}
		else
			for (int i = IDC_CHK_HUMI_CUR; i <= IDC_CHK_HUMI_MIN; i++)
			{
				((CButton*)GetDlgItem(i))->SetCheck(0);
			}
		break;

	case IDC_CHK_BARO:	//压
		if (((CButton*)GetDlgItem(IDC_CHK_BARO))->GetCheck() == 1)
			for (int i = IDC_CHK_BARO_CUR; i <= IDC_CHK_BARO_MIN; i++)
			{
				((CButton*)GetDlgItem(i))->SetCheck(1);
			}
		else
			for (int i = IDC_CHK_BARO_CUR; i <= IDC_CHK_BARO_MIN; i++)
			{
				((CButton*)GetDlgItem(i))->SetCheck(0);
			}
		break;

	case IDC_CHK_WIND:	//风
		if (((CButton*)GetDlgItem(IDC_CHK_WIND))->GetCheck() == 1)
			for (int i = IDC_CHK_WIND_SPE_CUR; i <= IDC_CHK_WIND_DIR_VECT; i++)
			{
				((CButton*)GetDlgItem(i))->SetCheck(1);
			}
		else
			for (int i = IDC_CHK_WIND_SPE_CUR; i <= IDC_CHK_WIND_DIR_VECT; i++)
			{
				((CButton*)GetDlgItem(i))->SetCheck(0);
			}
		break;

	default:
		AfxMessageBox(_T("CheckBox处理出错！"));
		break;
	}
}


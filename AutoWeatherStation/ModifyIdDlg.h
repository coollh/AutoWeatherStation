#pragma once
#include "afxcmn.h"
#include "afxwin.h"


// CModifyIdDlg 对话框

class CModifyIdDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CModifyIdDlg)

public:
	CModifyIdDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CModifyIdDlg();

// 对话框数据
	enum { IDD = IDD_DIALOG_MODIFY_ID };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedBtnStartScan();
	virtual BOOL OnInitDialog();
	CListCtrl m_ListStnRepo;
//	afx_msg void OnTimer(UINT_PTR nIDEvent);
protected:
	afx_msg LRESULT OnIdInfoReceived(WPARAM wParam, LPARAM lParam);
public:
//	afx_msg void OnLvnItemchangedListStationReport(NMHDR *pNMHDR, LRESULT *pResult);
	CComboBox m_ComboStnList;
	CComboBox m_ComboCurrentId;
	afx_msg void OnLvnItemActivateListStationReport(NMHDR *pNMHDR, LRESULT *pResult);
	CEdit m_EditChangeId;
	afx_msg void OnBnClickedBtnChangeId();
	afx_msg void OnBnClickedBtnRegisterNew();
	afx_msg void OnBnClickedBtnAssignNew();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	int m_SelRow;	//保存双击ListCtrl中某一行后，当前行信息
};

// 已存在站点扫描线程
UINT StationScanThread(LPVOID pParam);	//【注意】创建的线程函数为全局函数！
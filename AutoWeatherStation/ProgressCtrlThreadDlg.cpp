// ProcessCtrlThreadDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "afxdialogex.h"
#include "ProgressCtrlThread.h"
#include "ProgressCtrlThreadDlg.h"

//自定义消息
#define WM_PROGRESS_CHANGED		WM_USER + 100		//用于改变ProgressCtrl进度的自定义消息

// CProcessCtrlThreadDlg 对话框

IMPLEMENT_DYNAMIC(CProgressCtrlThreadDlg, CDialogEx)

CProgressCtrlThreadDlg::CProgressCtrlThreadDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CProgressCtrlThreadDlg::IDD, pParent)
{

}

CProgressCtrlThreadDlg::~CProgressCtrlThreadDlg()
{
}

void CProgressCtrlThreadDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PROGRESS_CTRL, m_Progress);
	DDX_Control(pDX, IDC_STATIC_TEXT, m_StaticText);
}


// CProgressCtrlThreadDlg 消息处理程序
BEGIN_MESSAGE_MAP(CProgressCtrlThreadDlg, CDialogEx)
	ON_BN_CLICKED(IDCANCEL, &CProgressCtrlThreadDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_PROGRESS_BTN_CANCEL, &CProgressCtrlThreadDlg::OnBnClickedProgressBtnCancel)
	ON_MESSAGE(WM_PROGRESS_CHANGED, &CProgressCtrlThreadDlg::OnProgressChanged)
//	ON_THREAD_MESSAGE(WM_PROGRESS_CHANGED, &CProgressCtrlThreadDlg::OnProgressChanged)	//注意，ON_THREAD_MESSAGE应当写在从CWinThread继承的类下面，而不是CDialog继承的类下面
END_MESSAGE_MAP()


BOOL CProgressCtrlThreadDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  在此添加额外的初始化
	ModifyStyleEx(WS_EX_APPWINDOW, WS_EX_TOOLWINDOW);	//将对话框样式设置从AppWindow修改为ToolWindow，从而达到去除任务栏图标的目的

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常:  OCX 属性页应返回 FALSE
}


void CProgressCtrlThreadDlg::OnBnClickedCancel()
{
	// TODO:  在此添加控件通知处理程序代码
//	CDialogEx::OnCancel();
}


void CProgressCtrlThreadDlg::OnBnClickedProgressBtnCancel()
{
	// TODO:  在此添加控件通知处理程序代码
	AfxMessageBox(_T("进度条将会被关闭"));
	CDialogEx::OnCancel();
}


//【此对话框消息可在HistoryDlg.cpp中发送，暂时未用】
afx_msg LRESULT CProgressCtrlThreadDlg::OnProgressChanged(WPARAM wParam, LPARAM lParam)
{
	int flag = (int)wParam;
	int c = (int)lParam;
	switch (flag)
	{
	case 1:
		m_Progress.SetRange(0, c);	//设置ProgressCtrl的总范围
		break;
	case 2:
		m_Progress.SetPos(c);	//设置进度位置
		break;
	case 3:
		m_Progress.SetPos(c);	//设置为满进度状态
		Sleep(200);
		EndDialog(IDCANCEL);	//退出此对话框
		break;
	default:
		break;
	}
	return 0;
}

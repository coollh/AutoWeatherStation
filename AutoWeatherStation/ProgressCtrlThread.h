// ProgressCtrlThread.h : 进度条对话框UI线程头文件
//

#pragma once

#include "resource.h"	


class CProgressCtrlThread : public CWinThread
{
	DECLARE_DYNCREATE(CProgressCtrlThread)	//声明此线程会被动态创建为UI线程

public:
	CProgressCtrlThread();

	// 重写
public:
	virtual BOOL InitInstance();

	// 实现

	DECLARE_MESSAGE_MAP()
protected:
	void OnThreadMessages(WPARAM wParam, LPARAM lParam);	//注意是线程间消息传递返回void而不是afx_msg LRESULT
};


//extern CProgressCtrlThread theProgressCtrlThread;
#pragma once
#include "afxcmn.h"
#include "afxwin.h"


// CProcessCtrlThreadDlg 对话框

class CProgressCtrlThreadDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CProgressCtrlThreadDlg)

public:
	CProgressCtrlThreadDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CProgressCtrlThreadDlg();

// 对话框数据
	enum { IDD = IDD_DIALOG_PROGRESS_CTRL };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedCancel();
	CProgressCtrl m_Progress;
	CStatic m_StaticText;
	afx_msg void OnBnClickedProgressBtnCancel();
protected:
	afx_msg LRESULT OnProgressChanged(WPARAM wParam, LPARAM lParam);
public:
	virtual BOOL OnInitDialog();
};

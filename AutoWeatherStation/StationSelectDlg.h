#pragma once
#include "afxwin.h"


// CStationSelectDlg 对话框

class CStationSelectDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CStationSelectDlg)

public:
	CStationSelectDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CStationSelectDlg();

// 对话框数据
	enum { IDD = IDD_DIALOG_STATIONSELECT };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	CListBox m_ListStnLeft;
	CListBox m_ListStnRight;
	afx_msg void OnBnClickedMfcbtnAddsel();
	afx_msg void OnBnClickedMfcbtnDelsel();
	afx_msg void OnBnClickedMfcbtnAddall();
	afx_msg void OnBnClickedMfcbtnDelall();
	afx_msg void OnBnClickedOk();
};

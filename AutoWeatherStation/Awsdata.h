// Awsdata.h : CAwsdata 的声明

#pragma once

// 代码生成在 2014年9月22日, 10:44

class CAwsdata : public CRecordset
{
public:
	CAwsdata(CDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CAwsdata)

// 字段/参数数据

// 以下字符串类型(如果存在)反映数据库字段(ANSI 数据类型的 CStringA 和 Unicode
// 数据类型的 CStringW)的实际数据类型。
//  这是为防止 ODBC 驱动程序执行可能
// 不必要的转换。如果希望，可以将这些成员更改为
// CString 类型，ODBC 驱动程序将执行所有必要的转换。
// (注意: 必须使用 3.5 版或更高版本的 ODBC 驱动程序
// 以同时支持 Unicode 和这些转换)。

	long	m_ID;	//记录序号
	CString	m_CmdRet;	//命令返回值，一般为"OK"，或保留
	CString	m_Command;	//命令，或保留
	CString	m_AwsType;	//气象站型号
	CTime	m_DDate;	//数据接收日期
	CTime	m_TTime;	//数据接收时间
	long	m_CurTemp;	//CurrentTemperature，当前温度
	long	m_AvgTemp;	//AverageTemperature，一段时间(1-10分钟)内的平均温度
	long	m_MaxTemp;	//MaximumTemperature，一段时间(1-10分钟)内的最大温度
	long	m_MinTemp;	//MinimumTemperature，一段时间(1-10分钟)内的最小温度
	long	m_CurHumi;	//CurrentHumidity，当前湿度
	long	m_AvgHumi;	//AverageHumidity，一段时间(1-10分钟)内的平均湿度
	long	m_MaxHumi;	//MaximumHumidity，一段时间(1-10分钟)内的最大湿度
	long	m_MinHumi;	//MinimumHumidity，一段时间(1-10分钟)内的最小湿度
	long	m_CurBaro;	//CurrentBarometricPressure，当前气压
	long	m_AvgBaro;	//AverageBarometricPressure，一段时间(1-10分钟)内的平均气压
	long	m_MaxBaro;	//MaximumBarometricPressure，一段时间(1-10分钟)内的最大气压
	long	m_MinBaro;	//MinimumBarometricPressure，一段时间(1-10分钟)内的最小气压
	long	m_CurWindSpe;	//CurrentWindSpeed，当前风速
	long	m_AvgWindSpe;	//AverageWindSpeed，一段时间(1-10分钟)内的平均风速
	long	m_MaxWindSpe;	//AverageWindSpeed，一段时间(1-10分钟)内的最大风速
	long	m_MinWindSpe;	//AverageWindSpeed，一段时间(1-10分钟)内的最小风速
	long	m_VectWindSpe;	//VectorialWindSpeed，一段时间(1-10分钟)内的矢量平均风速
	long	m_CurWindDir;	//CurrentWindDirection，当前风向
	long	m_VectWindDir;	//VectorialWindDirection，一段时间(1-10分钟)内的矢量风向

// 重写
	// 向导生成的虚函数重写
	public:
	virtual CString GetDefaultConnect();	// 默认连接字符串

	virtual CString GetDefaultSQL(); 	// 记录集的默认 SQL
	virtual void DoFieldExchange(CFieldExchange* pFX);	// RFX 支持

// 实现
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

};



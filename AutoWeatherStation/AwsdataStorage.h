///////////////////////////////////////////////////////////
//用于将Socket或串口接收到的数据处理后存入数据库中
//
#pragma once
#include "Awsdata.h"

class CAwsdataStorage
{
public:
	CAwsdataStorage();
	~CAwsdataStorage();
//成员变量
	CString m_strDataMerge;	//在普通模式下，用于存放合并前的临时数据（因为此类全局唯一，所以此变量在下次接收数据的时候仍然可用）
//	int m_iStationCount;
	CAwsdata* m_pAwsdata;	//指向数据库AwsData的指针
	//标志位，指示对当前从下位机接收到的数据进行何种处理，由各个窗口类（对话框类）向下位机发送命令的同时对此标志进行修改
	//	  值						描述																触发条件
		//0：默认值，不做任何处理（跳过PreProcess()函数，将接收的数据直接存入数据库）		程序初始化、点击菜单中的“普通模式”
		//1：当前处于调试模式（即命令完全由用户输入，输出则直接显示在对话框中）				点击菜单中的“调试模式”、点击主窗口的“发送”按钮
		//2：为重命名台站ID而向下位机发送了获取台站ID的命令									点击CModifyIdDlg中的“开始”按钮
		//3：增加了新台站而向下位机发送了获取台站ID的命令									点击CModifyIdDlg中的“扫描”按钮
		//4：对台站发送时钟初始化命令的时候修改此值											点击菜单中的“初始化设备时钟”
		//5：修改新台站ID																	点击CModifyIdDlg中的“确定”按钮
		//6：修改已有台站ID																	点击CModifyIdDlg中的“更改”按钮
		//7：网络方式下获取数据采集器连接的所有子台站信息									检测到网络连接，亦即CServerSocket::OnAccept()方法被触发的时候
	int m_ProcessFlag;
	//事件标识，用于同步
	CEvent m_ProcessEvent;	//用于同步台站扫描线程
	CEvent m_ClockInitEvent;	//用于同步台站时钟初始化线程
	CEvent m_StoreSuccessEvent; //用于向自动监测线程指示数据库存入数据成功
	CEvent m_DevIpSuccessEvent;	//用于向采集器发送获取子台站信息的命令的线程提示命令操作成功
	CArray<CString, CString> m_strArrayForMeter;	//分配内存用于存放气象数据
//成员函数
private:
	BOOL AnalyzeString(CString strReceive, CArray<CString, CString>& arrElement);	// 将接收到的CString类型的数据分解成数据库中的元素
public:
	bool PreProcess(CString strText);	//对下位机发来的数据预处理，达到拦截“发送命令的返回值（字符串）”的目的
	void StoreString(CString strText);	//将接收到的strText处理后存入数据库
};


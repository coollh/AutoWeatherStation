// ClientSocket.cpp : 实现文件
//

#include "stdafx.h"
#include "AutoWeatherStation.h"
#include "AutoWeatherStationDlg.h"
#include "ClientSocket.h"
#include "AwsdataStorage.h"


// CClientSocket

CClientSocket::CClientSocket(CPtrList* pList)
	:m_pList(pList), m_strName(_T(""))
{
}

CClientSocket::~CClientSocket()
{
}


// CClientSocket 成员函数


void CClientSocket::OnClose(int nErrorCode)
{
	// TODO:  在此添加专用代码和/或调用基类

////删除在ServerSocket中m_listSockets中保存的本客户连接的指针
	//【暂留空，以后完善】

////在台站列表中删除当前连接及相应子台站
	CAutoWeatherStationDlg* pDlg = (CAutoWeatherStationDlg*)theApp.GetMainWnd();
/*
	//删除数据采集器信息
	CString strClientIpAddr;
	UINT uPort;
	GetPeerName(strClientIpAddr, uPort);	//获取客户端（远端）IP与占用端口
	strClientIpAddr = _T("数据采集器（") + strClientIpAddr + _T("）");
	int nIndex = 0;
	nIndex = pDlg->m_List.FindStringExact(-1, strClientIpAddr);
	pDlg->m_List.DeleteString(nIndex);
	//删除子台站
	for (int i = nIndex; i < nIndex + pDlg->m_pAwsdataStorage->m_iStationCount; i++)
	{
		pDlg->m_List.DeleteString(nIndex);	//注意nIndex是不变量
	}
*/
	//直接清空台站列表
//	pDlg->m_List.ResetContent();	//【bug修复】此处无需清空台站列表，否则可能导致列表被意外多次清空导致自动监测服务失效
////重置子台站计数器
//	pDlg->m_pAwsdataStorage->m_iStationCount = 0;
////若自动监测服务已开启，则立刻将其暂停
	//由于此时的台站列表中已经没有数据采集器的台站信息了，因此自动监测服务实际上已经处于空循环中，已经不会对重新建立网络连接造成影响
////关闭此客户端Socket
	CSocket::OnClose(nErrorCode);
}


void CClientSocket::OnReceive(int nErrorCode)
{
	// TODO:  在此添加专用代码和/或调用基类
	CAutoWeatherStationDlg* pDlg = (CAutoWeatherStationDlg*)theApp.GetMainWnd();
///接收客户端传回的数据并保存
	char* pBuff = NULL;
	pBuff = new char[512];
	if (!pBuff)
	{
		AfxMessageBox(_T("分配缓冲区失败，内存不足！"));
		return;
	}
	memset(pBuff, 0, sizeof(char)* 512);
	Receive(pBuff, 512);
/*	if (512 != Receive(pBuff, 512))
	{
		AfxMessageBox(_T("收到数据有误！"));
		delete pBuff;
		return;
	}
*/
////数据处理
	CString strText(pBuff);		//利用CString的构造函数，初始化strText的同时进行类型转换，将多字节的char型转换为CString（Unicode）型
	//将缓存数据直接写入log日志文件
	if (strText != _T(""))	//不为空字串才会写入文件
	{
		try
		{
			pDlg->m_LogFile.WriteString(strText+_T("\r\n"));
		}
		catch (CFileException* pe)
		{
			TRACE(_T("File could not be written, cause = %d\n"),
				pe->m_cause);
			pe->Delete();
		}
	}
	//为配合数据采集器的心跳包而进行预判断，若判断接收到的数据为字符串LOG，则判定为心跳包，直接略过而不作任何处理，否则进入数据处理阶段
	if (strText != _T("LOG"))
	{
//	if (pDlg->m_pAwsdataStorage->m_ProcessFlag == 7)	//【调试采集器子台站列表用】
		pDlg->m_pAwsdataStorage->StoreString(strText);	//调用CAwsdataStorage类中的StoreString方法，将接收到的数据处理后存入数据库中
	}
	//回收内存，防止泄露
	delete[] pBuff;	//注：因为基本数据类型无析构函数，所以此处如果不使用delete[]而使用delete也是可以的

	CSocket::OnReceive(nErrorCode);
}

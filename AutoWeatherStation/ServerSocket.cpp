// ServerSocket.cpp : 实现文件
//

#include "stdafx.h"
#include "AutoWeatherStation.h"
#include "AutoWeatherStationDlg.h"
#include "ServerSocket.h"
#include "ClientSocket.h"


//自定义消息，同主窗口相同
#define WM_LISTBOX_INFO		WM_USER + 2		//自动监测线程用于获取主对话框Listbox控件信息的自定义消息


// CServerSocket

CServerSocket::CServerSocket()
{
}

CServerSocket::~CServerSocket()
{
}


// CServerSocket 成员函数


void CServerSocket::OnAccept(int nErrorCode)
{
	//接受到一个连接请求
	CClientSocket* theClientSock(0);
	theClientSock = new CClientSocket(&m_listSockets);
	if (!theClientSock)
	{
		AfxMessageBox(_T("内存不足,客户连接服务器失败！"));
		return;
	}
	Accept(*theClientSock);
	//将当前连接的客户端加入list中便于管理
	m_listSockets.AddTail(theClientSock);
	//将当前连接的客户端ID显示在ListBox中
	CString strClientIpAddr;
	UINT uPort;
//	theClientSock->GetSockName(strClientIpAddr, uPort);	//获取服务器（本地）IP与占用端口
	theClientSock->GetPeerName(strClientIpAddr, uPort);	//获取客户端（远端）IP与占用端口
	CAutoWeatherStationDlg* pDlg = (CAutoWeatherStationDlg*)theApp.GetMainWnd();
	//////////////
	//为确保准确，显示之前先清空ListBox！！！
	pDlg->m_List.ResetContent();	//【注意】可能只是暂时的方法
	//将采集器IP显示出来
	pDlg->m_List.AddString(_T("数据采集器（") + strClientIpAddr + _T("）"));	
	//将ProcessFlag置为7（进入获取采集器下位机所有台站IP的模式）
	pDlg->m_pAwsdataStorage->m_ProcessFlag = 7;
	//由于需要较长时间的延时，因此为防止界面无响应，而创建单独的线程，向采集器发送获取子台站信息的命令
	CWinThread* pThread = AfxBeginThread(DauMeasureThread, NULL);	//不向新线程传递任何参数
	//提示等待信息
	pDlg->m_RichEdit2Info.ReplaceSel(_T("等待数据采集器返回子台站信息，请稍后...\r\n"));

	pDlg->UpdateData(FALSE);

	CSocket::OnAccept(nErrorCode);
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//子线程，全局函数
//
//向采集器发送获取子台站信息的命令的线程
UINT DauMeasureThread(LPVOID pParam)
{
	CAutoWeatherStationDlg* pDlg = (CAutoWeatherStationDlg*)theApp.GetMainWnd();
	HWND hDlg = pDlg->GetSafeHwnd();	//获取主对话框句柄
	//将采集器连接的所有子台站显示出来，方法是向采集器发送获取子台站信息的专用命令
	::Sleep(2000);	//延时2s
	//提示等待信息
	::SendMessage(hDlg, WM_LISTBOX_INFO, 3, 2);	//以发送消息的方式向主窗口信息显示窗打印等待数据返回的信息
	//循环发送命令，直到确认命令生效，最多将会尝试5次
	bool success = false;	//是否成功获取的标识
	CStringA stra = "jdgz_DEVIP\r\n";	//向数据采集器获取子台站IP信息的命令，多个台站IP将分多次返回，每次返回的数据格式为xxxx.xxx
	CClientSocket* cc = (CClientSocket*)pDlg->m_iSocket->m_listSockets.GetTail();	//暂时只取最后一个客户端进行操作（位于List的尾部）
	for (int i = 0; i < 5; i++)
	{
		cc->Send(stra, stra.GetLength());	//发送命令
		DWORD r = ::WaitForSingleObject(pDlg->m_pAwsdataStorage->m_DevIpSuccessEvent, 5000);	//等待信息返回成功标识
		if (r == WAIT_TIMEOUT)	//超时
		{
			::SendMessage(hDlg, WM_LISTBOX_INFO, 3, 4);	//“子台站列表返回\t失败\r\n即将进行下一次尝试\r\n”
		}
		else if (r == WAIT_OBJECT_0)	//成功
		{
			::SendMessage(hDlg, WM_LISTBOX_INFO, 3, 3);	//“子台站列表返回\t成功\r\n”
			success = true;	//设置成功标识
			break;
		}
		else if (r == WAIT_FAILED)	//出错
		{
			AfxMessageBox(_T("线程同步出错！"));
			break;	//直接跳出循环
		}
	}
	if (!success) //如果未成功获取，则提示用户检查线路连接问题
	{
		::SendMessage(hDlg, WM_LISTBOX_INFO, 3, 5);	//“5次尝试均失败，请检查是否存在网络线路问题，然后重启网络连接！\r\n”
	}

	return 0;
}
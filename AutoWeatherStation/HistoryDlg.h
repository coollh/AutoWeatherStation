#pragma once
#include "afxdtctl.h"
#include "afxcmn.h"
#include "Awsdata.h"
#include "EasySize.h"

// CHistoryDlg 对话框

class CHistoryDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CHistoryDlg)

	DECLARE_EASYSIZE	//EasySize宏定义

public:
	CHistoryDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CHistoryDlg();

// 对话框数据
	enum { IDD = IDD_DIALOG_HISTORY };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	CDateTimeCtrl m_DateStart;
	CDateTimeCtrl m_DateEnd;
	CDateTimeCtrl m_TimeEnd;
	CDateTimeCtrl m_TimeStart;
	afx_msg void OnBnClickedBtnStationset();
	afx_msg void OnClose();
	virtual void OnOK();
	virtual void OnCancel();
	CListCtrl m_HistoryTable;
	CAwsdata m_AwsDatabase;
	afx_msg void OnBnClickedBtnElementset();
	afx_msg void OnBnClickedBtnQ();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnSizing(UINT fwSide, LPRECT pRect);
private:
	TCHAR* m_pElementSelect;	//指向已选择要素数组的指针
public:
	afx_msg void OnBnClickedBtnExportdata();
};

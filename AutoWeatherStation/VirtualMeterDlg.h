
// VirtualMeterDlg.h : 头文件
//

#pragma once
#include "Meter.h"
#include "StThermometer.h"
#include "CompassCtrl\Compass.h"


// CVirtualMeterDlg 对话框
class CVirtualMeterDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CVirtualMeterDlg)
// 构造
public:
	CVirtualMeterDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
	enum { IDD = IDD_VIRTUALMETER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CStThermometer m_Thermometer;
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnEnChangeEdit1();
//	CCompass m_Compass;
	CMeter m_MeterWindSpeed;
	CStThermometer m_Humidiometer;
	CStThermometer m_Barometer;
protected:
	afx_msg LRESULT OnRefreshVirtualMeter(WPARAM wParam, LPARAM lParam);
public:
	afx_msg void OnClose();
	CMeter m_MeterWindDir;
};

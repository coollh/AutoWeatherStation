/*------------------------------------------------------------------
*	版权所有 (C) 2007～2009 西安众恒科技有限公司
*
*	文件名称：StThermometer.h
*
*	文件描述：模拟温度计
*
*	创 建 者：Z.G.Feng
*
*	版 本 号：Ver 1.0.0.0
*
*	创建日期：2009-03-11
 *------------------------------------------------------------------*/
#if !defined(AFX_STTHERMOMETER_H__6BDD0529_311F_4EE3_A5D0_B519735FC1F4__INCLUDED_)
#define AFX_STTHERMOMETER_H__6BDD0529_311F_4EE3_A5D0_B519735FC1F4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// StThermometer.h : header file
//


/////////////////////////////////////////////////////////////////////////////
// CStThermometer window

class CStThermometer : public CStatic
{
// Construction
public:
	CStThermometer();

// Attributes
public:
	void SetAxis(double dlMax,double dlMin,LPCTSTR szUints);
	void SetScale(int nTicks,int nSubTicks);
	void SetLimit(int nAlrm,int nWarn);
	void SetData(float flData,int flag);
// Operations
public:
	void OnDrawBackGround(CDC *pDC);
	void OnDrawScale(CDC* pDC);
	void OnDrawLimit(CDC* pDC);
	void OnDrawData(CDC* pDC);
	void OnRefurbish(void);
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CStThermometer)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CStThermometer();

	// Generated message map functions
protected:
	//{{AFX_MSG(CStThermometer)
	afx_msg void OnPaint();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
private:
	COLORREF			m_clrText;			//文本颜色
	COLORREF			m_clrBackGround;	//背景颜色
	COLORREF			m_clrBorder;		//外框颜色
	COLORREF			m_clrWithin;		//内框颜色

	double				m_dlMaxVal;			//最大值
	double				m_dlMinVal;			//最小值
	CString				m_strUints;			//单位符号
	int					m_nTicks;			//主刻度数
	int					m_nSubTicks;		//子刻度数

	int					m_nAlrmVal;			//报警限值
	int					m_nWarnVal;			//预警限值
	
	CDC					m_dcBkGround;
	CBitmap				m_bmBkGround;
	CBitmap*			m_bmOldBkGround;

	CRect				m_rectCtrl;			//空间区域
	CRect				m_rectDraw;			//绘图区域

	float				m_flData;			//当前数据
	float				text_data;          //显示数据
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STTHERMOMETER_H__6BDD0529_311F_4EE3_A5D0_B519735FC1F4__INCLUDED_)

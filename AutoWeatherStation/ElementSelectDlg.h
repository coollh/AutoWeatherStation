#pragma once


// CElementSelectDlg 对话框

class CElementSelectDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CElementSelectDlg)

public:
	CElementSelectDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CElementSelectDlg();

// 对话框数据
	enum { IDD = IDD_DIALOG_ElEMENTSELECT };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
private:
/*	BOOL m_AvgBaro;
	BOOL m_CurBaro;
	BOOL m_MaxBaro;
	BOOL m_MinBaro;
	BOOL m_AvgHumi;
	BOOL m_CurHumi;
	BOOL m_MaxHumi;
	BOOL m_MinHumi;
	BOOL m_AvgTemp;
	BOOL m_CurTemp;
	BOOL m_MaxTemp;
	BOOL m_MinTemp;
	BOOL m_CurWindDir;
	BOOL m_VectWindDir;
	BOOL m_AvgWindSpe;
	BOOL m_CurWindSpe;
	BOOL m_MaxWindSpe;
	BOOL m_MinWindSpe;
	BOOL m_VectWindSpe;	*/
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnButtonCheck(UINT uID);
};
